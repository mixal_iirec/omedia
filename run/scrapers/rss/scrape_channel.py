import feedparser

def scrape_channel(url, last_url):
	feed = feedparser.parse(url)
	out = []
	for e in feed.entries:
		item = {}
		url = None
		for link in e.links:
			type = link.get('type')
			if isinstance(type, str) and type.startswith('audio'):
				url = link.href
			#TODO: does the job, but there must be a better way...
			elif type == None and isinstance(link.href, str) and link.href.endswith('.mp3'):
				url = link.href
			#TODO is it possible to get rss with whole youtube channel?
			elif isinstance(link.href, str) and link.href.find('youtube.com'):
				url = link.href
		if url == None:
			continue
		item['url'] = url
		item['title'] = e.title
		date = e.published_parsed
		item['date'] = "{:04}{:02}{:02}".format(date[0], date[1], date[2])
		item['description'] = e.summary
		out.append(item)
	return out
