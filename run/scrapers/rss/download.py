import urllib.request

#TODO add resume functionality for bigger files
def download(type, url, dir, max_resolution, lang):
	if type == 'media_only' or type == 'media_and_subtitles':
		filename = url[url.rfind("/")+1:]
		path = dir + filename

		#often 403
		#urllib.request.urlretrieve(url, path)

		class AppURLopener(urllib.request.FancyURLopener):
			version = "User Agent"
		urllib._urlopener = AppURLopener()
		urllib._urlopener.retrieve(url, path)

		return (filename, None)
	else:
		return (None, None)
