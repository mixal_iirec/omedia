import yt_dlp

def download(url, ydl):
	info = ydl.extract_info(url, False)
	if info.get('_type') == 'url':
		info = ydl.extract_info(info.get('url'), False)
	out = []
	for e in info.get('entries'):
		item = {}
		item['url'] = e.get('url')
		item['title'] = e.get('title')
		out.append(item)
	return out

def scrape_channel(url, last_url):
	ydl_options = {
		'extract_flat' : True,
		'quiet': True,
		#TODO: first try should download less and compare with from_date
		'playlistend': 30,
	}
	ydl_options_all = {
		'extract_flat' : True,
		'quiet': True,
	}

	if last_url == "" :
		out = download(url, yt_dlp.YoutubeDL(ydl_options_all))
	else:
		out = download(url, yt_dlp.YoutubeDL(ydl_options))
		for v in out :
			if v['url'] == last_url:
				break
		else:
			out = download(url, yt_dlp.YoutubeDL(ydl_options_all))

	return out
