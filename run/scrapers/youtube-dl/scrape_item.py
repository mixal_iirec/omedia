import yt_dlp

class Logger(object):
    def debug(self, msg):
        #print("debug: " + msg)
        pass

    def warning(self, msg):
        #print("warning" + msg)
        pass

    #def error(self, msg):
        #print("error: " + msg)
        #pass

def scrape_item(url):
    ydl_options = {
        'extract_flat' : True,
        'quiet': True,
        'logger': Logger(),
    }

    ydl = yt_dlp.YoutubeDL(ydl_options)
    info = ydl.extract_info(url, False)
    item = {}
    item['title'] = info.get('title')
    item['description'] = info.get('description')
    item['date'] = info.get('upload_date')
    item['duration'] = info.get('duration')
    return item

print(scrape_item("https://www.youtube.com/watch?v=b6po19LWiz0"))
