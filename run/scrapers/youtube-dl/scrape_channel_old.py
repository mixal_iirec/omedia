import yt_dlp

def scrape_channel(url, from_date):
	ydl_options = {
		'extract_flat' : True,
		'quiet': True,
		#TODO: first try should download less and compare with from_date
		#'playlistend': 1,
	}

	ydl = yt_dlp.YoutubeDL(ydl_options)
	info = ydl.extract_info(url, False)
	if info.get('_type') == 'url':
		info = ydl.extract_info(info.get('url'), False)
	out = []
	for e in info.get('entries'):
		item = {}
		item['url'] = e.get('url')
		item['title'] = e.get('title')
		out.append(item)
	return out
