import yt_dlp
import os

_filename = ''
_dir = None

_subfiles = []

def set_filename(filename):
  global _filename

  if filename.startswith(_dir):
    global _filename
    _filename = filename[len(_dir):]

class Logger(object):
  def debug(self, msg):
    split = msg.split(_dir)
    if msg.endswith('.vtt') and len(split) == 2:
      global _subfiles
      _subfiles.append(split[1])
    #print("debug: " + msg)

  def warning(self, msg):
    #print("warning" + msg)
    pass

  def error(self, msg):
    #print("error: " + msg)
    pass

def find_file():
  global _filename
  if not os.path.isfile(_dir + _filename):
    prefix = os.path.splitext(_filename)[0]
    _filename = ''
    for filename in os.listdir(_dir):
      if filename.startswith(prefix) and not filename.endswith('.vtt'):
        _filename = filename

# todo new hook info
def hook(d):
  print(d['downloaded_bytes'], d['total_bytes'], sep='/')

def post_hook(d):
  if d['status'] == 'finished':
    set_filename(d['info_dict']['filepath'])
    find_file()

def download(type, url, dir, max_resolution, langs):
  global _dir
  _dir = dir
  ydl_options = {
    'quiet': True,
    'logger': Logger(),
    'outtmpl': dir + '%(id)s - %(title)s.%(ext)s',
    'format': 'bestvideo[height<={0}]+bestaudio/best[height<={0}]'.format(max_resolution),
    'progress_hooks': [hook],
    'postprocessor_hooks': [post_hook],
  }
  if type == 'media_only':
    ydl = yt_dlp.YoutubeDL(ydl_options)
    ydl.download([url])
    return (_filename, None)

  elif type == 'media_and_subtitles':
    ydl_options['writesubtitles'] = True
    ydl_options['subtitleslangs'] = langs.split(' ')
    ydl_options['subtitlesformat'] = 'vtt'

    ydl = yt_dlp.YoutubeDL(ydl_options)
    ydl.download([url])
    return (_filename, _subfiles)

  elif type == 'subtitles_only':
    ydl_options['skip_download'] = True
    #ydl_options['writeautomaticsub'] = True
    ydl_options['writesubtitles'] = True
    ydl_options['subtitleslangs'] = langs.split(' ')
    ydl_options['subtitlesformat'] = 'vtt'

    ydl = yt_dlp.YoutubeDL(ydl_options)
    ydl.download([url])
    return (None, _subfiles)
  else:
    return (None, None)
