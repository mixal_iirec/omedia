use std::collections::VecDeque;
use std::path::PathBuf;
use std::sync::{
    mpsc::{channel, Sender},
    Arc,
};
use std::thread::{self, JoinHandle};

use crate::config::Config;
use crate::msg::{DownloadType, EditMsg, RequestMsg};

use crate::entries::ENTRIES;

use crate::scrapers::DownloadScraper;

use crate::general::progress::{ProgressId, ProgressMsg};
use crate::item::ChItemId;

pub enum DownloadMsg {
    Abort,
}

pub struct Downloader {
    handle: Option<(
        ChItemId,
        JoinHandle<crate::Result<(Option<String>, Vec<String>)>>,
        Sender<DownloadMsg>,
    )>,
    tx_requests: Sender<RequestMsg>,

    send: Vec<ChItemId>,
    deque: VecDeque<(
        (String, PathBuf, DownloadScraper),
        ChItemId,
        ProgressId,
        DownloadType,
    )>,

    config: Arc<Config>,
}

impl Downloader {
    pub fn new(config: Arc<Config>, tx_requests: Sender<RequestMsg>) -> Self {
        Self {
            handle: None,
            tx_requests,

            send: Vec::new(),
            deque: VecDeque::new(),

            config,
        }
    }

    pub fn download(
        &mut self,
        item: (
            (String, PathBuf, DownloadScraper),
            ChItemId,
            ProgressId,
            DownloadType,
        ),
    ) {
        self.deque.push_back(item);
        self.try_next();
    }

    fn try_next(&mut self) {
        if self.handle.is_none() {
            if let Some(((url, dir, scraper), id, progress_id, typ)) = self.deque.pop_front() {
                self.send.push(id);
                let (tx, rx) = channel();
                let tx_requests = self.tx_requests.clone();
                let config = Arc::clone(&self.config);

                let _ = tx_requests.send(RequestMsg::ReportProgress {
                    id: progress_id,
                    msg: ProgressMsg::Set { done: 0, from: 1 },
                });

                let thread = thread::spawn(move || {
                    let result =
                        scraper.download(&url, &dir, typ, config, rx, &tx_requests, progress_id);
                    let _ = tx_requests.send(RequestMsg::FileDowloaded);
                    let _ = tx_requests.send(RequestMsg::ReportProgress {
                        id: progress_id,
                        msg: ProgressMsg::Done,
                    });
                    result
                });
                self.handle = Some((id, thread, tx));
            }
        }
    }

    pub fn download_finished(&mut self) -> crate::Result<()> {
        let res = self.join_handle();
        self.try_next();
        res
    }

    pub fn edit(&self, msg: EditMsg) -> crate::Result<()> {
        Ok(self.tx_requests.send(RequestMsg::Edit(msg))?)
    }

    fn join_handle(&mut self) -> crate::Result<()> {
        if let Some((id, handle, _)) = self.handle.take() {
            let (filename, subtitles_filenames) = handle.join().unwrap()?;
            if let Some(filename) = filename {
                self.edit(EditMsg::Str(
                    ENTRIES.string_name_id("downfile"),
                    vec![(id, filename)],
                ))?;
            }
            if !subtitles_filenames.is_empty() {
                self.edit(EditMsg::AddFilePaths(
                    ENTRIES.string_name_id("subtitles"),
                    vec![(id, subtitles_filenames)],
                ))?;
            }
        }
        Ok(())
    }

    pub fn done(&self) -> usize {
        self.send.len() - if self.handle.is_some() { 1 } else { 0 }
    }

    pub fn list_downloads(&self) -> impl Iterator<Item = ChItemId> + '_ {
        self.send
            .iter()
            .chain(self.deque.iter().map(|(_, id, _, _)| id))
            .cloned()
    }

    pub fn abort_download(&mut self, list: Vec<ChItemId>) {
        let set: std::collections::HashSet<_> = list.into_iter().collect();
        self.send.retain(|id| !set.contains(id));
        self.deque.retain(|(_, id, _, _)| !set.contains(id));
        if let Some((id, _, tx)) = &mut self.handle {
            if set.contains(id) {
                let _ = tx.send(DownloadMsg::Abort);
            }
        }
    }
}

impl Drop for Downloader {
    fn drop(&mut self) {
        if let Some((.., tx)) = self.handle.take() {
            let _ = tx.send(DownloadMsg::Abort);
        }
    }
}
