use std::borrow::Borrow;
use std::fs::{File, OpenOptions};
use std::io::{BufReader, BufWriter, Read, Seek, SeekFrom, Write};
use std::path::Path;

#[macro_use]
mod macros {
    macro_rules! impl_default{
        ($trait:ident for $($ty:ty),* $(,)?) => {$(impl $trait for $ty{})*};
    }
}

mod load_on_read;
mod string_file_iterator;
mod traits;
pub use load_on_read::LoadOnRead;
pub use string_file_iterator::StringFileIterator;
use traits::{BytesSize, FromBytes, WriteSelf};
#[cfg(test)]
mod tests;

pub fn from_compact_file<T: FromBytes>(path: &Path) -> crate::Result<Vec<T>> {
    fn fn_inner<T: FromBytes>(path: &Path) -> std::io::Result<Vec<T>> {
        if !path.exists() {
            return Ok(Vec::new());
        }
        let file = OpenOptions::new().read(true).open(path)?;
        let file_size = file.metadata()?.len() as usize;
        let mut buf_reader = BufReader::new(file);
        let mut ret = Vec::new();
        let mut buf = vec![0u8; T::bytes()];
        for _ in 0..file_size / T::bytes() {
            buf_reader.read_exact(&mut buf)?;
            ret.push(T::from_bytes(&buf));
        }
        Ok(ret)
    }
    Ok(fn_inner(path).map_err(|e| (path, e))?)
}

pub fn set_compact_file<T, B, It>(path: &Path, vars: It) -> crate::Result<()>
where
    T: BytesSize + WriteSelf,
    B: Borrow<T>,
    It: IntoIterator<Item = B>,
{
    let fn_inner = || {
        let file = OpenOptions::new().write(true).create(true).open(path)?;

        let mut buf = BufWriter::new(file);

        for v in vars.into_iter() {
            v.borrow().write(&mut buf)?;
        }
        buf.flush()?;
        Ok(())
    };
    Ok(fn_inner().map_err(|e| (path, e))?)
}

pub fn string_from_compact_file<P>((path, indices): (P, P)) -> crate::Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let path = path.as_ref();
    let indices = indices.as_ref();

    if !path.exists() {
        return Ok(Vec::new());
    }
    let file = File::open(path).map_err(|e| (path, e))?;
    let offsets = from_compact_file(indices)?;
    let mut buf_reader = BufReader::new(file);
    let mut ret = Vec::new();
    for i in 0..offsets.len() {
        let len = offsets[i] - if i == 0 { 0 } else { offsets[i - 1] };
        let mut buf = vec![0u8; len as usize];
        buf_reader.read_exact(&mut buf).map_err(|e| (path, e))?;
        ret.push(String::from_utf8(buf)?);
    }
    Ok(ret)
}

pub fn string_from_compact_file_iter<P, It>(
    (path, indices): (P, P),
    it: It,
) -> crate::Result<StringFileIterator<It>>
where
    P: AsRef<Path>,
    It: Iterator<Item = Option<usize>>,
{
    StringFileIterator::new(path.as_ref(), indices.as_ref(), it)
}

pub fn push_string_to_compact_file<S, It, P>(
    (path, indices): (P, P),
    vars: It,
    from: usize,
) -> crate::Result<()>
where
    S: AsRef<str>,
    It: IntoIterator<Item = S>,
    P: AsRef<Path>,
{
    let path = path.as_ref();
    let indices = indices.as_ref();

    let mut offsets: Vec<u32> = from_compact_file(indices)?;
    let last = offsets.last().cloned().unwrap_or(0);
    offsets.resize(from, last);

    let fn_inner = || {
        let mut file = OpenOptions::new().write(true).create(true).open(path)?;

        let mut pos = offsets.last().cloned().unwrap_or(0) as u64;
        file.set_len(pos)?;
        file.seek(SeekFrom::Start(pos))?;
        let mut buf = BufWriter::new(file);

        for v in vars.into_iter() {
            pos += buf.write(v.as_ref().as_bytes())? as u64;
            offsets.push(pos as u32);
        }
        buf.flush()?;
        Ok(())
    };
    fn_inner().map_err(|e| (path, e))?;

    set_compact_file::<u32, _, _>(indices, &offsets)?;

    Ok(())
}
