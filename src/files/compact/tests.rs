use std::path::PathBuf;

use super::*;

#[test]
fn string_compact_file() -> crate::Result<()> {
    let path1 = PathBuf::from("..test_string1");
    let path2 = PathBuf::from("..test_string2");
    let path = (path1.as_path(), path2.as_path());
    let mut vec: Vec<&str> = vec!["aoe", "axx", "aiclr", "", "aoe;qj", "clrg", "akmtnss", ""];
    push_string_to_compact_file(path, &vec, 0)?;
    assert_eq!(&vec, &string_from_compact_file(path)?);
    vec[3] = "333";
    vec[5] = "not";
    vec.pop();
    vec.pop();
    push_string_to_compact_file(path, &vec[3..], 3)?;
    assert_eq!(&vec, &string_from_compact_file(path)?);

    let idxs = vec![0, 5, 3];
    let from_idxs: Vec<Option<String>> =
        string_from_compact_file_iter(path, idxs.iter().map(|i| Some(*i)))?.collect();
    let compare: Vec<_> = idxs
        .iter()
        .map(|i| Some(String::from(*vec.get(*i).unwrap())))
        .collect();
    assert_eq!(from_idxs, compare);

    crate::files::remove_file(path.0)?;
    crate::files::remove_file(path.1)?;
    Ok(())
}
