use std::fs::File;
use std::io::{BufReader, Read};
use std::path::Path;

use super::from_compact_file;

pub struct StringFileIterator<It> {
    it: It,
    reader: Option<BufReader<File>>,
    last_read_pos: u32,
    offsets: Vec<u32>,
}

impl<It: Iterator<Item = Option<usize>>> StringFileIterator<It> {
    pub fn new(path: &Path, indices: &Path, it: It) -> crate::Result<Self> {
        Ok(Self {
            it,
            reader: if path.exists() {
                Some(BufReader::new(File::open(path).map_err(|e| (path, e))?))
            } else {
                None
            },
            last_read_pos: 0,
            offsets: from_compact_file(indices)?,
        })
    }
}

impl<It: Iterator<Item = Option<usize>>> Iterator for StringFileIterator<It> {
    type Item = Option<String>;
    fn next(&mut self) -> Option<Self::Item> {
        self.it.next().map(|offset| {
            let offset = offset?;
            let reader = self.reader.as_mut()?;
            let start = *self.offsets.get(offset.wrapping_sub(1)).unwrap_or(&0);

            let last_read_pos = &mut self.last_read_pos;
            self.offsets
                .get(offset)
                .map(|end| {
                    let mut buf = vec![0u8; (end - start) as usize];
                    reader
                        .seek_relative(start as i64 - *last_read_pos as i64)
                        .ok()?;
                    reader.read_exact(&mut buf).ok()?;
                    *last_read_pos = *end;
                    String::from_utf8(buf).ok()
                })
                .flatten()
        })
    }
}
