use chrono::{Datelike, NaiveDate};
use std::convert::TryInto;
use std::io::Write;

use crate::general::BitMask;

pub trait BytesSize: Sized {
    fn bytes() -> usize {
        std::mem::size_of::<Self>()
    }
}
impl_default!(BytesSize for i32, u32, u64, NaiveDate, BitMask<u8>, BitMask<u32>);

pub unsafe trait FromBytes: BytesSize {
    fn from_bytes(bytes: &[u8]) -> Self;
}
unsafe impl FromBytes for i32 {
    fn from_bytes(bytes: &[u8]) -> Self {
        Self::from_le_bytes(bytes.try_into().unwrap())
    }
}
unsafe impl FromBytes for u32 {
    fn from_bytes(bytes: &[u8]) -> Self {
        Self::from_le_bytes(bytes.try_into().unwrap())
    }
}
unsafe impl FromBytes for u64 {
    fn from_bytes(bytes: &[u8]) -> Self {
        Self::from_le_bytes(bytes.try_into().unwrap())
    }
}
unsafe impl FromBytes for BitMask<u32> {
    fn from_bytes(bytes: &[u8]) -> Self {
        Self::from(u32::from_le_bytes(bytes.try_into().unwrap()))
    }
}
unsafe impl FromBytes for NaiveDate {
    fn from_bytes(bytes: &[u8]) -> Self {
        NaiveDate::from_num_days_from_ce(i32::from_le_bytes(bytes.try_into().unwrap()))
    }
}

pub trait WriteSelf: Sized {
    fn write<W: Write>(&self, w: &mut W) -> std::io::Result<usize>;
}
impl WriteSelf for i32 {
    fn write<W: Write>(&self, w: &mut W) -> std::io::Result<usize> {
        w.write(&self.to_le_bytes())
    }
}
impl WriteSelf for u32 {
    fn write<W: Write>(&self, w: &mut W) -> std::io::Result<usize> {
        w.write(&self.to_le_bytes())
    }
}
impl WriteSelf for u64 {
    fn write<W: Write>(&self, w: &mut W) -> std::io::Result<usize> {
        w.write(&self.to_le_bytes())
    }
}
impl WriteSelf for BitMask<u8> {
    fn write<W: Write>(&self, w: &mut W) -> std::io::Result<usize> {
        w.write(&self.inner().to_le_bytes())
    }
}
impl WriteSelf for BitMask<u32> {
    fn write<W: Write>(&self, w: &mut W) -> std::io::Result<usize> {
        w.write(&self.inner().to_le_bytes())
    }
}
impl WriteSelf for NaiveDate {
    fn write<W: Write>(&self, w: &mut W) -> std::io::Result<usize> {
        w.write(&self.num_days_from_ce().to_le_bytes())
    }
}
