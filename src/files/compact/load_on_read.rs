use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

pub struct LoadOnRead {
    path: PathBuf,
    cache: Option<String>,
}

impl LoadOnRead {
    pub fn new(path: PathBuf) -> Self {
        Self { path, cache: None }
    }

    pub fn load_owned(&self) -> crate::Result<String> {
        let mut file = File::open(&self.path).map_err(|e| (&self.path, e))?;
        let mut res = String::new();
        file.read_to_string(&mut res).map_err(|e| (&self.path, e))?;
        Ok(res)
    }

    pub fn get_cached(&mut self) -> crate::Result<&str> {
        if self.cache.is_none() {
            self.cache = Some(self.load_owned()?);
        }
        Ok(self.cache.as_ref().unwrap())
    }

    pub fn clear_cache(&mut self) {
        self.cache = None;
    }
}
