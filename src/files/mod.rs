use std::fs::{DirEntry, File};
use std::io::{BufReader, BufWriter};
use std::path::Path;
pub mod compact;

pub fn create_dir_all(path: &Path) -> crate::Result<()> {
    std::fs::create_dir_all(path).map_err(|e| crate::Error::from((path, e)))
}

pub fn remove_dir_all(path: &Path) -> crate::Result<()> {
    std::fs::remove_dir_all(path).map_err(|e| crate::Error::from((path, e)))
}

pub fn read_dir(path: &Path) -> crate::Result<impl Iterator<Item = DirEntry>> {
    Ok(std::fs::read_dir(path)
        .map_err(|e| crate::Error::from((path, e)))?
        .filter_map(|path| path.ok()))
}

pub fn remove_file(path: &Path) -> crate::Result<()> {
    std::fs::remove_file(path).map_err(|e| crate::Error::from((path, e)))
}

pub fn prepare_string_for_filepath(s: &str) -> String {
    s.replace("/", "|")
}

pub fn yaml_to_file<T: serde::Serialize>(path: &Path, value: &T) -> crate::Result<()> {
    let yaml_writer = BufWriter::new(File::create(path).map_err(|e| (path, e))?);
    serde_yaml::to_writer(yaml_writer, value)?;
    Ok(())
}

pub fn yaml_from_file<'a, T: serde::de::DeserializeOwned>(path: &Path) -> crate::Result<T> {
    let yaml_reader = BufReader::new(File::open(path).map_err(|e| (path, e))?);
    Ok(serde_yaml::from_reader(yaml_reader)?)
}
