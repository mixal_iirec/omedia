use std::sync::Arc;

use crate::channel::{Channel, ChannelId, ChannelIdLess, ChannelMap};
use crate::config::BoolEntries;
use crate::entries::{EntryId, EntrySet};
use crate::general::progress::{ProgressId, ProgressMsg};
use crate::general::ListId;
use crate::item::{ChItemId, Item, ItemIdLess};
use crate::playlist::{Playlist, PlaylistId, PlaylistIdLess};

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub enum DownloadType {
    MediaOnly,
    MediaAndSubtitles,
    SubtitlesOnly,
}

#[derive(Debug)]
pub enum EditMsg {
    Bool(EntryId<bool>, Vec<(ChItemId, bool)>),
    I32(EntryId<i32>, Vec<(ChItemId, i32)>),
    Str(EntryId<str>, Vec<(ChItemId, String)>),
    AddFilePaths(EntryId<str>, Vec<(ChItemId, Vec<String>)>),

    BoolEntries(BoolEntries),

    Channel(Vec<(ChannelId, ChannelIdLess)>),
    Playlist(Vec<(PlaylistId, PlaylistIdLess)>),
}

#[derive(Debug)]
pub enum UpdateMsg {
    Items(Vec<Item>),
    AddItems(Vec<Item>),
    DownloadList { items: Vec<Item>, done: usize },
    Playlists(Vec<Arc<Playlist>>),
}

#[derive(Debug)]
pub enum UiMsg {
    Msg(&'static str),
    Progress(ProgressId, ProgressMsg),
    Error(crate::Error),
    ReceiveChannels(ChannelMap<Arc<Channel>>),
    ReceivePlaylists(ListId, Option<Arc<Channel>>, Vec<Arc<Playlist>>),
    ReceivePlaylist(ListId, Arc<Playlist>, Vec<Item>),
    ReceivePlayData(Vec<(String, f64, ChItemId)>),
    ReceiveDownloadList(ListId, Vec<Item>, usize),
    Update(ListId, UpdateMsg),
    End,
}

#[derive(Debug)]
pub enum ReqUpdateMsg {
    Playlists {
        channel: Option<ChannelId>,
    },
    Playlist {
        id: PlaylistId,
        str_needed: EntrySet<str>,
    },
    ItemsStrNeeded {
        items: Vec<Item>,
        str_needed: EntrySet<str>,
    },
    CustomPlaylist {
        items: Vec<Item>,
        playlist: PlaylistIdLess,
    },
    DownloadList {
        str_needed: EntrySet<str>,
    },
    AddItemsFromIds {
        items: Vec<ChItemId>,
        str_needed: EntrySet<str>,
    },
}

#[derive(Debug)]
pub enum RequestMsg {
    RequestChannels,
    RequestPlaylists {
        list: ListId,
        channel: Option<ChannelId>,
    },
    AddChannels {
        channels: Vec<ChannelIdLess>,
    },
    DeleteChannels {
        channels: Vec<ChannelId>,
    },
    AddPlaylists {
        channel: Option<ChannelId>,
        playlists: Vec<PlaylistIdLess>,
    },
    DeletePlaylists {
        ids: Vec<PlaylistId>,
    },
    RequestPlaylist {
        list: ListId,
        id: PlaylistId,
        str_needed: EntrySet<str>,
    },
    FetchItems {
        all: bool,
        progress: ProgressId,
    },
    FetchedItems {
        channel: ChannelId,
        items: Vec<ItemIdLess>,
    },
    Edit(EditMsg),
    DeleteItems {
        ids: Vec<ChItemId>,
    },
    DeleteItemMedia {
        ids: Vec<ChItemId>,
    },
    DownloadItems {
        typ: DownloadType,
        list: Vec<(ChItemId, ProgressId)>,
    },
    RequestPlayData {
        list: Vec<ChItemId>,
    },
    RequestDownloadList {
        list: ListId,
        str_needed: EntrySet<str>,
    },
    AbortDownload {
        ids: Vec<ChItemId>,
    },
    ReportProgress {
        id: ProgressId,
        msg: ProgressMsg,
    },
    CopyItems {
        ids: Vec<ChItemId>,
        into: ChannelId,
    },
    CopyPlaylists {
        ids: Vec<PlaylistId>,
        into: Option<ChannelId>,
    },
    RequestUpdate {
        list: ListId,
        msg: ReqUpdateMsg,
    },

    FileDowloaded,
    End,
}
