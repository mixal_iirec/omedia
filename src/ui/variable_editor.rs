use pancurses::endwin;

use std::collections::BTreeMap;
use std::io::Write;
use std::path::Path;

use serde::de::DeserializeOwned;
use serde::ser::Serialize;

use crate::files::{yaml_from_file, yaml_to_file};

pub fn edit_new_var<T, FS, FL>(editor: &str, fsave: FS, floader: FL) -> crate::Result<T>
where
    FS: FnOnce(&Path) -> crate::Result<()>,
    FL: Fn(&Path) -> crate::Result<T>,
{
    let path = std::env::temp_dir();
    let path = (0..)
        .map(|i| path.join(format!("omedia-temp-edit-{}.yaml", i)))
        .find(|p| !p.exists())
        .unwrap();

    fsave(&path)?;

    endwin();

    let result = loop {
        std::process::Command::new(editor)
            .arg(path.as_os_str())
            .spawn()
            .map_err(|e| crate::Error::Io(e))?
            .wait()
            .map_err(|e| crate::Error::Io(e))?;

        match floader(&path) {
            Ok(o) => break Ok(o),
            Err(error) => {
                let mut file = std::fs::OpenOptions::new()
                    .write(true)
                    .append(true)
                    .open(&path)
                    .map_err(|e| (&path, e))?;

                // empty file is a way to abort editing
                if file.metadata().map_err(|e| (&path, e))?.len() == 0 {
                    break Err(crate::Error::ActionAbortedByUser);
                }

                write!(file, "#{error}").map_err(|e| (&path, e))?;
            }
        }
    };

    crate::files::remove_file(&path)?;
    result
}

pub fn edit_var<T: Serialize, O: DeserializeOwned>(editor: &str, default: &T) -> crate::Result<O> {
    edit_new_var(
        editor,
        |path| yaml_to_file(path, default),
        |path| yaml_from_file(path),
    )
}

pub fn edit_var_list<T, ID, IDLESS: Serialize + DeserializeOwned>(
    editor: &str,
    list: &Vec<T>,
    it: impl Iterator<Item = usize>,
    f_id: impl Fn(&T) -> ID,
    f_id_less: impl Fn(&T) -> &IDLESS,
) -> crate::Result<Vec<(ID, IDLESS)>> {
    let map_indices = it
        .filter_map(|idx| Some((idx, f_id_less(list.get(idx)?))))
        .collect();
    Ok(
        edit_var::<BTreeMap<_, _>, BTreeMap<usize, _>>(editor, &map_indices)?
            .into_iter()
            .filter_map(|(idx, id_less)| Some((f_id(list.get(idx)?), id_less)))
            .collect(),
    )
}
