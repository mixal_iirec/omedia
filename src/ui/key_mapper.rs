use itertools::Itertools;
use pancurses::Input;
use std::borrow::{Borrow, Cow};

use crate::msg::DownloadType;

type Map<K, V> = std::collections::HashMap<K, V>;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub enum MovementType {
    Up,
    Down,
    PageUp,
    PageDown,
    Begin,
    End,
    Marked,
    Stationar,
    Search { forward: bool },
    Goto,
}

fn movement_comment(typ: MovementType) -> &'static str {
    match typ {
        MovementType::Up => "Up",
        MovementType::Down => "Down",
        MovementType::PageUp => "PageUp",
        MovementType::PageDown => "PageDown",
        MovementType::Begin => "Start",
        MovementType::End => "End",
        MovementType::Marked => "Marked",
        MovementType::Stationar => "Stationar",
        MovementType::Search { forward: true } => "Search forward",
        MovementType::Search { forward: false } => "Search backward",
        MovementType::Goto => "Goto",
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Movement {
    pub typ: MovementType,
    pub modifier: String,
    pub repeat: usize,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub enum RangeAction {
    Download { typ: DownloadType },
    Mark,
    Play,
    Yank,
    DeleteShallow,
    DeleteMedia,
    DeleteDeep,
    Edit,
    SetBool { val: bool },
}

fn range_comment(typ: RangeAction) -> &'static str {
    match typ {
        RangeAction::Download {
            typ: DownloadType::MediaOnly,
        } => "Download only media",
        RangeAction::Download {
            typ: DownloadType::MediaAndSubtitles,
        } => "Download all",
        RangeAction::Download {
            typ: DownloadType::SubtitlesOnly,
        } => "Download only subtitles",
        RangeAction::Mark => "Mark",
        RangeAction::Play => "Play",
        RangeAction::Yank => "Yank",
        RangeAction::DeleteShallow => "Delete shallow (remove from list)",
        RangeAction::DeleteDeep => "Delete deep (delete from files)",
        RangeAction::DeleteMedia => "Delete downloaded media",
        RangeAction::Edit => "Edit",
        RangeAction::SetBool { val: true } => "Set boolean value",
        RangeAction::SetBool { val: false } => "Unset boolean value",
    }
}

fn range_modifiable(action: RangeAction) -> bool {
    match action {
        RangeAction::SetBool { .. } => true,
        _ => false,
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct RangeActionMod {
    pub typ: RangeAction,
    pub modifier: String,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub enum SpecialAction {
    Quit,
    Clear,
    Add,
    TemporalPlaylist,
    Return,
    Fetch { all: bool },
    NextList,
    Select,
    MarkOne,
    PasteDeep,
    PasteShallow,
    Sort { smallest_up: bool },
    DrawCustom,
    Enum,
    Help,
    RepeatAction,

    EditEntries,

    TabClose,
    TabUpdate,
    TabNewChannels,
    TabNewPlaylists,
    TabNewDownloads,
    TabNewProgressBars,

    TabEntries,
    TabNewDrawerChoice,
}

fn special_comment(typ: SpecialAction) -> &'static str {
    match typ {
        SpecialAction::Quit => "Quit",
        SpecialAction::Clear => "Clear message bar",
        SpecialAction::Add => "Add",
        SpecialAction::TemporalPlaylist => "Create temporal custom playlist",
        SpecialAction::Return => "Return",
        SpecialAction::Fetch { all: true } => "Fetch all items",
        SpecialAction::Fetch { all: false } => "Fetch only new items",
        SpecialAction::NextList => "NextList",
        SpecialAction::Select => "Select",
        SpecialAction::MarkOne => "Mark one item",
        SpecialAction::PasteDeep => "Paste deep (create copy in channel files)",
        SpecialAction::PasteShallow => "Paste shallow (into list)",
        SpecialAction::Sort { smallest_up: true } => "Sort with smallest up",
        SpecialAction::Sort { smallest_up: false } => "Sort with smallest down",
        SpecialAction::DrawCustom => "Specify custom drawer ex. `title|duration`",
        SpecialAction::Enum => "Enumerate list",
        SpecialAction::Help => "Help",
        SpecialAction::RepeatAction => "Repeat last action",

        SpecialAction::EditEntries => "Edit bool entries names",

        SpecialAction::TabClose => "Close tab",
        SpecialAction::TabUpdate => "Update tab",
        SpecialAction::TabNewChannels => "Open channel tab",
        SpecialAction::TabNewPlaylists => "Open playlist tab",
        SpecialAction::TabNewDownloads => "Open downloads tab",
        SpecialAction::TabNewProgressBars => "Open progress tab",

        SpecialAction::TabEntries => "List all entries used in items",
        SpecialAction::TabNewDrawerChoice => "Open drawer choice tab",
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct SpecialActionMod {
    pub typ: SpecialAction,
    pub modifier: String,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Action {
    Range {
        action: Option<RangeActionMod>,
        movement: Movement,
    },
    Special {
        action: SpecialActionMod,
    },
}

enum TreeAccessError {
    TooShort,
    BranchMiss,
}
use TreeAccessError::*;

enum ActionTree<T: Copy> {
    Sub(Map<Input, ActionTree<T>>),
    Action(T),
}

impl<T: Copy> ActionTree<T> {
    fn new() -> Self {
        Self::Sub(Map::new())
    }

    fn from_action<In: Borrow<Input>, I: Iterator<Item = In>>(input: &mut I, action: T) -> Self {
        match input.next() {
            Some(v) => Self::Sub(
                std::iter::once((*v.borrow(), Self::from_action(input, action))).collect(),
            ),
            None => Self::Action(action),
        }
    }

    fn add_action<In: Borrow<Input>, I: Iterator<Item = In>>(
        &mut self,
        input: &mut I,
        action: T,
    ) -> bool {
        if let Some(inp) = input.next() {
            match self {
                Self::Sub(map) => {
                    let mut ret = true;
                    map.entry(*inp.borrow())
                        .and_modify(|tree| ret = tree.add_action(input, action))
                        .or_insert_with(|| Self::from_action(input, action));
                    ret
                }
                Self::Action(_) => false,
            }
        } else {
            false
        }
    }

    fn action<'a, In: Borrow<Input>, I: Iterator<Item = In>>(
        &self,
        input: &'a mut I,
    ) -> Result<(T, &'a mut I), TreeAccessError> {
        match self {
            Self::Sub(map) => map
                .get(input.next().ok_or(TooShort)?.borrow())
                .ok_or(BranchMiss)?
                .action(input),
            Self::Action(action) => Ok((*action, input)),
        }
    }

    fn __list_actions(&self, actions: &mut Vec<(String, T)>, buffer: &mut Vec<Input>) {
        match self {
            ActionTree::Action(t) => {
                actions.push((input_iter_as_str_human_readable(buffer.iter()), *t))
            }
            ActionTree::Sub(map) => {
                for (input, tree) in map.iter() {
                    buffer.push(*input);
                    tree.__list_actions(actions, buffer);
                    buffer.pop();
                }
            }
        }
    }
    fn list_actions(&self) -> Vec<(String, T)> {
        let mut actions = Vec::new();
        self.__list_actions(&mut actions, &mut Vec::new());
        actions
    }
}

impl<In: Borrow<Input>, I: IntoIterator<Item = In>, T: Copy> std::iter::FromIterator<(I, T)>
    for ActionTree<T>
{
    fn from_iter<It: IntoIterator<Item = (I, T)>>(it: It) -> Self {
        let mut s = Self::new();
        s.extend(it);
        s
    }
}
impl<In: Borrow<Input>, I: IntoIterator<Item = In>, T: Copy> Extend<(I, T)> for ActionTree<T> {
    fn extend<It: IntoIterator<Item = (I, T)>>(&mut self, it: It) {
        for (input, action) in it {
            self.add_action(&mut input.into_iter(), action);
        }
    }
}

pub struct KeyMapper {
    specials: ActionTree<SpecialAction>,
    ranges: ActionTree<RangeAction>,
    movements: ActionTree<MovementType>,

    last_action: Option<Action>,
    input: Vec<Input>,
    modifier: bool,
}

impl KeyMapper {
    pub fn new() -> Self {
        Self {
            specials: vec![
                (
                    "quit".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::Quit,
                ),
                (
                    "clear".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::Clear,
                ),
                (
                    "add".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::Add,
                ),
                (
                    "ch".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::TemporalPlaylist,
                ),
                (vec![Input::Character(27 as char)], SpecialAction::Return), //Escape has 1s timeout
                (vec![Input::Character('`')], SpecialAction::Return),
                (vec![Input::Character(';')], SpecialAction::Return),
                (
                    vec![Input::Character('f')],
                    SpecialAction::Fetch { all: false },
                ),
                (
                    vec![Input::Character('F')],
                    SpecialAction::Fetch { all: true },
                ),
                (vec![Input::Character('\t')], SpecialAction::NextList),
                (vec![Input::KeyEnter], SpecialAction::Select),
                (vec![Input::Character('\n')], SpecialAction::Select),
                (vec![Input::Character(' ')], SpecialAction::MarkOne),
                (
                    "PP".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::PasteDeep,
                ),
                (
                    "pp".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::PasteShallow,
                ),
                (
                    "sort/".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::Sort { smallest_up: true },
                ),
                (
                    "sort?".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::Sort { smallest_up: false },
                ),
                (
                    "draw/".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::DrawCustom,
                ),
                (
                    "enum".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::Enum,
                ),
                (vec![Input::KeyF1], SpecialAction::Help),
                (
                    "help".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::Help,
                ),
                (
                    ".".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::RepeatAction,
                ),
                (
                    "ZZ".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::TabClose,
                ),
                (
                    "tC".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::TabClose,
                ),
                (
                    "tup".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::TabUpdate,
                ),
                (
                    "tch".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::TabNewChannels,
                ),
                (
                    "tpl".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::TabNewPlaylists,
                ),
                (
                    "tdown".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::TabNewDownloads,
                ),
                (
                    "tprog".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::TabNewProgressBars,
                ),
                (
                    "tentry".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::TabEntries,
                ),
                (
                    "tdraw".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::TabNewDrawerChoice,
                ),
                (
                    "edit_bool".chars().map(|c| Input::Character(c)).collect(),
                    SpecialAction::EditEntries,
                ),
            ]
            .into_iter()
            .collect(),
            ranges: vec![
                (
                    "DD".chars().map(|c| Input::Character(c)).collect(),
                    RangeAction::DeleteDeep,
                ),
                (
                    "Dm".chars().map(|c| Input::Character(c)).collect(),
                    RangeAction::DeleteMedia,
                ),
                (
                    "dd".chars().map(|c| Input::Character(c)).collect(),
                    RangeAction::DeleteShallow,
                ),
                (
                    "dm".chars().map(|c| Input::Character(c)).collect(),
                    RangeAction::Download {
                        typ: DownloadType::MediaOnly,
                    },
                ),
                (
                    "dM".chars().map(|c| Input::Character(c)).collect(),
                    RangeAction::Download {
                        typ: DownloadType::MediaAndSubtitles,
                    },
                ),
                (
                    "ds".chars().map(|c| Input::Character(c)).collect(),
                    RangeAction::Download {
                        typ: DownloadType::SubtitlesOnly,
                    },
                ),
                (
                    "e".chars().map(|c| Input::Character(c)).collect(),
                    RangeAction::Edit,
                ),
                (vec![Input::Character('m')], RangeAction::Mark),
                (vec![Input::Character('p')], RangeAction::Play),
                (vec![Input::Character('y')], RangeAction::Yank),
                (
                    "set/".chars().map(|c| Input::Character(c)).collect(),
                    RangeAction::SetBool { val: true },
                ),
                (
                    "set?".chars().map(|c| Input::Character(c)).collect(),
                    RangeAction::SetBool { val: false },
                ),
            ]
            .into_iter()
            .collect(),
            movements: vec![
                (vec![Input::Character('k')], MovementType::Up),
                (vec![Input::Character('j')], MovementType::Down),
                (vec![Input::KeyUp], MovementType::Up),
                (vec![Input::KeyDown], MovementType::Down),
                (vec![Input::KeyPPage], MovementType::PageUp),
                (vec![Input::KeyNPage], MovementType::PageDown),
                (vec![Input::KeyHome], MovementType::Begin),
                (vec![Input::KeyEnd], MovementType::End),
                (vec![Input::Character('m')], MovementType::Marked),
                (vec![Input::Character(' ')], MovementType::Stationar),
                (
                    vec![Input::Character('/')],
                    MovementType::Search { forward: true },
                ),
                (
                    vec![Input::Character('?')],
                    MovementType::Search { forward: false },
                ),
                (vec![Input::Character('g')], MovementType::Goto),
            ]
            .into_iter()
            .collect(),
            last_action: None,
            input: Vec::new(),
            modifier: false,
        }
    }

    fn check_modifier(&mut self) {
        self.modifier = false;
        for input in self.input.iter() {
            if is_modifier(*input) {
                self.modifier = true;
            } else if modifier_ender(*input) {
                self.modifier = false;
            }
        }
    }

    pub fn get_action(&mut self, input: Input) -> Option<Action> {
        if input == Input::KeyBackspace {
            self.input.pop();
            self.check_modifier();
        } else {
            self.input.push(input);
        }

        if self.modifier {
            if modifier_ender(input) {
                self.modifier = false;
            } else {
                return None;
            }
        } else if is_modifier(input) {
            self.modifier = true;
        }

        let mut action = match self.specials.action(&mut self.input.iter()) {
            Err(TooShort) => None,
            Err(BranchMiss) => {
                let mut iter = self.input.iter();
                let action = match self.ranges.action(iter.by_ref()) {
                    Err(TooShort) => return None,
                    Err(BranchMiss) => {
                        iter = self.input.iter();
                        None
                    }
                    Ok((typ, modifier)) => Some(RangeActionMod {
                        typ,
                        modifier: if range_modifiable(typ) {
                            take_modifier(modifier)
                        } else {
                            String::new()
                        },
                    }),
                };

                let repeat = iter
                    .take_while_ref(|c| {
                        if let Input::Character(c) = c {
                            c.is_digit(10)
                        } else {
                            false
                        }
                    })
                    .map(|c| {
                        if let Input::Character(c) = c {
                            c.to_digit(10).unwrap() as usize
                        } else {
                            unreachable!()
                        }
                    })
                    .fold(0, |pre, d| pre * 10 + d);
                let repeat = if repeat == 0 { 1 } else { repeat };

                match self.movements.action(iter.by_ref()) {
                    Err(TooShort) => None,
                    Err(BranchMiss) => {
                        self.modifier = false;
                        self.input.clear();
                        None
                    }
                    Ok((typ, modifier)) => {
                        (!self.modifier || modifier_ender(input)).then(|| Action::Range {
                            movement: Movement {
                                typ,
                                modifier: take_modifier(modifier),
                                repeat,
                            },
                            action,
                        })
                    }
                }
            }
            Ok((typ, modifier)) => {
                (!self.modifier || modifier_ender(input)).then(|| Action::Special {
                    action: SpecialActionMod {
                        typ,
                        modifier: take_modifier(modifier),
                    },
                })
            }
        };
        if action.is_some() {
            self.modifier = false;
            if let Action::Special {
                action:
                    SpecialActionMod {
                        typ: SpecialAction::RepeatAction,
                        ..
                    },
            } = action.as_ref().unwrap()
            {
                action = self.last_action.clone();
            } else {
                self.last_action = action.clone();
            }
            self.input = Vec::new();
        }
        action
    }

    pub fn input_buffer_str(&self) -> String {
        input_iter_as_str_human_readable(self.input.iter())
    }

    pub fn list_special_actions(&self) -> impl Iterator<Item = (String, &'static str)> {
        let mut list = self.specials.list_actions();
        list.sort_by_key(|a| a.1);
        list.into_iter()
            .map(|(input, action)| (input, special_comment(action)))
    }

    pub fn list_movement_actions(&self) -> impl Iterator<Item = (String, &'static str)> {
        let mut list = self.movements.list_actions();
        list.sort_by_key(|a| a.1);
        list.into_iter()
            .map(|(input, action)| (input, movement_comment(action)))
    }

    pub fn list_range_actions(&self) -> impl Iterator<Item = (String, &'static str)> {
        let mut list = self.ranges.list_actions();
        list.sort_by_key(|a| a.1);
        list.into_iter()
            .map(|(input, action)| (input, range_comment(action)))
    }
}

fn take_modifier<In: Borrow<Input>>(input: &mut impl Iterator<Item = In>) -> String {
    input_iter_as_str(input.take_while(|i| !modifier_ender(*i.borrow())))
}

fn modifier_ender(input: Input) -> bool {
    input == Input::Character('\n') || input == Input::KeyEnter
}
fn is_modifier(input: Input) -> bool {
    input == Input::Character('/') || input == Input::Character('?')
}

fn input_iter_as_str<In: Borrow<Input>, I: Iterator<Item = In>>(input: I) -> String {
    input
        .filter_map(|inp| match *inp.borrow() {
            Input::Character(c) => Some(c),
            _ => None,
        })
        .collect()
}

fn input_iter_as_str_human_readable<In: Borrow<Input>, I: Iterator<Item = In>>(input: I) -> String {
    input
        .into_iter()
        .map(|c| pancurses_input_to_str(*c.borrow()))
        .fold(String::new(), |pre, s| pre + &s)
}

fn pancurses_input_to_str(input: Input) -> Cow<'static, str> {
    match input {
        Input::Character('\u{1b}') => Cow::from("<Escape>"),
        Input::Character('\n') => Cow::from("<Enter>"),
        Input::Character('\t') => Cow::from("<Tab>"),
        Input::Character(' ') => Cow::from("␣"),
        Input::Character(s) => Cow::from(format!("{}", s)),
        Input::Unknown(u) => Cow::from(format!("<?{}?>", u)),
        Input::KeyCodeYes => Cow::from("<CodeYes>"),
        Input::KeyBreak => Cow::from("<Break>"),
        Input::KeyDown => Cow::from("<Down>"),
        Input::KeyUp => Cow::from("<Up>"),
        Input::KeyLeft => Cow::from("<Left>"),
        Input::KeyRight => Cow::from("<Right>"),
        Input::KeyHome => Cow::from("<Home>"),
        Input::KeyBackspace => Cow::from("<Backspace>"),
        Input::KeyF0 => Cow::from("<F0>"),
        Input::KeyF1 => Cow::from("<F1>"),
        Input::KeyF2 => Cow::from("<F2>"),
        Input::KeyF3 => Cow::from("<F3>"),
        Input::KeyF4 => Cow::from("<F4>"),
        Input::KeyF5 => Cow::from("<F5>"),
        Input::KeyF6 => Cow::from("<F6>"),
        Input::KeyF7 => Cow::from("<F7>"),
        Input::KeyF8 => Cow::from("<F8>"),
        Input::KeyF9 => Cow::from("<F9>"),
        Input::KeyF10 => Cow::from("<F10>"),
        Input::KeyF11 => Cow::from("<F11>"),
        Input::KeyF12 => Cow::from("<F12>"),
        Input::KeyF13 => Cow::from("<F13>"),
        Input::KeyF14 => Cow::from("<F14>"),
        Input::KeyF15 => Cow::from("<F15>"),
        Input::KeyDL => Cow::from("<DL>"),
        Input::KeyIL => Cow::from("<IL>"),
        Input::KeyDC => Cow::from("<DC>"),
        Input::KeyIC => Cow::from("<IC>"),
        Input::KeyEIC => Cow::from("<EIC>"),
        Input::KeyClear => Cow::from("<Clear>"),
        Input::KeyEOS => Cow::from("<EOS>"),
        Input::KeyEOL => Cow::from("<EOL>"),
        Input::KeySF => Cow::from("<SF>"),
        Input::KeySR => Cow::from("<SR>"),
        Input::KeyNPage => Cow::from("<NPage>"),
        Input::KeyPPage => Cow::from("<PPage>"),
        Input::KeySTab => Cow::from("<STab>"),
        Input::KeyCTab => Cow::from("<CTab>"),
        Input::KeyCATab => Cow::from("<CATab>"),
        Input::KeyEnter => Cow::from("<Enter>"),
        Input::KeySReset => Cow::from("<SReset>"),
        Input::KeyReset => Cow::from("<Reset>"),
        Input::KeyPrint => Cow::from("<Print>"),
        Input::KeyLL => Cow::from("<LL>"),
        Input::KeyAbort => Cow::from("<Abort>"),
        Input::KeySHelp => Cow::from("<SHelp>"),
        Input::KeyLHelp => Cow::from("<LHelp>"),
        Input::KeyBTab => Cow::from("<BTab>"),
        Input::KeyBeg => Cow::from("<Beg>"),
        Input::KeyCancel => Cow::from("<Cancel>"),
        Input::KeyClose => Cow::from("<Close>"),
        Input::KeyCommand => Cow::from("<Command>"),
        Input::KeyCopy => Cow::from("<Copy>"),
        Input::KeyCreate => Cow::from("<Create>"),
        Input::KeyEnd => Cow::from("<End>"),
        Input::KeyExit => Cow::from("<Exit>"),
        Input::KeyFind => Cow::from("<Find>"),
        Input::KeyHelp => Cow::from("<Help>"),
        Input::KeyMark => Cow::from("<Mark>"),
        Input::KeyMessage => Cow::from("<Message>"),
        Input::KeyMove => Cow::from("<Move>"),
        Input::KeyNext => Cow::from("<Next>"),
        Input::KeyOpen => Cow::from("<Open>"),
        Input::KeyOptions => Cow::from("<Options>"),
        Input::KeyPrevious => Cow::from("<Previous>"),
        Input::KeyRedo => Cow::from("<Redo>"),
        Input::KeyReference => Cow::from("<Reference>"),
        Input::KeyRefresh => Cow::from("<Refresh>"),
        Input::KeyReplace => Cow::from("<Replace>"),
        Input::KeyRestart => Cow::from("<Restart>"),
        Input::KeyResume => Cow::from("<Resume>"),
        Input::KeySave => Cow::from("<Save>"),
        Input::KeySBeg => Cow::from("<SBeg>"),
        Input::KeySCancel => Cow::from("<SCancel>"),
        Input::KeySCommand => Cow::from("<SCommand>"),
        Input::KeySCopy => Cow::from("<SCopy>"),
        Input::KeySCreate => Cow::from("<SCreate>"),
        Input::KeySDC => Cow::from("<SDC>"),
        Input::KeySDL => Cow::from("<SDL>"),
        Input::KeySelect => Cow::from("<Select>"),
        Input::KeySEnd => Cow::from("<SEnd>"),
        Input::KeySEOL => Cow::from("<SEOL>"),
        Input::KeySExit => Cow::from("<SExit>"),
        Input::KeySFind => Cow::from("<SFind>"),
        Input::KeySHome => Cow::from("<SHome>"),
        Input::KeySIC => Cow::from("<SIC>"),
        Input::KeySLeft => Cow::from("<SLeft>"),
        Input::KeySMessage => Cow::from("<SMessage>"),
        Input::KeySMove => Cow::from("<SMove>"),
        Input::KeySNext => Cow::from("<SNext>"),
        Input::KeySOptions => Cow::from("<SOptions>"),
        Input::KeySPrevious => Cow::from("<SPrevious>"),
        Input::KeySPrint => Cow::from("<SPrint>"),
        Input::KeySRedo => Cow::from("<SRedo>"),
        Input::KeySReplace => Cow::from("<SReplace>"),
        Input::KeySRight => Cow::from("<SRight>"),
        Input::KeySResume => Cow::from("<SResume>"),
        Input::KeySSave => Cow::from("<SSave>"),
        Input::KeySSuspend => Cow::from("<SSuspend>"),
        Input::KeySUndo => Cow::from("<SUndo>"),
        Input::KeySuspend => Cow::from("<Suspend>"),
        Input::KeyUndo => Cow::from("<Undo>"),
        Input::KeyResize => Cow::from("<Resize>"),
        Input::KeyEvent => Cow::from("<Event>"),
        Input::KeyMouse => Cow::from("<Mouse>"),
        Input::KeyA1 => Cow::from("<A1>"),
        Input::KeyA3 => Cow::from("<A3>"),
        Input::KeyB2 => Cow::from("<B2>"),
        Input::KeyC1 => Cow::from("<C1>"),
        Input::KeyC3 => Cow::from("<C3>"),
    }
}
