use chrono::NaiveDate;

use crate::entries::{EntryId, EntryMap, ENTRIES};
use crate::item::{ChItemId, ItemIdLess};
use crate::msg::{EditMsg, RequestMsg};

use crate::ui::handler::shareable::HandlerShareable;

#[derive(Debug, Copy, Clone)]
pub enum ItemEntryKind {
    Date,
    Channel,
    Bool(EntryId<bool>),
    I32(EntryId<i32>),
    Str(EntryId<str>),
}

pub fn smart_name_to_entryid(name: &str) -> crate::Result<ItemEntryKind> {
    Ok(match name {
        "date" => ItemEntryKind::Date,
        "channel" => ItemEntryKind::Channel,
        name => {
            if let Some(name) = name.strip_prefix('B') {
                ItemEntryKind::Bool(
                    ENTRIES
                        .bool_name_id_option(name)
                        .ok_or(crate::Error::InvalidEntryName)?,
                )
            } else if let Some(name) = name.strip_prefix('I') {
                ItemEntryKind::I32(
                    ENTRIES
                        .i32_name_id_option(name)
                        .ok_or(crate::Error::InvalidEntryName)?,
                )
            } else if let Some(name) = name.strip_prefix('S') {
                ItemEntryKind::Str(
                    ENTRIES
                        .string_name_id_option(name)
                        .ok_or(crate::Error::InvalidEntryName)?,
                )
            } else if let Some(id) = ENTRIES.bool_name_id_option(name) {
                ItemEntryKind::Bool(id)
            } else if let Some(id) = ENTRIES.i32_name_id_option(name) {
                ItemEntryKind::I32(id)
            } else if let Some(id) = ENTRIES.string_name_id_option(name) {
                ItemEntryKind::Str(id)
            } else {
                return Err(crate::Error::InvalidEntryName);
            }
        }
    })
}

pub fn send_item_edit<'a>(
    handler: &HandlerShareable,
    iter: impl IntoIterator<Item = (ChItemId, &'a ItemIdLess, &'a ItemIdLess)>,
) -> crate::Result<()> {
    let mut _dates: Vec<(ChItemId, NaiveDate)> = Vec::new();
    let mut bools: EntryMap<bool, Vec<(ChItemId, bool)>> = EntryMap::new();
    let mut i32s: EntryMap<i32, Vec<(ChItemId, i32)>> = EntryMap::new();
    let mut strings: EntryMap<str, Vec<(ChItemId, String)>> = EntryMap::new();

    for (id, old, new) in iter {
        if new.date() != old.date() {
            _dates.push((id, new.date().clone()));
        }

        let var_bools = new.bools;
        for idx in 0..var_bools.len() {
            if var_bools.get(idx) != old.var_bool(idx.into()) {
                bools
                    .get_mut_or_default(idx.into())
                    .push((id, var_bools.get(idx)))
            }
        }

        for (entry_id, value) in new.i32s.iter() {
            if Some(value) != old.var_i32(entry_id) {
                i32s.get_mut_or_default(entry_id).push((id, value.clone()))
            }
        }

        for (entry_id, value) in new.strings.iter() {
            if Some(value) != old.strings.get(entry_id) {
                strings
                    .get_mut_or_default(entry_id)
                    .push((id, String::from(&**value)))
            }
        }
    }

    for (id, items) in bools {
        handler.send_req(RequestMsg::Edit(EditMsg::Bool(id, items)))?;
    }
    for (id, items) in i32s {
        handler.send_req(RequestMsg::Edit(EditMsg::I32(id, items)))?;
    }
    for (id, items) in strings {
        handler.send_req(RequestMsg::Edit(EditMsg::Str(id, items)))?;
    }
    Ok(())
}
