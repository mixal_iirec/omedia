use std::time::{Duration, Instant};

pub struct Cycler {
    period: Duration,
    last: Instant,
}

impl Cycler {
    pub fn from_millis(millis: u64) -> Self {
        Self {
            period: Duration::from_millis(millis),
            last: Instant::now(),
        }
    }

    pub fn time_for_next(&mut self) -> bool {
        if self.last.elapsed() >= self.period {
            self.last += self.period;
            true
        } else {
            false
        }
    }
}
