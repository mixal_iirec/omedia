use crate::ui::view::View;

use super::{BasicList, ListId, ListInner};
use crate::ui::handler::HandlerShareable;

pub struct Keymaplist {
    basic_list: BasicList,
    lines: Vec<(String, &'static str)>,
    max_width: usize,
}
impl_basic_list_deref!(Keymaplist);

impl Keymaplist {
    pub fn new(id: ListId, handler: &HandlerShareable) -> Self {
        let title = String::from("Controls");

        let keymapper = handler.keymapper();

        let mut lines = Vec::new();
        lines.push((String::from("Special Actions"), ""));
        lines.extend(keymapper.list_special_actions());
        lines.push((String::new(), ""));
        lines.push((String::from("Range Actions"), ""));
        lines.extend(keymapper.list_range_actions());
        lines.push((String::new(), ""));
        lines.push((String::from("Movements / Range Specifiers"), ""));
        lines.extend(keymapper.list_movement_actions());
        Self {
            basic_list: BasicList::new_up(id, title, lines.len(), false),
            max_width: lines
                .iter()
                .map(|(f, s)| if s.is_empty() { 0 } else { f.len() })
                .max()
                .unwrap_or(1),
            lines,
        }
    }
}

impl ListInner for Keymaplist {
    type Item = (String, &'static str);
    fn items(&self) -> &[Self::Item] {
        &self.lines
    }
    type ItemId = ();
    fn item_id(_: &Self::Item) -> Self::ItemId {
        ()
    }

    fn draw_idx(&self, view: &View, x: usize, idx: usize) {
        let s = &self.lines[idx];
        view.mvprint(0, x, &s.0);
        view.mvprint(0, x + self.max_width + 1, &s.1);
    }

    fn item_visibly_matches(&self, _: &HandlerShareable, idx: usize, regex: &regex::Regex) -> bool {
        let lines = &self.lines[idx];
        regex.is_match(&lines.0) || regex.is_match(&lines.1)
    }
}
