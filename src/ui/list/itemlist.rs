use rand::seq::SliceRandom;
use rand::thread_rng;

use std::cmp::Ordering;
use std::collections::{BTreeMap, BTreeSet};
use std::sync::Arc;

use crate::channel::{Channel, ChannelId};
use crate::entries::ENTRIES;
use crate::item::{ChItemId, Item, ItemIdLess};
use crate::msg::{EditMsg, ReqUpdateMsg, RequestMsg};
use crate::playlist::{Playlist, PlaylistIdLess};

use crate::ui::drawers::ItemDraw;
use crate::ui::drawers::{Drawer, DrawerType};
use crate::ui::general::{send_item_edit, smart_name_to_entryid, ItemEntryKind};
use crate::ui::handler::shareable::{Clipboard, HandlerShareable};
use crate::ui::key_mapper::{RangeAction, RangeActionMod, SpecialAction, SpecialActionMod};
use crate::ui::variable_editor::edit_var;
use crate::ui::view::View;
use crate::ui::{LocalUiMsg, UpdateMsg};

use super::{BasicList, ListId, ListInner};

#[derive(Debug, Copy, Clone)]
enum Sort {
    Random,
    Entry(ItemEntryKind, bool),
}

#[derive(Debug, Clone)]
enum PlaylistInfo {
    Normal(Arc<Playlist>),
    Custom(PlaylistIdLess),
}

pub struct Itemlist {
    basic_list: BasicList,

    channel: Option<ChannelId>,
    playlist_info: PlaylistInfo,
    items: Vec<(Item, Arc<Channel>)>,

    item_draw: ItemDraw,

    sort_by: Sort,
}
impl_basic_list_deref!(Itemlist);

impl Itemlist {
    fn title<'a>(
        playlist_info: &PlaylistInfo,
        items: impl IntoIterator<Item = &'a Item>,
    ) -> String {
        let playlist = match playlist_info {
            PlaylistInfo::Normal(playlist) => &**playlist,
            PlaylistInfo::Custom(playlist) => playlist,
        };

        let id = ENTRIES.i32_name_id("duration");
        let (len, sum) = items
            .into_iter()
            .map(|item| item.var_i32(id).cloned().unwrap_or(0) as i64)
            .fold((0, 0), |s, d| (s.0 + 1, s.1 + d));
        format!(
            "Playlist: {} | num: {len} | length: {}",
            playlist.name(),
            crate::ui::drawers::into_str::i64_as_reduced_hms(sum)
        )
    }

    pub fn new(
        id: ListId,
        playlist_info: Arc<Playlist>,
        items: Vec<Item>,
        handler: &HandlerShareable,
    ) -> Self {
        let channel = playlist_info.id().channel();
        let playlist_info = PlaylistInfo::Normal(playlist_info);
        let title = Self::title(&playlist_info, &items);

        let mut r = Self {
            basic_list: BasicList::new_down(id, title, items.len(), handler.config().enum_lists()),

            channel,
            playlist_info,
            items: super::map_items_with_channels(items, handler).collect(),

            item_draw: handler.item_draw().clone(),
            sort_by: Sort::Entry(ItemEntryKind::Date, true),
        };
        r.sort();
        r
    }

    fn update_after_item_change(&mut self) {
        self.title = Self::title(&self.playlist_info, self.items.iter().map(|(i, _)| i));
    }
}

impl ListInner for Itemlist {
    type Item = (Item, Arc<Channel>);
    fn items(&self) -> &[Self::Item] {
        &self.items
    }
    type ItemId = ChItemId;
    fn item_id(item: &Self::Item) -> Self::ItemId {
        item.0.id()
    }

    fn item_height(&self) -> usize {
        self.item_draw.height() as usize
    }

    fn sort(&mut self) {
        match self.sort_by {
            Sort::Random => self.items.shuffle(&mut thread_rng()),
            Sort::Entry(by, smallest_up) => {
                let cond_rev = if smallest_up {
                    |c| c
                } else {
                    |c: Ordering| c.reverse()
                };
                use ItemEntryKind::*;
                match by {
                    Date => self
                        .items
                        .sort_by(|(a, _), (b, _)| cond_rev(a.date().cmp(b.date()))),
                    Channel => self
                        .items
                        .sort_by(|(_, a), (_, b)| cond_rev(a.name().cmp(b.name()))),
                    Bool(id) => self
                        .items
                        .sort_by(|(a, _), (b, _)| cond_rev(a.var_bool(id).cmp(&b.var_bool(id)))),
                    I32(id) => self
                        .items
                        .sort_by(|(a, _), (b, _)| cond_rev(a.var_i32(id).cmp(&b.var_i32(id)))),
                    Str(id) => self.items.sort_by(|(a, _), (b, _)| {
                        cond_rev(a.var_string(id).cmp(&b.var_string(id)))
                    }),
                }
            }
        }
    }

    fn draw_idx(&self, view: &View, x: usize, idx: usize) {
        self.item_draw.draw_with_offset(view, &self.items[idx], x);
    }

    fn update(&mut self, handler: &HandlerShareable, update: UpdateMsg) -> crate::Result<()> {
        match update {
            UpdateMsg::Drawer(Drawer::Item(d)) => {
                self.item_draw = d;
                self.request_update(handler)?;
            }
            UpdateMsg::Backend(crate::msg::UpdateMsg::Items(items)) => self
                .preserve_marked_and_sort(|s| {
                    s.items = super::map_items_with_channels(items, handler).collect();
                    s.update_after_item_change();
                }),
            UpdateMsg::Backend(crate::msg::UpdateMsg::AddItems(items)) => self
                .preserve_marked_and_sort(|s| {
                    s.items
                        .extend(super::map_items_with_channels(items, handler));
                    s.update_after_item_change();
                }),
            _ => (),
        }
        Ok(())
    }

    fn request_update(&self, handler: &HandlerShareable) -> crate::Result<()> {
        let msg = match &self.playlist_info {
            PlaylistInfo::Normal(playlist) => ReqUpdateMsg::Playlist {
                id: playlist.id(),
                str_needed: self.item_draw.str_needed(),
            },
            PlaylistInfo::Custom(_) => ReqUpdateMsg::ItemsStrNeeded {
                items: self.items.iter().map(|(i, _)| i).cloned().collect(),
                str_needed: self.item_draw.str_needed(),
            },
        };
        handler.send_req(RequestMsg::RequestUpdate { list: self.id, msg })?;
        Ok(())
    }

    fn special_action_impl(
        &mut self,
        handler: &mut HandlerShareable,
        action: SpecialActionMod,
    ) -> crate::Result<()> {
        match action.typ {
            SpecialAction::Select => handler.send_req(RequestMsg::RequestPlayData {
                list: vec![self.selected_id()?],
            })?,
            SpecialAction::Add => {
                if let Some(channel) = self.channel {
                    let items = edit_var(handler.config().editor(), &vec![ItemIdLess::default()])?;
                    handler.send_req(RequestMsg::FetchedItems { channel, items })?;
                    self.request_update(handler)?;
                }
            }
            SpecialAction::TemporalPlaylist => {
                let playlist: PlaylistIdLess =
                    edit_var(handler.config().editor(), &PlaylistIdLess::default())?;
                self.playlist_info = PlaylistInfo::Custom(playlist.clone());

                let items = self.items.iter().map(|(i, _)| i.clone()).collect();

                let msg = ReqUpdateMsg::CustomPlaylist { items, playlist };

                handler.send_req(RequestMsg::RequestUpdate { list: self.id, msg })?;
            }
            SpecialAction::Return => handler.send_req(RequestMsg::RequestPlaylists {
                list: self.id,
                channel: self.channel,
            })?,
            SpecialAction::TabNewDrawerChoice => {
                handler.send_ui_msg(LocalUiMsg::OpenDrawerChoice(self.id, DrawerType::Item))
            }
            SpecialAction::PasteShallow => {
                if let Clipboard::Items(ids) = handler.clipboard() {
                    let mut ids = ids.clone();
                    let already: BTreeSet<_> = self.items.iter().map(|(i, _)| i.id()).collect();
                    ids.retain(|id| !already.contains(id));

                    let msg = ReqUpdateMsg::AddItemsFromIds {
                        items: ids,
                        str_needed: self.item_draw.str_needed(),
                    };
                    handler.send_req(RequestMsg::RequestUpdate { list: self.id, msg })?;
                }
            }

            SpecialAction::Sort { smallest_up } => {
                self.sort_by = match action.modifier.as_ref() {
                    "random" => Sort::Random,
                    name => Sort::Entry(smart_name_to_entryid(name)?, smallest_up),
                };
                self.preserve_marked_and_sort(|_| ());
            }
            SpecialAction::DrawCustom => {
                self.item_draw = ItemDraw::simple_custom(&action.modifier);
                self.request_update(handler)?;
            }
            _ => (),
        }
        Ok(())
    }

    fn item_visibly_matches(&self, _: &HandlerShareable, idx: usize, regex: &regex::Regex) -> bool {
        self.item_draw.visibly_matches(&self.items[idx], regex)
    }

    fn range_action_impl(
        &mut self,
        handler: &mut HandlerShareable,
        action: RangeActionMod,
        iter: Box<dyn Iterator<Item = usize> + 'static>,
    ) -> crate::Result<()> {
        match action.typ {
            RangeAction::DeleteDeep => {
                let ids = self.ids_iter(iter);
                handler.send_req(RequestMsg::DeleteItems { ids })?;
                self.request_update(handler)?;
            }
            RangeAction::DeleteMedia => {
                let ids = self.ids_iter(iter);
                handler.send_req(RequestMsg::DeleteItemMedia { ids })?;
                self.request_update(handler)?;
            }
            RangeAction::DeleteShallow => {
                let mut keep = vec![true; self.items.len()];
                iter.for_each(|idx| keep[idx] = false);

                self.preserve_marked_and_sort(|s| {
                    let mut idx = 0;
                    s.items.retain(|_| (keep[idx], idx += 1).0);
                });
            }
            RangeAction::Download { typ } => {
                let title_id = ENTRIES.string_name_id("title");
                let list = iter
                    .map(|idx| &self.items[idx])
                    .map(|(item, channel)| {
                        let title = match item.var_string(title_id) {
                            Some(title) => format!("Download of `{title}`"),
                            None => format!(
                                "Downloading #{} from channel {}",
                                item.item_id(),
                                channel.name()
                            ),
                        };
                        (item.id(), handler.add_new_progress(title, false))
                    })
                    .collect();
                handler.send_req(RequestMsg::DownloadItems { typ, list })?;
            }
            RangeAction::Play => handler.send_req(RequestMsg::RequestPlayData {
                list: self.ids_iter(iter),
            })?,
            RangeAction::Yank => {
                handler.set_clipboard(Clipboard::Items(self.ids_iter(iter)));
                handler.send_ui_msg(LocalUiMsg::Msg("Items copied!"));
            }
            RangeAction::SetBool { val } => {
                handler.send_req(RequestMsg::Edit(EditMsg::Bool(
                    ENTRIES
                        .bool_name_id_option(&action.modifier)
                        .ok_or(crate::Error::InvalidEntryName)?,
                    iter.map(|i| (self.items[i].0.id(), val)).collect(),
                )))?;
                self.request_update(handler)?;
            }
            RangeAction::Edit => {
                let edit_items: BTreeMap<usize, ItemIdLess> = edit_var(
                    handler.config().editor(),
                    &iter
                        .map(|idx| (idx, self.items[idx].0.id_less()))
                        .collect::<BTreeMap<_, _>>(),
                )?;

                send_item_edit(
                    handler,
                    edit_items.iter().map(|(idx, new_item)| {
                        let old_item = &self.items[*idx].0;
                        (old_item.id(), old_item.id_less(), new_item)
                    }),
                )?;

                self.request_update(handler)?;
            }
            RangeAction::Mark => unreachable!(),
        }
        Ok(())
    }
}
