use pancurses::*;
use regex::Regex;
use std::ops::DerefMut;
use std::sync::Arc;

use super::move_list::{Move, Movelist};
use crate::channel::{Channel, ChannelMap};

use crate::ui::handler::HandlerShareable;
use crate::ui::key_mapper::{
    Movement, MovementType, RangeAction, RangeActionMod, SpecialAction, SpecialActionMod,
};
use crate::ui::view::View;
use crate::ui::UpdateMsg;

pub use crate::general::{ListId, ListMap};

#[macro_use]
mod macros {
    macro_rules! impl_basic_list_deref{
		($ty:ty $(,$args: tt)*) => {
			impl<$($args,)*> std::ops::Deref for $ty{
				type Target = BasicList;
				fn deref(&self) -> &Self::Target{&self.basic_list}
			}
			impl<$($args,)*> std::ops::DerefMut for $ty{
				fn deref_mut(&mut self) -> &mut Self::Target{&mut self.basic_list}
			}
		}
	}
}
mod channellist;
mod downloadlist;
mod drawerlist;
mod itemlist;
mod keymaplist;
mod playlistlist;
mod progresslist;
mod textlist;
pub use channellist::Channellist;
pub use downloadlist::Downloadlist;
pub use drawerlist::Drawerlist;
pub use itemlist::Itemlist;
pub use keymaplist::Keymaplist;
pub use playlistlist::Playlistlist;
pub use progresslist::Progresslist;
pub use textlist::Textlist;

pub struct BasicList {
    id: ListId,

    movelist: Movelist,
    title: String,
    marked: Vec<bool>,

    enumerate: bool,
}

impl BasicList {
    fn new_up(id: ListId, title: String, len: usize, enumerate: bool) -> Self {
        Self {
            id,
            movelist: Movelist::new_up(len, 1),
            title,
            marked: vec![false; len],
            enumerate,
        }
    }

    fn new_down(id: ListId, title: String, len: usize, enumerate: bool) -> Self {
        Self {
            id,
            movelist: Movelist::new_down(len, 1),
            title,
            marked: vec![false; len],
            enumerate,
        }
    }

    pub fn id(&self) -> ListId {
        self.id.clone()
    }

    pub fn selected(&self) -> usize {
        self.movelist.selected()
    }
    pub fn toggle_enumerate(&mut self) {
        self.enumerate = !self.enumerate;
    }

    fn mark_one(&mut self) {
        let idx = self.movelist.selected();
        self.marked[idx] = !self.marked[idx];
    }

    fn marked<'a>(&'a self) -> impl DoubleEndedIterator<Item = usize> + 'a {
        self.marked
            .iter()
            .enumerate()
            .filter(|(_, m)| **m)
            .map(|(i, _)| i)
    }

    fn mark<I: IntoIterator<Item = usize>>(&mut self, it: I) {
        for i in it {
            self.marked[i] = !self.marked[i];
        }
    }

    fn movement(&mut self, movement: Movement) {
        match movement.typ {
            MovementType::Up => self.movelist.mv(Move::Up(movement.repeat)),
            MovementType::Down => self.movelist.mv(Move::Down(movement.repeat)),
            MovementType::PageUp => self.movelist.mv(Move::PageUp(movement.repeat)),
            MovementType::PageDown => self.movelist.mv(Move::PageDown(movement.repeat)),
            MovementType::Begin => self.movelist.mv(Move::Begin),
            MovementType::End => self.movelist.mv(Move::End),
            MovementType::Goto => self.movelist.mv(Move::To(movement.repeat)),
            _ => (),
        }
    }

    fn draw<D>(&self, view: &View, d: D, len: usize, selected_list: bool)
    where
        D: Fn(&View, usize, usize),
    {
        let title_view = view.get(0).unwrap();
        title_view.mvprint(0, 0, &self.title);
        title_view.draw_line(1, "─");

        let enum_width = format!("{}", self.movelist.size()).len(); //all ASCII

        let list_view = view.get(1).unwrap();
        for (i, view) in (self.movelist.offset()..len).zip(list_view.iter()) {
            let attr = if let Some(true) = self.marked.get(i) {
                COLOR_PAIR(1)
            } else {
                0
            } | if i == self.selected() {
                A_ITALIC | if selected_list { A_BOLD } else { 0 }
            } else {
                0
            };

            view.with_attrs(attr, |view| {
                if self.enumerate {
                    d(view, enum_width + 1, i);
                    view.draw_column(enum_width, "│");
                    let num = format!("{}", i);
                    view.mvprint(0, enum_width - num.len(), &num);
                } else {
                    d(view, 0, i);
                }
            });
        }
    }
}

trait ListInner: DerefMut<Target = BasicList> {
    type Item;
    fn items(&self) -> &[Self::Item];
    type ItemId: Ord;
    fn item_id(item: &Self::Item) -> Self::ItemId;

    fn selected(&self) -> crate::Result<&Self::Item> {
        Ok(self
            .items()
            .get(self.deref().selected())
            .ok_or(crate::Error::InvalidId)?)
    }

    fn selected_id(&self) -> crate::Result<Self::ItemId> {
        Ok(Self::item_id(self.selected()?))
    }

    fn ids_iter(&self, iter: impl Iterator<Item = usize>) -> Vec<Self::ItemId> {
        iter.map(|i| Self::item_id(&self.items()[i])).collect()
    }

    fn preserve_marked_and_sort(&mut self, f: impl FnOnce(&mut Self)) {
        let marked: std::collections::BTreeSet<Self::ItemId> = self
            .marked
            .iter()
            .zip(self.items())
            .filter(|(m, _)| **m)
            .map(|(_, p)| p)
            .map(Self::item_id)
            .collect();
        f(self);
        self.sort();
        let len = self.items().len();
        self.movelist.change_size(len);
        self.marked = self
            .items()
            .iter()
            .map(|p| marked.contains(&Self::item_id(p)))
            .collect();
    }

    fn item_height(&self) -> usize {
        1
    }

    fn sort(&mut self) {}

    fn restructure(&mut self, view: &mut View) {
        view.split_vertical_by(vec![0, 2]);
        let list_view = view.get_mut(1).unwrap();
        let vis = list_view.y() / self.item_height();
        self.movelist.set_visibility(vis);
        list_view.split_vertical_with_size(self.item_height());
    }

    fn draw_idx(&self, _view: &View, _offset_x: usize, _idx: usize) {}
    fn draw(&self, _: &HandlerShareable, view: &View, selected_list: bool) {
        self.deref().draw(
            view,
            |view, offset_x, idx| self.draw_idx(view, offset_x, idx),
            self.items().len(),
            selected_list,
        );
    }

    fn update(&mut self, _handler: &HandlerShareable, _update: UpdateMsg) -> crate::Result<()> {
        Ok(())
    }
    fn update_channels(&mut self, _channels: &ChannelMap<Arc<Channel>>) {}
    fn set_drawer(&mut self, _drawer: crate::ui::drawers::Drawer) {}
    fn request_update(&self, _handler: &HandlerShareable) -> crate::Result<()> {
        Ok(())
    }
    fn dependent_on(&self, _list: ListId) -> bool {
        false
    }

    fn special_action_impl(
        &mut self,
        _handler: &mut HandlerShareable,
        _action: SpecialActionMod,
    ) -> crate::Result<()> {
        Ok(())
    }
    fn special_action(
        &mut self,
        handler: &mut HandlerShareable,
        action: SpecialActionMod,
    ) -> crate::Result<()> {
        match action.typ {
            SpecialAction::MarkOne => self.mark_one(),
            _ => self.special_action_impl(handler, action)?,
        }
        Ok(())
    }

    fn range_action_impl(
        &mut self,
        _: &mut HandlerShareable,
        _: RangeActionMod,
        _: Box<dyn Iterator<Item = usize> + 'static>,
    ) -> crate::Result<()> {
        Ok(())
    }

    fn range_action(
        &mut self,
        handler: &mut HandlerShareable,
        movement: Movement,
        action: Option<RangeActionMod>,
    ) -> crate::Result<()> {
        let iter = self.move_and_act_on(handler, movement, action.is_some())?;
        if let Some(action) = action {
            match action.typ {
                RangeAction::Mark => self.mark(iter),
                _ => self.range_action_impl(handler, action, iter)?,
            }
        }
        Ok(())
    }

    fn item_visibly_matches(&self, _: &HandlerShareable, _idx: usize, _: &Regex) -> bool {
        false
    }

    fn move_and_act_on(
        &mut self,
        handler: &HandlerShareable,
        movement: Movement,
        dummy: bool,
    ) -> crate::Result<Box<dyn Iterator<Item = usize> + 'static>> {
        let old_selected = self.deref().selected();
        Ok(match movement.typ {
            MovementType::Marked => Box::new(self.marked().collect::<Vec<_>>().into_iter()),
            MovementType::Search { forward } => {
                let regex = Regex::new(&movement.modifier)?;
                let vec: Vec<_> = if forward {
                    (old_selected + if dummy { 0 } else { 1 }..self.movelist.size())
                        .filter(|idx| self.item_visibly_matches(handler, *idx, &regex))
                        .take(movement.repeat)
                        .collect()
                } else {
                    (0..old_selected.saturating_sub(if dummy { 1 } else { 0 }))
                        .rev()
                        .filter(|idx| self.item_visibly_matches(handler, *idx, &regex))
                        .take(movement.repeat)
                        .collect()
                };
                if !dummy {
                    if let Some(last) = vec.last() {
                        self.movelist.set_selected(*last)
                    }
                }
                Box::new(vec.into_iter())
            }
            _ => {
                self.movement(movement);
                let new_selected = self.deref().selected();
                if dummy {
                    self.movelist.set_selected(old_selected);
                }
                Box::new(if new_selected > old_selected {
                    old_selected..new_selected + 1
                } else {
                    new_selected..old_selected + 1
                })
            }
        })
    }
}

//used for both itemlist and dowloadlist
use crate::item::Item;
fn map_items_with_channels(
    it: Vec<Item>,
    handler: &HandlerShareable,
) -> impl Iterator<Item = (Item, Arc<Channel>)> + '_ {
    it.into_iter().filter_map(move |i| {
        let channel = Arc::clone(handler.channels_map().get(i.channel_id())?);
        Some((i, channel))
    })
}

pub(in crate::ui) trait List {
    fn id(&self) -> ListId;
    fn toggle_enumerate(&mut self);
    fn restructure(&mut self, view: &mut View);
    fn draw(&self, handler: &HandlerShareable, view: &View, selected_list: bool);

    fn update(&mut self, _handler: &HandlerShareable, _update: UpdateMsg) -> crate::Result<()>;
    fn update_channels(&mut self, _channels: &ChannelMap<Arc<Channel>>);
    fn set_drawer(&mut self, _drawer: crate::ui::drawers::Drawer);
    fn request_update(&self, _handler: &HandlerShareable) -> crate::Result<()>;
    fn dependent_on(&self, _list: ListId) -> bool;

    fn special_action(
        &mut self,
        _handler: &mut HandlerShareable,
        _action: SpecialActionMod,
    ) -> crate::Result<()>;

    fn range_action(
        &mut self,
        handler: &mut HandlerShareable,
        movement: Movement,
        action: Option<RangeActionMod>,
    ) -> crate::Result<()>;
}

impl<L: ListInner> List for L {
    fn id(&self) -> ListId {
        self.deref().id()
    }
    fn toggle_enumerate(&mut self) {
        self.deref_mut().toggle_enumerate()
    }
    fn restructure(&mut self, view: &mut View) {
        ListInner::restructure(self, view)
    }
    fn draw(&self, handler: &HandlerShareable, view: &View, selected_list: bool) {
        ListInner::draw(self, handler, view, selected_list)
    }

    fn update(&mut self, handler: &HandlerShareable, update: UpdateMsg) -> crate::Result<()> {
        ListInner::update(self, handler, update)
    }
    fn update_channels(&mut self, channels: &ChannelMap<Arc<Channel>>) {
        ListInner::update_channels(self, channels)
    }
    fn set_drawer(&mut self, drawer: crate::ui::drawers::Drawer) {
        ListInner::set_drawer(self, drawer)
    }
    fn request_update(&self, handler: &HandlerShareable) -> crate::Result<()> {
        ListInner::request_update(self, handler)
    }
    fn dependent_on(&self, list: ListId) -> bool {
        ListInner::dependent_on(self, list)
    }

    fn special_action(
        &mut self,
        handler: &mut HandlerShareable,
        action: SpecialActionMod,
    ) -> crate::Result<()> {
        ListInner::special_action(self, handler, action)
    }

    fn range_action(
        &mut self,
        handler: &mut HandlerShareable,
        movement: Movement,
        action: Option<RangeActionMod>,
    ) -> crate::Result<()> {
        ListInner::range_action(self, handler, movement, action)
    }
}
