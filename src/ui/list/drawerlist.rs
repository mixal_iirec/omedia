use serde::de::DeserializeOwned;

use std::path::Path;

use crate::config::Config;

use super::{BasicList, ListId, ListInner};
use crate::ui::drawers::{Drawer, PropertyDrawer};
use crate::ui::handler::HandlerShareable;
use crate::ui::key_mapper::{SpecialAction, SpecialActionMod};
use crate::ui::view::View;
use crate::ui::{LocalUiMsg, UpdateMsg};

pub struct Drawerlist<D> {
    basic_list: BasicList,

    change: ListId,
    drawers: Vec<D>,
}
impl_basic_list_deref!(Drawerlist<D>, D);

impl<T: crate::ui::drawers::Name + DeserializeOwned> Drawerlist<PropertyDrawer<T>> {
    pub fn new(id: ListId, change: ListId, dir: &Path, config: &Config) -> crate::Result<Self> {
        let title = format!("Choice of {} drawers", T::name());
        let drawers: Vec<_> = crate::files::read_dir(dir)?
            .filter_map(|path| crate::files::yaml_from_file(&path.path()).ok())
            .collect();
        Ok(Self {
            basic_list: BasicList::new_up(id, title, 0, config.enum_lists()),

            change,
            drawers,
        })
    }
}

impl<T: Clone + 'static> ListInner for Drawerlist<PropertyDrawer<T>>
where
    PropertyDrawer<T>: Into<Drawer>,
{
    type Item = PropertyDrawer<T>;
    fn items(&self) -> &[Self::Item] {
        &self.drawers
    }
    type ItemId = ();
    fn item_id(_: &Self::Item) -> Self::ItemId {
        ()
    }

    fn draw_idx(&self, view: &View, x: usize, idx: usize) {
        view.mvprint(0, x, self.drawers[idx].name());
    }

    fn dependent_on(&self, list: ListId) -> bool {
        self.change == list
    }

    fn item_visibly_matches(&self, _: &HandlerShareable, _: usize, _: &regex::Regex) -> bool {
        //todo
        false
    }

    fn special_action_impl(
        &mut self,
        handler: &mut HandlerShareable,
        action: SpecialActionMod,
    ) -> crate::Result<()> {
        match action.typ {
            SpecialAction::Select => {
                let drawer = self.selected()?.clone().into();
                handler.send_ui_msg(LocalUiMsg::Update(self.change, UpdateMsg::Drawer(drawer)));
                handler.send_ui_msg(LocalUiMsg::CloseTab(self.id));
            }
            _ => (),
        }
        Ok(())
    }
}
