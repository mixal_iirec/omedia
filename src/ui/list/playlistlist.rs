use std::sync::Arc;

use crate::channel::Channel;
use crate::msg::{EditMsg, ReqUpdateMsg, RequestMsg};
use crate::playlist::{Playlist, PlaylistId, PlaylistIdLess};

use crate::ui::drawers::{Drawer, DrawerType, PlaylistDraw};
use crate::ui::handler::shareable::{Clipboard, HandlerShareable};
use crate::ui::key_mapper::{RangeAction, RangeActionMod, SpecialAction, SpecialActionMod};
use crate::ui::variable_editor::{edit_var, edit_var_list};
use crate::ui::view::View;
use crate::ui::{LocalUiMsg, UpdateMsg};

use super::{BasicList, ListId, ListInner};

pub struct Playlistlist {
    basic_list: BasicList,

    channel: Option<Arc<Channel>>,
    playlists: Vec<Arc<Playlist>>,

    playlist_draw: PlaylistDraw,
}
impl_basic_list_deref!(Playlistlist);

impl Playlistlist {
    pub fn new(
        id: ListId,
        channel: Option<Arc<Channel>>,
        playlists: Vec<Arc<Playlist>>,
        handler: &HandlerShareable,
    ) -> Self {
        let title = match &channel {
            Some(channel) => format!("Playlists on channel: {}", channel.name()),
            None => String::from("Global playlists"),
        };

        let enum_lists = handler.config().enum_lists();
        let mut r = Self {
            basic_list: BasicList::new_up(id, title, playlists.len(), enum_lists),

            channel,
            playlists,

            playlist_draw: handler.playlist_draw().clone(),
        };
        r.sort();
        r
    }
}

impl ListInner for Playlistlist {
    type Item = Arc<Playlist>;
    fn items(&self) -> &[Self::Item] {
        &self.playlists
    }
    type ItemId = PlaylistId;
    fn item_id(item: &Self::Item) -> Self::ItemId {
        item.id()
    }

    fn item_height(&self) -> usize {
        self.playlist_draw.height() as usize
    }

    fn sort(&mut self) {
        self.playlists.sort_by(|a, b| a.name().cmp(b.name()));
    }

    fn draw_idx(&self, view: &View, x: usize, idx: usize) {
        self.playlist_draw
            .draw_with_offset(view, &self.playlists[idx], x);
    }

    fn update(&mut self, _: &HandlerShareable, update: UpdateMsg) -> crate::Result<()> {
        match update {
            UpdateMsg::Drawer(Drawer::Playlist(d)) => self.playlist_draw = d,
            UpdateMsg::Backend(crate::msg::UpdateMsg::Playlists(playlists)) => {
                self.preserve_marked_and_sort(|s| s.playlists = playlists)
            }
            _ => (),
        }
        Ok(())
    }

    fn request_update(&self, handler: &HandlerShareable) -> crate::Result<()> {
        handler.send_req(RequestMsg::RequestUpdate {
            list: self.id,
            msg: ReqUpdateMsg::Playlists {
                channel: self.channel.as_ref().map(|c| c.id()),
            },
        })
    }

    fn special_action_impl(
        &mut self,
        handler: &mut HandlerShareable,
        action: SpecialActionMod,
    ) -> crate::Result<()> {
        match action.typ {
            SpecialAction::Select => {
                let str_needed = handler.item_draw().str_needed();
                handler.send_req(RequestMsg::RequestPlaylist {
                    list: self.id,
                    id: self.selected_id()?,
                    str_needed,
                })?;
            }
            SpecialAction::Add => {
                let playlists =
                    edit_var(handler.config().editor(), &vec![PlaylistIdLess::default()])?;
                let channel = self.channel.as_ref().map(|c| c.id());
                handler.send_req(RequestMsg::AddPlaylists { channel, playlists })?;
                self.request_update(handler)?;
            }
            SpecialAction::Return => {
                if self.channel.is_some() {
                    handler.send_ui_msg(LocalUiMsg::SetChannels(self.id));
                }
            }
            SpecialAction::TabNewDrawerChoice => {
                handler.send_ui_msg(LocalUiMsg::OpenDrawerChoice(self.id, DrawerType::Playlist))
            }
            SpecialAction::PasteDeep => match &handler.clipboard() {
                Clipboard::Playlists(playlists) => {
                    handler.send_req(RequestMsg::CopyPlaylists {
                        ids: playlists.clone(),
                        into: self.channel.as_ref().map(|c| c.id()),
                    })?;
                    self.request_update(handler)?;
                }
                Clipboard::Items(items) => {
                    if let Some(channel) = &self.channel {
                        handler.send_req(RequestMsg::CopyItems {
                            ids: items.clone(),
                            into: channel.id(),
                        })?;
                        self.request_update(handler)?;
                    }
                }
                _ => (),
            },
            SpecialAction::DrawCustom => {
                self.playlist_draw = PlaylistDraw::simple_custom(&action.modifier)
            }
            _ => (),
        }
        Ok(())
    }

    fn item_visibly_matches(&self, _: &HandlerShareable, idx: usize, regex: &regex::Regex) -> bool {
        self.playlist_draw
            .visibly_matches(&self.playlists[idx], regex)
    }

    fn range_action_impl(
        &mut self,
        handler: &mut HandlerShareable,
        action: RangeActionMod,
        iter: Box<dyn Iterator<Item = usize> + 'static>,
    ) -> crate::Result<()> {
        match action.typ {
            RangeAction::DeleteDeep => {
                let ids = self.ids_iter(iter);
                handler.send_req(RequestMsg::DeletePlaylists { ids })?;
                self.request_update(handler)?;
            }
            RangeAction::DeleteShallow => {
                let mut keep = vec![true; self.playlists.len()];
                iter.for_each(|idx| keep[idx] = false);

                self.preserve_marked_and_sort(|s| {
                    let mut idx = 0;
                    s.playlists.retain(|_| (keep[idx], idx += 1).0);
                });
            }
            RangeAction::Yank => {
                handler.set_clipboard(Clipboard::Playlists(self.ids_iter(iter)));
                handler.send_ui_msg(LocalUiMsg::Msg("Playlists copied!"));
            }
            RangeAction::Edit => {
                let edit_playlists = edit_var_list(
                    handler.config().editor(),
                    &self.playlists,
                    iter,
                    |item| item.id(),
                    |item| item.id_less(),
                )?;
                handler.send_req(RequestMsg::Edit(EditMsg::Playlist(edit_playlists)))?;
                self.request_update(handler)?;
            }
            _ => (),
        }
        Ok(())
    }
}
