use regex::Regex;

use crate::ui::key_mapper::{Movement, RangeActionMod, SpecialActionMod};
use crate::ui::view::View;

use super::{BasicList, ListId, ListInner};
use crate::ui::handler::HandlerShareable;

pub struct Progresslist {
    basic_list: BasicList,
}
impl_basic_list_deref!(Progresslist);

impl Progresslist {
    pub fn new(id: ListId, enum_lists: bool) -> Self {
        let title = format!("All progress bars (unordered)");
        Self {
            basic_list: BasicList::new_up(id, title, 0, enum_lists),
        }
    }
}

impl ListInner for Progresslist {
    type Item = ();
    fn items(&self) -> &[Self::Item] {
        &[]
    }
    type ItemId = ();
    fn item_id(_: &Self::Item) -> Self::ItemId {
        ()
    }
    fn draw(&self, handler: &HandlerShareable, view: &View, selected_list: bool) {
        let vec: Vec<_> = handler.progresses().values().collect();
        self.basic_list.draw(
            view,
            |view, x, idx| view.draw_progress_with_offset(0, &vec[idx], x),
            vec.len(),
            selected_list,
        );
    }

    fn item_visibly_matches(&self, handler: &HandlerShareable, idx: usize, regex: &Regex) -> bool {
        let title = handler.progresses().values().nth(idx).map(|p| p.title());
        regex.is_match(title.unwrap_or(""))
    }

    // disable marking
    fn special_action(
        &mut self,
        _: &mut HandlerShareable,
        _: SpecialActionMod,
    ) -> crate::Result<()> {
        Ok(())
    }

    // disable marking, and change_size before moving
    fn range_action(
        &mut self,
        handler: &mut HandlerShareable,
        movement: Movement,
        action: Option<RangeActionMod>,
    ) -> crate::Result<()> {
        self.movelist
            .change_size(handler.progresses().iter().count());
        let _ = self.move_and_act_on(handler, movement, action.is_some())?;
        Ok(())
    }
}
