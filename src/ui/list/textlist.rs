use crate::entries::ENTRIES;
use crate::ui::handler::HandlerShareable;
use crate::ui::view::View;

use super::{BasicList, ListId, ListInner};

pub struct Textlist {
    basic_list: BasicList,
    lines: Vec<String>,
}
impl_basic_list_deref!(Textlist);

impl Textlist {
    pub fn entry_list(id: ListId) -> Self {
        let title = String::from("Entry names");

        let mut lines = Vec::new();
        lines.push(String::from("Strings"));
        lines.extend(ENTRIES.string_name_list().iter().map(|s| format!("-{s}")));
        lines.push(String::new());
        lines.push(String::from("i32s"));
        lines.extend(ENTRIES.i32_name_list().iter().map(|s| format!("-{s}")));
        lines.push(String::new());
        lines.push(String::from("Bools"));
        lines.extend(ENTRIES.bool_name_list().iter().map(|s| format!("-{s}")));
        Self {
            basic_list: BasicList::new_up(id, title, 0, false),
            lines,
        }
    }
}

impl ListInner for Textlist {
    type Item = String;
    fn items(&self) -> &[Self::Item] {
        &self.lines
    }
    type ItemId = ();
    fn item_id(_: &Self::Item) -> Self::ItemId {
        ()
    }

    fn draw_idx(&self, view: &View, x: usize, idx: usize) {
        view.mvprint(0, x, &self.lines[idx]);
    }

    fn item_visibly_matches(&self, _: &HandlerShareable, idx: usize, regex: &regex::Regex) -> bool {
        regex.is_match(&self.lines[idx])
    }
}
