use std::sync::Arc;

use crate::channel::Channel;
use crate::entries::ENTRIES;
use crate::item::{ChItemId, Item};
use crate::msg::{ReqUpdateMsg, RequestMsg};

use crate::ui::drawers::{Drawer, DrawerType, ItemDraw};
use crate::ui::handler::HandlerShareable;
use crate::ui::key_mapper::{RangeAction, RangeActionMod, SpecialAction, SpecialActionMod};
use crate::ui::view::View;
use crate::ui::{LocalUiMsg, UpdateMsg};

use super::{BasicList, ListId, ListInner};

pub struct Downloadlist {
    basic_list: BasicList,

    items: Vec<(Item, Arc<Channel>)>,
    done: usize,

    item_draw: ItemDraw,
}
impl_basic_list_deref!(Downloadlist);

impl Downloadlist {
    pub fn new(id: ListId, items: Vec<Item>, done: usize, handler: &HandlerShareable) -> Self {
        let duration_id = ENTRIES.i32_name_id("duration");
        let title = format!(
            "Dowloads | num: {} | length: {}s",
            items.len(),
            items
                .iter()
                .map(|item| item.var_i32(duration_id).cloned().unwrap_or(0) as i64)
                .sum::<i64>()
        );

        Self {
            basic_list: BasicList::new_down(id, title, items.len(), handler.config().enum_lists()),

            items: super::map_items_with_channels(items, handler).collect(),
            done,

            item_draw: handler.item_draw().clone(),
        }
    }
}

impl ListInner for Downloadlist {
    type Item = (Item, Arc<Channel>);
    fn items(&self) -> &[Self::Item] {
        &self.items
    }
    type ItemId = ChItemId;
    fn item_id(item: &Self::Item) -> Self::ItemId {
        item.0.id()
    }

    fn item_height(&self) -> usize {
        self.item_draw.height() as usize
    }

    fn draw_idx(&self, view: &View, x: usize, idx: usize) {
        self.item_draw
            .draw_with_offset(view, &self.items[idx], x + 2);
        view.draw_column(x + 1, "│");
        let symbol = if idx < self.done {
            "✓"
        } else if idx > self.done {
            ""
        } else {
            "⋯"
        };
        view.mvprint(0, x, symbol);
    }

    fn update(&mut self, handler: &HandlerShareable, update: UpdateMsg) -> crate::Result<()> {
        match update {
            UpdateMsg::Drawer(Drawer::Item(d)) => {
                self.item_draw = d;
                self.request_update(handler)?;
            }
            UpdateMsg::Backend(crate::msg::UpdateMsg::DownloadList { items, done }) => self
                .preserve_marked_and_sort(|s| {
                    s.items = super::map_items_with_channels(items, handler).collect();
                    s.done = done;
                }),
            _ => (),
        }
        Ok(())
    }

    fn request_update(&self, handler: &HandlerShareable) -> crate::Result<()> {
        let str_needed = self.item_draw.str_needed();
        let msg = ReqUpdateMsg::DownloadList { str_needed };
        handler.send_req(RequestMsg::RequestUpdate { list: self.id, msg })
    }

    fn special_action_impl(
        &mut self,
        handler: &mut HandlerShareable,
        action: SpecialActionMod,
    ) -> crate::Result<()> {
        match action.typ {
            SpecialAction::TabNewDrawerChoice => {
                handler.send_ui_msg(LocalUiMsg::OpenDrawerChoice(self.id, DrawerType::Item))
            }
            SpecialAction::DrawCustom => {
                self.item_draw = ItemDraw::simple_custom(&action.modifier);
                self.request_update(handler)?;
            }
            _ => (),
        }
        Ok(())
    }

    fn item_visibly_matches(&self, _: &HandlerShareable, idx: usize, regex: &regex::Regex) -> bool {
        self.item_draw.visibly_matches(&self.items[idx], regex)
    }

    fn range_action_impl(
        &mut self,
        handler: &mut HandlerShareable,
        action: RangeActionMod,
        iter: Box<dyn Iterator<Item = usize> + 'static>,
    ) -> crate::Result<()> {
        match action.typ {
            RangeAction::DeleteShallow => {
                let ids = self.ids_iter(iter);
                handler.send_req(RequestMsg::AbortDownload { ids })?;
                self.request_update(handler)?;
            }
            _ => (),
        }
        Ok(())
    }
}
