use std::sync::Arc;

use crate::channel::{Channel, ChannelId, ChannelIdLess, ChannelMap};
use crate::msg::{EditMsg, RequestMsg};

use crate::ui::drawers::ChannelDraw;
use crate::ui::drawers::{Drawer, DrawerType};
use crate::ui::handler::HandlerShareable;
use crate::ui::key_mapper::{RangeAction, RangeActionMod, SpecialAction, SpecialActionMod};
use crate::ui::variable_editor::{edit_var, edit_var_list};
use crate::ui::view::View;
use crate::ui::{LocalUiMsg, UpdateMsg};

use super::{BasicList, ListId, ListInner};

pub struct Channellist {
    basic_list: BasicList,

    channels: Vec<Arc<Channel>>,
    channel_draw: ChannelDraw,
}
impl_basic_list_deref!(Channellist);

impl Channellist {
    pub fn new(id: ListId, handler: &HandlerShareable) -> Self {
        let channels: Vec<_> = handler.channels_map().values().cloned().collect();
        let title = String::from("Channel list");
        let mut r = Self {
            basic_list: BasicList::new_up(id, title, channels.len(), handler.config().enum_lists()),

            channels,
            channel_draw: handler.channel_draw().clone(),
        };
        r.sort();
        r
    }
}

impl ListInner for Channellist {
    type Item = Arc<Channel>;
    fn items(&self) -> &[Self::Item] {
        &self.channels
    }
    type ItemId = ChannelId;
    fn item_id(item: &Self::Item) -> Self::ItemId {
        item.id()
    }

    fn item_height(&self) -> usize {
        self.channel_draw.height() as usize
    }

    fn sort(&mut self) {
        self.channels.sort_by(|a, b| a.name().cmp(b.name()));
    }

    fn draw_idx(&self, view: &View, x: usize, idx: usize) {
        self.channel_draw
            .draw_with_offset(view, &self.channels[idx], x);
    }

    fn update(&mut self, _: &HandlerShareable, update: UpdateMsg) -> crate::Result<()> {
        if let UpdateMsg::Drawer(Drawer::Channel(d)) = update {
            self.channel_draw = d;
        }
        Ok(())
    }
    fn update_channels(&mut self, channels: &ChannelMap<Arc<Channel>>) {
        self.preserve_marked_and_sort(|s| s.channels = channels.values().cloned().collect());
    }

    fn request_update(&self, handler: &HandlerShareable) -> crate::Result<()> {
        handler.send_req(RequestMsg::RequestChannels)
    }

    fn special_action_impl(
        &mut self,
        handler: &mut HandlerShareable,
        action: SpecialActionMod,
    ) -> crate::Result<()> {
        match action.typ {
            SpecialAction::Select => handler.send_req(RequestMsg::RequestPlaylists {
                list: self.id,
                channel: Some(self.selected_id()?),
            })?,
            SpecialAction::Add => {
                let channels =
                    edit_var(handler.config().editor(), &vec![ChannelIdLess::default()])?;
                handler.send_req(RequestMsg::AddChannels { channels })?;
                self.request_update(handler)?;
            }
            SpecialAction::TabNewDrawerChoice => {
                handler.send_ui_msg(LocalUiMsg::OpenDrawerChoice(self.id, DrawerType::Channel))
            }
            SpecialAction::DrawCustom => {
                self.channel_draw = ChannelDraw::simple_custom(&action.modifier)
            }
            _ => (),
        }
        Ok(())
    }

    fn item_visibly_matches(&self, _: &HandlerShareable, idx: usize, regex: &regex::Regex) -> bool {
        self.channel_draw
            .visibly_matches(&self.channels[idx], regex)
    }

    fn range_action_impl(
        &mut self,
        handler: &mut HandlerShareable,
        action: RangeActionMod,
        iter: Box<dyn Iterator<Item = usize> + 'static>,
    ) -> crate::Result<()> {
        match action.typ {
            RangeAction::DeleteDeep => {
                let channels = self.ids_iter(iter);
                handler.send_req(RequestMsg::DeleteChannels { channels })?;
                handler.send_req(RequestMsg::RequestChannels)?;
            }
            RangeAction::DeleteShallow => {
                let mut keep = vec![true; self.channels.len()];
                iter.for_each(|idx| keep[idx] = false);

                self.preserve_marked_and_sort(|s| {
                    let mut idx = 0;
                    s.channels.retain(|_| (keep[idx], idx += 1).0);
                });
            }
            RangeAction::Edit => {
                let edit_channels = edit_var_list(
                    handler.config().editor(),
                    &self.channels,
                    iter,
                    |item| item.id(),
                    |item| item.id_less(),
                )?;

                handler.send_req(RequestMsg::Edit(EditMsg::Channel(edit_channels)))?;
                self.request_update(handler)?;
            }
            _ => (),
        }
        Ok(())
    }
}
