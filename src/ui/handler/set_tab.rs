use std::path::PathBuf;
use std::sync::Arc;

use crate::channel::Channel;
use crate::item::Item;
use crate::playlist::Playlist;
use crate::ui::drawers::{ChannelDraw, DrawerType, ItemDraw, PlaylistDraw};
use crate::ui::list::*;

use super::Handler;

impl Handler {
    pub fn set_channel_tab(&mut self, tab: Option<ListId>) {
        let tab = tab.unwrap_or_else(|| self.tabs.new_id());
        self.set_tab(tab, Box::new(Channellist::new(tab, &self.shareable)));
    }

    pub fn set_playlist_tab(
        &mut self,
        tab: Option<ListId>,
        channel: Option<Arc<Channel>>,
        playlists: Vec<Arc<Playlist>>,
    ) {
        let tab = tab.unwrap_or_else(|| self.tabs.new_id());
        self.set_tab(
            tab,
            Box::new(Playlistlist::new(tab, channel, playlists, &self.shareable)),
        );
    }

    pub fn set_item_tab(&mut self, tab: Option<ListId>, playlist: Arc<Playlist>, items: Vec<Item>) {
        let tab = tab.unwrap_or_else(|| self.tabs.new_id());
        self.set_tab(
            tab,
            Box::new(Itemlist::new(tab, playlist, items, &self.shareable)),
        );
    }

    pub fn set_download_tab(&mut self, tab: Option<ListId>, items: Vec<Item>, done: usize) {
        let tab = tab.unwrap_or_else(|| self.tabs.new_id());
        self.set_tab(
            tab,
            Box::new(Downloadlist::new(tab, items, done, &self.shareable)),
        );
    }

    pub fn set_progress_tab(&mut self, tab: Option<ListId>) {
        let tab = tab.unwrap_or_else(|| self.tabs.new_id());
        self.set_tab(
            tab,
            Box::new(Progresslist::new(tab, self.shareable.config().enum_lists())),
        );
    }

    pub fn set_help_tab(&mut self, tab: Option<ListId>) {
        let tab = tab.unwrap_or_else(|| self.tabs.new_id());
        self.set_tab(tab, Box::new(Keymaplist::new(tab, &self.shareable)));
    }

    pub fn set_entry_tab(&mut self, tab: Option<ListId>) {
        let tab = tab.unwrap_or_else(|| self.tabs.new_id());
        self.set_tab(tab, Box::new(Textlist::entry_list(tab)));
    }

    pub fn add_drawer_choice_tab(&mut self, list: ListId, typ: DrawerType) -> crate::Result<()> {
        let id = self.tabs.new_id();
        self.set_tab(
            id,
            match typ {
                DrawerType::Item => Box::new(Drawerlist::<ItemDraw>::new(
                    id,
                    list,
                    &PathBuf::from("drawers/item/"),
                    self.config(),
                )?),
                DrawerType::Playlist => Box::new(Drawerlist::<PlaylistDraw>::new(
                    id,
                    list,
                    &PathBuf::from("drawers/playlist/"),
                    self.config(),
                )?),
                DrawerType::Channel => Box::new(Drawerlist::<ChannelDraw>::new(
                    id,
                    list,
                    &PathBuf::from("drawers/channel/"),
                    self.config(),
                )?),
            },
        );
        Ok(())
    }
}
