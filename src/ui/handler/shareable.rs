use std::collections::VecDeque;
use std::path::Path;
use std::sync::mpsc::Sender;
use std::sync::Arc;

use crate::channel::{Channel, ChannelMap};
use crate::config::Config;
use crate::general::progress::{Progress, ProgressId, ProgressMap, ProgressMsg};
use crate::general::Id;
use crate::item::ChItemId;
use crate::msg::RequestMsg;
use crate::playlist::PlaylistId;

use crate::files::yaml_from_file;

use crate::ui::cycler::Cycler;
use crate::ui::drawers::{ChannelDraw, Drawer, ItemDraw, PlaylistDraw};
use crate::ui::key_mapper::KeyMapper;
use crate::ui::LocalUiMsg;

pub enum Clipboard {
    Items(Vec<ChItemId>),
    Playlists(Vec<PlaylistId>),
    None,
}

pub struct HandlerShareable {
    progresses: ProgressMap<Progress>,
    progress_current_id: ProgressId,
    progresses_cycler: Cycler,

    pub(super) infomsg: String,

    clipboard: Clipboard,

    pub(super) keymapper: KeyMapper,

    pub(super) channels_map: ChannelMap<Arc<Channel>>,

    channel_draw: ChannelDraw,
    playlist_draw: PlaylistDraw,
    item_draw: ItemDraw,

    pub(super) config: Arc<Config>,
    tx_request: Sender<RequestMsg>,

    pub(super) ui_msg_queue: VecDeque<LocalUiMsg>,
}

impl HandlerShareable {
    pub fn config(&self) -> &Config {
        &self.config
    }
    pub fn send_req(&self, msg: RequestMsg) -> crate::Result<()> {
        Ok(self.tx_request.send(msg)?)
    }

    pub(in crate::ui) fn send_ui_msg(&mut self, msg: LocalUiMsg) {
        self.ui_msg_queue.push_back(msg);
    }

    pub fn clipboard(&self) -> &Clipboard {
        &self.clipboard
    }
    pub fn set_clipboard(&mut self, clipboard: Clipboard) {
        self.clipboard = clipboard;
    }

    pub fn keymapper(&self) -> &KeyMapper {
        &self.keymapper
    }

    pub fn channels_map(&self) -> &ChannelMap<Arc<Channel>> {
        &self.channels_map
    }

    pub fn channel_draw(&self) -> &ChannelDraw {
        &self.channel_draw
    }
    pub fn playlist_draw(&self) -> &PlaylistDraw {
        &self.playlist_draw
    }
    pub fn item_draw(&self) -> &ItemDraw {
        &self.item_draw
    }

    pub(super) fn new(config: Arc<Config>, tx_request: Sender<RequestMsg>) -> Self {
        Self {
            progresses: ProgressMap::new(),
            progress_current_id: ProgressId::first(),
            progresses_cycler: Cycler::from_millis(1000),

            infomsg: String::new(),

            clipboard: Clipboard::None,

            keymapper: KeyMapper::new(),

            channels_map: ChannelMap::new(),

            channel_draw: yaml_from_file(Path::new("drawers/channel/default.yaml"))
                .unwrap_or(ChannelDraw::default()),
            playlist_draw: yaml_from_file(Path::new("drawers/playlist/default.yaml"))
                .unwrap_or(PlaylistDraw::default()),
            item_draw: yaml_from_file(Path::new("drawers/item/default.yaml"))
                .unwrap_or(ItemDraw::default()),

            config,
            tx_request,

            ui_msg_queue: VecDeque::new(),
        }
    }

    pub fn add_new_progress(&mut self, title: String, auto_finish: bool) -> ProgressId {
        let id = self.progresses.first_free();
        self.progresses
            .insert(id, Progress::new(title, auto_finish));
        id
    }

    pub fn progress_msg(&mut self, progress: ProgressId, msg: ProgressMsg) -> crate::Result<()> {
        self.progresses
            .get_mut(progress)
            .ok_or(crate::Error::InvalidId)?
            .msg(msg);
        Ok(())
    }

    pub fn current_progress(&self) -> Option<&Progress> {
        self.progresses.get(self.progress_current_id)
    }

    pub fn maybe_cycle_progress(&mut self) -> bool {
        if self.progresses_cycler.time_for_next() {
            let was_some = if let Some(progress_bar) = self.progresses.get(self.progress_current_id)
            {
                if progress_bar.is_finished() {
                    self.progresses.remove(self.progress_current_id);
                }
                true
            } else {
                false
            };
            self.progress_current_id = self
                .progresses
                .next_cycle_filter(self.progress_current_id, |progress| !progress.is_zero());
            was_some || self.progresses.get(self.progress_current_id).is_some()
        } else {
            false
        }
    }

    pub fn progresses(&self) -> &ProgressMap<Progress> {
        &self.progresses
    }

    pub fn set_infomsg(&mut self, msg: String) {
        self.infomsg = msg
    }
    pub fn report_error(&mut self, e: crate::Error) {
        self.infomsg = format!("Err: {}", e)
    }

    pub fn set_drawer(&mut self, drawer: Drawer) {
        match drawer {
            Drawer::Item(d) => self.item_draw = d,
            Drawer::Playlist(d) => self.playlist_draw = d,
            Drawer::Channel(d) => self.channel_draw = d,
        }
    }
}
