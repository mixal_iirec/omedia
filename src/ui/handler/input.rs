use pancurses::Input;

use crate::msg::{EditMsg, RequestMsg};

use crate::ui::key_mapper::{Action, Movement, RangeActionMod, SpecialAction, SpecialActionMod};
use crate::ui::variable_editor::edit_var;

use super::Handler;

impl Handler {
    fn special_action(&mut self, action: SpecialActionMod) -> crate::Result<()> {
        self.tabs
            .current_mut()
            .special_action(&mut self.shareable, action)
    }

    fn range_action(
        &mut self,
        movement: Movement,
        action: Option<RangeActionMod>,
    ) -> crate::Result<()> {
        self.tabs
            .current_mut()
            .range_action(&mut self.shareable, movement, action)
    }

    pub fn process_input(&mut self, input: Input) -> crate::Result<()> {
        match input {
            Input::KeyResize => self.resize(),
            input => {
                if let Some(action) = self.shareable.keymapper.get_action(input) {
                    match action {
                        Action::Special { action } => self.preprocess_special_action(action),
                        Action::Range { action, movement } => self.range_action(movement, action),
                    }?
                }
                self.to_redraw();
            }
        }
        Ok(())
    }

    pub fn preprocess_special_action(&mut self, special: SpecialActionMod) -> crate::Result<()> {
        match special.typ {
            SpecialAction::Quit => self.send_req(RequestMsg::End)?,
            SpecialAction::Clear => self.set_infomsg(String::new()),
            SpecialAction::Fetch { all } => {
                let progress = self.add_new_progress(String::from("Fetching items"), true);
                self.send_req(RequestMsg::FetchItems { all, progress })?;
            }
            SpecialAction::Help => self.set_help_tab(None),
            SpecialAction::NextList => self.tabs.select_next(),
            SpecialAction::TabClose => self.tab_close(self.tabs.id_current()),
            SpecialAction::TabUpdate => self.tabs.current_mut().request_update(&self.shareable)?,
            SpecialAction::TabNewChannels => self.set_channel_tab(None),
            SpecialAction::TabNewPlaylists => {
                let list = self.tabs.new_id();
                let channel = None;
                self.send_req(RequestMsg::RequestPlaylists { list, channel })?;
            }
            SpecialAction::TabNewDownloads => {
                let list = self.tabs.new_id();
                let str_needed = self.item_draw().str_needed();
                self.send_req(RequestMsg::RequestDownloadList { list, str_needed })?;
            }
            SpecialAction::TabNewProgressBars => self.set_progress_tab(None),
            SpecialAction::TabEntries => self.set_entry_tab(None),
            SpecialAction::Enum => self.tabs.current_mut().toggle_enumerate(),
            SpecialAction::EditEntries => {
                let new_entries = edit_var(self.config.editor(), &self.config.bool_entries)?;
                self.send_req(RequestMsg::Edit(EditMsg::BoolEntries(new_entries)))?;
            }
            _ => self.special_action(special)?,
        }
        Ok(())
    }
}
