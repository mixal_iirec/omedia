use pancurses::Input;
use std::sync::mpsc::Sender;
use std::sync::Arc;
use std::time::Instant;

use crate::channel::{Channel, ChannelMap};
use crate::config::Config;
use crate::msg::{RequestMsg, UiMsg};
use crate::player::Player;

use super::view::View;

use tab::HandlerTabs;

mod draw;
mod input;
mod set_tab;
pub mod shareable;
mod tab;

pub use shareable::HandlerShareable;

pub struct Handler {
    shareable: HandlerShareable,

    view: View,

    player: Player,

    tabs: HandlerTabs,

    redraw_timing: (bool, Instant),
}

impl std::ops::Deref for Handler {
    type Target = HandlerShareable;
    fn deref(&self) -> &Self::Target {
        &self.shareable
    }
}

impl std::ops::DerefMut for Handler {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.shareable
    }
}

impl Handler {
    pub fn input(&self) -> Option<Input> {
        self.view.window.getch()
    }
    pub fn player(&self) -> &Player {
        &self.player
    }

    pub fn set_channels(&mut self, channels: ChannelMap<Arc<Channel>>) {
        self.channels_map = channels;
        for tab in self.tabs.iter_mut() {
            tab.update_channels(&self.shareable.channels_map);
        }
    }

    pub(in crate::ui) fn take_ui_msg(&mut self) -> Option<crate::ui::LocalUiMsg> {
        self.shareable.ui_msg_queue.pop_front()
    }

    pub fn new(config: Arc<Config>, tx_request: Sender<RequestMsg>, tx_ui: Sender<UiMsg>) -> Self {
        let mut handler = Self {
            view: View::new(Self::setup_term()),

            player: Player::new(Arc::clone(&config), tx_ui.clone(), tx_request.clone()),

            tabs: HandlerTabs::new(),

            redraw_timing: (true, Instant::now()),

            shareable: HandlerShareable::new(config, tx_request),
        };
        handler.check_no_tab();
        let msg = RequestMsg::RequestPlaylists {
            list: handler.tabs.new_id(),
            channel: None,
        };
        let _ = handler.send_req(msg);
        handler
    }
}
