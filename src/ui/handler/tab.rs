use crate::ui::list::{List, ListId, ListMap};
use crate::ui::UpdateMsg;

use super::Handler;

pub(super) struct HandlerTabs {
    inner: ListMap<Box<dyn List>>,
    ids: Vec<ListId>,
    current: usize,
}

impl HandlerTabs {
    pub fn new() -> Self {
        Self {
            inner: ListMap::new(),
            ids: Vec::new(),
            current: 0,
        }
    }

    pub fn count(&self) -> usize {
        self.inner.iter().count()
    }

    // todo: using lending iterator
    pub fn iter_mut<'a>(&'a mut self) -> impl Iterator<Item = &'a mut Box<dyn List + 'static>> {
        let mut values: ListMap<_> = self.inner.iter_mut().map(|(k, v)| (k, (v, 0))).collect();
        for (i, id) in self.ids.iter().enumerate() {
            values.get_mut(*id).unwrap().1 = i;
        }
        let mut values: Vec<_> = values.into_values().collect();
        values.sort_by_key(|(_, a)| *a);

        values.into_iter().map(|(f, _)| f)
    }

    pub fn id_current(&self) -> ListId {
        self.ids[self.current]
    }
    pub fn current(&self) -> &Box<dyn List> {
        self.inner
            .get(self.id_current())
            .expect("tab is current is guaranteed to be valid")
    }
    pub fn current_mut(&mut self) -> &mut Box<dyn List> {
        self.inner
            .get_mut(self.id_current())
            .expect("tab is current is guaranteed to be valid")
    }

    pub fn new_id(&mut self) -> ListId {
        self.inner.first_free()
    }

    pub fn select_next(&mut self) {
        self.current += 1;
        if self.current >= self.ids.len() {
            self.current = 0;
        }
    }
}

impl Handler {
    pub(super) fn check_no_tab(&mut self) {
        if self.tabs.ids.len() == 0 {
            self.set_channel_tab(None);
            self.tabs.current = 0;
        }
    }

    pub(super) fn set_tab(&mut self, id: ListId, tab: Box<dyn List>) {
        if self.tabs.inner.insert(id, tab).is_none() {
            let new_pos = self.tabs.current + if self.tabs.ids.is_empty() { 0 } else { 1 };
            self.tabs.ids.insert(new_pos, id);
            self.tabs.current = new_pos;
        }
        self.restructure();
    }

    pub fn tab_close(&mut self, tab: ListId) {
        let to_remove: Vec<_> = self
            .tabs
            .inner
            .iter()
            .filter(|(id, t)| *id == tab || t.dependent_on(tab))
            .map(|(id, _)| id)
            .collect();
        to_remove.iter().for_each(|id| {
            self.tabs.inner.remove(*id);
        });
        self.tabs.ids.retain(|v| !to_remove.contains(v));
        self.tabs.current = self.tabs.current.saturating_sub(1);
        self.check_no_tab();
        self.restructure();
    }

    pub(in crate::ui) fn tab_update(
        &mut self,
        tab: ListId,
        update: UpdateMsg,
    ) -> crate::Result<()> {
        self.tabs
            .inner
            .get_mut(tab)
            .ok_or(crate::Error::InvalidId)?
            .update(&self.shareable, update)
    }
}
