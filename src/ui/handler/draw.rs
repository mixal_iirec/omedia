use pancurses::*;
use std::time::{Duration, Instant};

use super::Handler;

impl Drop for Handler {
    fn drop(&mut self) {
        endwin();
    }
}

impl Handler {
    pub(super) fn setup_term() -> Window {
        let window = initscr();
        window.keypad(true);
        window.timeout(1);
        curs_set(0);
        noecho();
        start_color();
        use_default_colors();
        init_pair(1, COLOR_YELLOW, -1); //marked
        init_pair(2, COLOR_BLACK, COLOR_GREEN); //progress_bar
        init_pair(3, COLOR_GREEN, COLOR_BLACK); //progress_bar
        window
    }

    pub fn resize(&mut self) {
        resize_term(0, 0);
        self.restructure();
    }

    pub(super) fn restructure(&mut self) {
        self.view.split_vertical_by(vec![0, self.view.y() - 2]);
        let tab_view = self.view.get_mut(0).unwrap();

        if self.shareable.config.multiple_visible_tabs() {
            tab_view.split_horizontal_with_bars(self.tabs.count());
            for (tab, view) in self.tabs.iter_mut().zip(tab_view.iter_mut().step_by(2)) {
                tab.restructure(view);
            }
        } else {
            self.tabs.current_mut().restructure(tab_view);
        }
        self.to_redraw();
    }

    pub fn to_redraw(&mut self) {
        self.redraw_timing.0 = true;
    }

    pub fn draw(&mut self) {
        self.view.window.erase();

        let tab_view = self.view.get(0).unwrap();
        if self.shareable.config.multiple_visible_tabs() {
            let id_current = self.tabs.id_current();
            for (tab, view) in self.tabs.iter_mut().zip(tab_view.iter().step_by(2)) {
                tab.draw(&self.shareable, view, tab.id() == id_current);
            }
            for view in tab_view.iter().skip(1).step_by(2) {
                view.draw_column(0, "│");
            }
        } else {
            self.tabs.current().draw(&self.shareable, tab_view, true);
        }

        let view = self.view.get(1).unwrap();
        match self.shareable.current_progress() {
            Some(progress) => view.draw_progress(0, progress),
            None => view.draw_line(0, "═"),
        }
        let input = self.keymapper.input_buffer_str();
        let info = if input.is_empty() {
            &self.infomsg
        } else {
            &input
        };
        view.mvprint(1, 0, info);

        self.view.window.refresh();
    }

    pub fn redraw_if_needed(&mut self) {
        if (self.shareable.maybe_cycle_progress() || self.redraw_timing.0)
            && self.redraw_timing.1.elapsed() > Duration::from_millis(5)
        {
            self.draw();
            self.redraw_timing = (false, Instant::now());
        }
    }
}
