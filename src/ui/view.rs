use itertools::Itertools;
use pancurses::{chtype, Window, COLOR_PAIR};
use std::iter::once;
use unicode_segmentation::UnicodeSegmentation;

use crate::general::progress::Progress;

pub struct View {
    pub window: Window,
    subviews: Vec<View>,
}

impl View {
    pub fn x(&self) -> usize {
        self.window.get_max_x() as usize
    }
    pub fn y(&self) -> usize {
        self.window.get_max_y() as usize
    }
    pub fn get(&self, idx: usize) -> Option<&View> {
        self.subviews.get(idx)
    }
    pub fn get_mut(&mut self, idx: usize) -> Option<&mut View> {
        self.subviews.get_mut(idx)
    }

    pub fn subviews(&self) -> &Vec<View> {
        &self.subviews
    }
    pub fn iter(&self) -> impl DoubleEndedIterator<Item = &View> {
        self.subviews.iter()
    }
    pub fn iter_mut(&mut self) -> impl DoubleEndedIterator<Item = &mut View> {
        self.subviews.iter_mut()
    }

    pub fn new(window: Window) -> Self {
        View {
            window,
            subviews: Vec::new(),
        }
    }

    pub fn mv(&self, y: usize, x: usize) {
        self.window.mv(y as i32, x as i32);
    }

    pub fn print(&self, s: &str) {
        self.window.addstr(&s);
    }
    pub fn mvprint(&self, y: usize, x: usize, s: &str) {
        self.mv(y, x);
        self.print(&s);
    }

    pub fn split_horizontal_by<I: IntoIterator<Item = usize>>(&mut self, x: I) {
        self.subviews = x
            .into_iter()
            .chain(once(self.x()))
            .tuple_windows()
            .map(|(left, right)| {
                View::new(
                    self.window
                        .derwin(self.y() as i32, (right - left) as i32, 0, left as i32)
                        .unwrap(),
                )
            })
            .collect();
    }
    pub fn split_vertical_by<I: IntoIterator<Item = usize>>(&mut self, y: I) {
        self.subviews = y
            .into_iter()
            .chain(once(self.y()))
            .tuple_windows()
            .map(|(up, down)| {
                View::new(
                    self.window
                        .derwin((down - up) as i32, self.x() as i32, up as i32, 0)
                        .unwrap(),
                )
            })
            .collect();
    }

    pub fn split_horizontal(&mut self, num: usize) {
        let x = self.x();
        self.split_horizontal_by((0..num).map(|n| n * x / num))
    }
    pub fn split_horizontal_with_size(&mut self, size: usize) {
        self.split_horizontal_by((0..self.x()).step_by(size))
    }

    pub fn split_horizontal_with_bars(&mut self, num: usize) {
        let x = self.x();
        let mut vec = Vec::new();
        for n in 0..num {
            vec.push(n * x / num);
            if n != 0 {
                vec.push(n * x / num + 1);
            }
        }
        self.split_horizontal_by(vec)
    }

    pub fn split_vertical(&mut self, num: usize) {
        let y = self.y();
        self.split_vertical_by((0..num).map(|n| n * y / num))
    }

    pub fn split_vertical_with_size(&mut self, size: usize) {
        self.split_vertical_by((0..self.y()).step_by(size))
    }

    pub fn split_vertical_with_bars(&mut self, num: usize) {
        let y = self.y();
        let mut vec = Vec::new();
        for n in 0..num {
            vec.push(n * y / num);
            if n != 0 {
                vec.push(n * y / num + 1);
            }
        }
        self.split_vertical_by(vec)
    }

    pub fn draw_line(&self, line: usize, c: &str) {
        self.mv(line, 0);
        (0..self.x()).for_each(|_| {
            self.print(c);
        });
    }
    pub fn draw_column(&self, column: usize, c: &str) {
        (0..self.y()).for_each(|y| {
            self.mvprint(y, column, c);
        });
    }

    pub fn with_attrs<T: Into<chtype> + Copy, F: FnOnce(&View)>(&self, attr: T, f: F) {
        self.window.attron(attr);
        f(self);
        self.window.attroff(attr);
    }

    pub fn draw_progress(&self, line: usize, progress: &Progress) {
        self.draw_progress_with_offset(line, progress, 0);
    }

    pub fn draw_progress_with_offset(&self, line: usize, progress: &Progress, offset_x: usize) {
        let from = if progress.from() == 0 {
            1
        } else {
            progress.from()
        };
        let x = self.x() - offset_x;
        let idx = x * progress.done() / from;
        let ch = |i| if i < idx { "═" } else { "─" };

        let text = format!(
            "{}({}/{})",
            progress.title(),
            progress.done(),
            progress.from()
        );
        let text_len = text.graphemes(true).count();

        let mut buffer = String::new();
        if text_len >= x {
            buffer = text.graphemes(true).take(x).collect();
        } else {
            let mut buff_len = 0;

            let pre = (x - text_len) / 2;
            for i in 0..pre {
                buffer.push_str(ch(i));
            }
            buff_len += pre;

            buffer.push_str(&text);
            buff_len += text_len;

            for i in buff_len..x {
                buffer.push_str(ch(i));
            }
        }
        let split_idx = buffer
            .grapheme_indices(true)
            .nth(idx)
            .map(|ind| ind.0)
            .unwrap_or(buffer.len());
        let (left, right) = buffer.split_at(split_idx);

        self.mv(line, offset_x);
        self.with_attrs(COLOR_PAIR(2), |view| {
            view.print(left);
        });
        self.with_attrs(COLOR_PAIR(3), |view| {
            view.print(right);
        });
    }
}
