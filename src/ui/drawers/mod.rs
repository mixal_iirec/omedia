mod alignment;
mod channel_draw;
mod item_draw;
mod playlist_draw;
mod size;

pub mod into_str;

pub use channel_draw::ChannelDraw;
pub use item_draw::ItemDraw;
pub use playlist_draw::PlaylistDraw;

use regex::Regex;

use serde::{Deserialize, Serialize};

use std::borrow::Cow;

use crate::general::unicode::split_at_grapheme_max;
use crate::ui::view::View;

use alignment::Alignment;
use size::{resolved_sizes, Size};

pub trait Name {
    fn name() -> &'static str;
}

static STR_PIPE: &str = "│";

pub trait CowStr<I> {
    fn cow_str<'a>(&'a self, item: &'a I) -> Cow<'a, str>;
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct PropertyItem<P> {
    property: P,
    size: Size,
    align: Alignment,
    repeat: bool,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct PropertyLine<P> {
    height: usize,
    items: Vec<PropertyItem<P>>,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct PropertyDrawer<P> {
    name: String,
    lines: Vec<PropertyLine<P>>,
}

impl<P: 'static> PropertyDrawer<P> {
    pub fn new(lines: Vec<PropertyLine<P>>, name: String) -> Self {
        Self { lines, name }
    }

    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn height(&self) -> usize {
        self.lines.iter().map(|l| l.height).sum()
    }

    pub fn draw_with_offset<I>(&self, view: &View, item: &I, offset_x: usize)
    where
        P: CowStr<I>,
    {
        let mut cur_line = 0;
        for PropertyLine {
            height: lines,
            items,
        } in self.lines.iter()
        {
            let mut cur_width = offset_x;
            for (width, property, align, repeat) in resolved_sizes(
                items
                    .iter()
                    .map(|it| (it.size, &it.property, &it.align, &it.repeat)),
                view.x(),
            ) {
                let mut rest: &str = &property.cow_str(item);
                for l in 0..*lines {
                    if rest.len() == 0 {
                        break;
                    }
                    let (act, _rest, idx) = split_at_grapheme_max(rest, width as usize);
                    if !repeat {
                        rest = _rest;
                    }
                    let x = match align {
                        Alignment::Left => 0,
                        Alignment::Right => width - idx,
                        Alignment::Center => (width - idx) / 2,
                    };
                    view.mvprint(cur_line + l, cur_width + x, &act);
                }
                cur_width += width;
            }
            cur_line += lines;
        }
    }

    pub fn visibly_matches<I>(&self, item: &I, regex: &Regex) -> bool
    where
        P: CowStr<I>,
    {
        for PropertyLine { items, .. } in self.lines.iter() {
            for PropertyItem { property, .. } in items.iter() {
                if regex.is_match(&property.cow_str(item)) {
                    return true;
                }
            }
        }
        false
    }
}

#[derive(Debug)]
pub enum Drawer {
    Item(ItemDraw),
    Playlist(PlaylistDraw),
    Channel(ChannelDraw),
}
impl From<ItemDraw> for Drawer {
    fn from(d: ItemDraw) -> Self {
        Self::Item(d)
    }
}
impl From<PlaylistDraw> for Drawer {
    fn from(d: PlaylistDraw) -> Self {
        Self::Playlist(d)
    }
}
impl From<ChannelDraw> for Drawer {
    fn from(d: ChannelDraw) -> Self {
        Self::Channel(d)
    }
}

#[derive(Debug)]
pub enum DrawerType {
    Item,
    Playlist,
    Channel,
}
