use std::str::FromStr;

use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum Alignment {
    Left,
    Right,
    Center,
}

pub struct InvalidAlignment;

impl FromStr for Alignment {
    type Err = InvalidAlignment;
    fn from_str(s: &str) -> Result<Self, InvalidAlignment> {
        match s {
            "left" => Ok(Alignment::Left),
            "right" => Ok(Alignment::Right),
            "center" => Ok(Alignment::Center),
            _ => Err(InvalidAlignment {}),
        }
    }
}
