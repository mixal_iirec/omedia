use chrono::NaiveDate;
use serde::Deserialize;
use std::borrow::Cow;

pub fn i64_as_reduced_hms(v: i64) -> String {
    let s = v % 60;
    let m = v / 60 % 60;
    let h = v / 60 / 60;
    match (h, m, s) {
        (0, 0, s) => format!("{s}s"),
        (0, m, s) => format!("{m}:{s:02}"),
        (h, m, s) => format!("{h}:{m:02}:{s:02}"),
    }
}

#[derive(Deserialize)]
#[serde(
    try_from = "String",
    bound = "Visualizer<T>: TryFrom<String>, <Visualizer<T> as TryFrom<String>>::Error: std::fmt::Display"
)]
pub struct Visualizer<T: ?Sized> {
    format: String,
    fun: Box<dyn for<'a> Fn(&'a T) -> Cow<'a, str> + Send>,
}

impl<T: ?Sized> Visualizer<T> {
    pub fn fun<'a>(&self, v: &'a T) -> Cow<'a, str> {
        (*self.fun)(v)
    }
}

impl<T: ?Sized> Clone for Visualizer<T>
where
    Self: TryFrom<String>,
    <Self as TryFrom<String>>::Error: std::fmt::Debug,
{
    fn clone(&self) -> Self {
        Self::try_from(self.format.clone()).expect("Missing default implementation for Visualizer")
    }
}

impl<T: ?Sized> std::fmt::Debug for Visualizer<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.format)
    }
}

fn bool_to_str<'a>(b: &'a bool) -> Cow<'a, str> {
    Cow::from(if *b { "T" } else { "F" })
}

impl TryFrom<String> for Visualizer<bool> {
    type Error = &'static str;

    fn try_from(format: String) -> Result<Self, Self::Error> {
        let fun = match format.as_str() {
            "default" => Box::new(bool_to_str),
            _ => return Err("invalid bool visualizer format"),
        };

        Ok(Self { format, fun })
    }
}

impl TryFrom<String> for Visualizer<i32> {
    type Error = &'static str;

    fn try_from(format: String) -> Result<Self, Self::Error> {
        let fun: Box<dyn for<'a> Fn(&'a i32) -> Cow<'a, str> + Send> = match format.as_str() {
            "default" => Box::new(|i: &i32| Cow::from(i.to_string())),
            "fn:hms" => Box::new(|i: &i32| Cow::from(i64_as_reduced_hms(*i as i64))),
            _ => return Err("invalid i32 visualizer format"),
        };

        Ok(Self { format, fun })
    }
}

fn str_into_cow<'a>(s: &'a str) -> Cow<'a, str> {
    if s.contains("\t\t") {
        //used to replace newline
        Cow::from(s.replace("\t\t", "  "))
    } else {
        Cow::from(s)
    }
}

impl TryFrom<String> for Visualizer<str> {
    type Error = &'static str;

    fn try_from(format: String) -> Result<Self, Self::Error> {
        let fun: Box<dyn for<'a> Fn(&'a str) -> Cow<'a, str> + Send> = match format.as_str() {
            "default" => Box::new(str_into_cow),
            _ => return Err("invalid string visualizer format"),
        };

        Ok(Self { format, fun })
    }
}

impl TryFrom<String> for Visualizer<NaiveDate> {
    type Error = &'static str;

    fn try_from(format: String) -> Result<Self, Self::Error> {
        let fun: Box<dyn for<'a> Fn(&'a NaiveDate) -> Cow<'a, str> + Send> = match format.as_str() {
            "default" => Box::new(|d: &NaiveDate| Cow::from(d.format("%Y-%m-%d").to_string())),
            f if f.starts_with("format:") => {
                let format = f.strip_prefix("format:").unwrap().to_owned();
                Box::new(move |d: &NaiveDate| Cow::from(d.format(&format).to_string()))
            }
            _ => return Err("invalid date visualizer format"),
        };

        Ok(Self { format, fun })
    }
}

impl<T: ?Sized> Default for Visualizer<T>
where
    Self: TryFrom<String>,
    <Self as TryFrom<String>>::Error: std::fmt::Debug,
{
    fn default() -> Self {
        Self::try_from(String::from("default"))
            .expect("Missing default implementation for Visualizer")
    }
}

impl<T: ?Sized> serde::ser::Serialize for Visualizer<T> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.format)
    }
}
