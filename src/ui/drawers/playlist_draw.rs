use std::borrow::Cow;

use serde::{Deserialize, Serialize};

use crate::playlist::Playlist;

use super::alignment::Alignment;
use super::size::Size;
use super::CowStr;
use super::{PropertyDrawer, PropertyItem, PropertyLine, STR_PIPE};

impl super::Name for PlaylistProperty {
    fn name() -> &'static str {
        "playlist"
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum PlaylistProperty {
    Name,
    Pipe,
    Text(String),
    Blank,
}

use PlaylistProperty::*;

impl CowStr<Playlist> for PlaylistProperty {
    fn cow_str<'a>(&'a self, playlist: &'a Playlist) -> Cow<'a, str> {
        match self {
            Name => Cow::from(playlist.name()),
            Pipe => Cow::from(STR_PIPE),
            Text(t) => Cow::from(t.as_str()),
            Blank => Cow::from(""),
        }
    }
}

pub type PlaylistDraw = PropertyDrawer<PlaylistProperty>;

impl PlaylistDraw {
    pub fn default() -> Self {
        Self::new(
            vec![PropertyLine {
                height: 1,
                items: vec![PropertyItem {
                    size: Size::Proportional(42),
                    property: Name,
                    align: Alignment::Left,
                    repeat: false,
                }],
            }],
            String::from("Default"),
        )
    }

    pub fn simple_custom(s: &str) -> Self {
        let mut items = Vec::new();

        for split in s.split("|") {
            let (prop, size) = match split {
                "name" => (Name, Size::Proportional(100)),
                s => (Text(String::from(s)), Size::Proportional(100)),
            };
            items.push(PropertyItem {
                size,
                property: prop,
                align: Alignment::Left,
                repeat: false,
            });

            items.push(PropertyItem {
                size: Size::Absolute(1),
                property: Pipe,
                align: Alignment::Left,
                repeat: true,
            });
        }
        items.pop();

        Self::new(
            vec![PropertyLine { height: 1, items }],
            String::from("Custom"),
        )
    }
}
