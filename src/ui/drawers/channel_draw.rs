use std::borrow::Cow;

use serde::{Deserialize, Serialize};

use crate::channel::Channel;

use super::alignment::Alignment;
use super::size::Size;
use super::CowStr;
use super::{PropertyDrawer, PropertyItem, PropertyLine, STR_PIPE};

impl super::Name for ChannelProperty {
    fn name() -> &'static str {
        "channel"
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum ChannelProperty {
    Name,
    Url,
    Scraper,
    Pipe,
    Text(String),
    Blank,
}

use ChannelProperty::*;

impl CowStr<Channel> for ChannelProperty {
    fn cow_str<'a>(&'a self, channel: &'a Channel) -> Cow<'a, str> {
        match self {
            Name => Cow::from(channel.name()),
            Url => Cow::from(channel.url()),
            Scraper => Cow::from(
                channel
                    .scraper_dir()
                    .map(|scr| scr.to_str().unwrap())
                    .unwrap_or(""),
            ),
            Pipe => Cow::from(STR_PIPE),
            Text(t) => Cow::from(t.as_str()),
            Blank => Cow::from(""),
        }
    }
}

pub type ChannelDraw = PropertyDrawer<ChannelProperty>;

impl ChannelDraw {
    pub fn default() -> Self {
        Self::new(
            vec![PropertyLine {
                height: 1,
                items: vec![PropertyItem {
                    size: Size::Proportional(42),
                    property: Name,
                    align: Alignment::Left,
                    repeat: false,
                }],
            }],
            String::from("Default"),
        )
    }

    pub fn simple_custom(s: &str) -> Self {
        let mut items = Vec::new();

        for split in s.split("|") {
            let (prop, size) = match split {
                "name" => (Name, Size::Proportional(100)),
                "url" => (Url, Size::Proportional(100)),
                "scraper" => (Scraper, Size::Proportional(100)),
                s => (Text(String::from(s)), Size::Proportional(100)),
            };
            items.push(PropertyItem {
                size,
                property: prop,
                align: Alignment::Left,
                repeat: false,
            });

            items.push(PropertyItem {
                size: Size::Absolute(1),
                property: Pipe,
                align: Alignment::Left,
                repeat: true,
            });
        }
        items.pop();

        Self::new(
            vec![PropertyLine { height: 1, items }],
            String::from("Custom"),
        )
    }
}
