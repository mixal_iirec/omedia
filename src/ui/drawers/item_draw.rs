use std::borrow::Cow;
use std::sync::Arc;

use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use serde_with::{serde_as, FromInto, PickFirst};

use chrono::NaiveDate;

use crate::channel::Channel;
use crate::entries::{EntryId, EntrySet, ENTRIES};
use crate::item::Item;
use crate::ui::general::{smart_name_to_entryid, ItemEntryKind};

use super::alignment::Alignment;
use super::into_str::Visualizer;
use super::size::Size;
use super::CowStr;
use super::{PropertyDrawer, PropertyItem, PropertyLine, STR_PIPE};

impl super::Name for ItemProperty {
    fn name() -> &'static str {
        "item"
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(bound = "")]
pub struct Property<T: ?Sized>
where
    EntryId<T>: Serialize + DeserializeOwned,
    Visualizer<T>: std::fmt::Debug + Serialize + DeserializeOwned,
{
    id: EntryId<T>,
    visualizer: Visualizer<T>,
}

impl<T: ?Sized> Clone for Property<T>
where
    EntryId<T>: Serialize + DeserializeOwned,
    Visualizer<T>: std::fmt::Debug + Serialize + DeserializeOwned + Clone,
{
    fn clone(&self) -> Self {
        Self {
            id: self.id.clone(),
            visualizer: self.visualizer.clone(),
        }
    }
}

impl<T: ?Sized> From<EntryId<T>> for Property<T>
where
    EntryId<T>: Serialize + DeserializeOwned,
    Visualizer<T>: std::fmt::Debug + Serialize + DeserializeOwned + Default,
{
    fn from(id: EntryId<T>) -> Self {
        Self {
            id,
            visualizer: Default::default(),
        }
    }
}

#[serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "snake_case")]
#[serde(remote = "ItemProperty")]
pub enum ItemProperty {
    Date(Visualizer<NaiveDate>),
    Channel,
    Pipe,
    Text(String),
    Bool(#[serde_as(as = "PickFirst<(_, FromInto<EntryId<bool>>)>")] Property<bool>),
    I32(#[serde_as(as = "PickFirst<(_, FromInto<EntryId<i32>>)>")] Property<i32>),
    Str(#[serde_as(as = "PickFirst<(_, FromInto<EntryId<str>>)>")] Property<str>),
    Blank,
}

// allow for empty Date enum varint
impl<'de> Deserialize<'de> for ItemProperty {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::de::Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(rename_all = "snake_case")]
        enum HelperInner {
            Date,
        }

        #[derive(Deserialize)]
        #[serde(untagged)]
        enum Helper {
            Date(HelperInner),
            #[serde(with = "ItemProperty")]
            P(ItemProperty),
        }
        let h = Helper::deserialize(deserializer)?;

        Ok(match h {
            Helper::Date(_) => ItemProperty::Date(Default::default()),
            Helper::P(p) => p,
        })
    }
}

use ItemProperty::*;

impl CowStr<(Item, Arc<Channel>)> for ItemProperty {
    fn cow_str<'a>(&'a self, (item, channel): &'a (Item, Arc<Channel>)) -> Cow<'a, str> {
        match self {
            Date(v) => v.fun(item.date()),
            Channel => Cow::from(channel.name()),
            Pipe => Cow::from(STR_PIPE),
            Text(v) => Cow::from(v.as_str()),
            Bool(p) => p.visualizer.fun(
                // required because we don't have reference to the bool, so static references are needed
                if item.var_bool(p.id) { &true } else { &false },
            ),
            I32(p) => p.visualizer.fun(item.var_i32(p.id).unwrap_or(&0)),
            Str(p) => p.visualizer.fun(item.var_string(p.id).unwrap_or("")),
            Blank => Cow::from(""),
        }
    }
}

pub type ItemDraw = PropertyDrawer<ItemProperty>;

impl ItemDraw {
    pub fn default() -> Self {
        Self::new(
            vec![PropertyLine {
                height: 1,
                items: vec![
                    PropertyItem {
                        size: Size::Absolute(10),
                        property: Date(Default::default()),
                        align: Alignment::Left,
                        repeat: false,
                    },
                    PropertyItem {
                        size: Size::Absolute(1),
                        property: Pipe,
                        align: Alignment::Left,
                        repeat: false,
                    },
                    PropertyItem {
                        size: Size::Proportional(70),
                        property: Str(ENTRIES.string_name_id("title").into()),
                        align: Alignment::Left,
                        repeat: false,
                    },
                    PropertyItem {
                        size: Size::Absolute(1),
                        property: Pipe,
                        align: Alignment::Left,
                        repeat: false,
                    },
                    PropertyItem {
                        size: Size::Proportional(30),
                        property: Str(ENTRIES.string_name_id("url").into()),
                        align: Alignment::Left,
                        repeat: false,
                    },
                ],
            }],
            String::from("Default"),
        )
    }

    pub fn str_needed(&self) -> EntrySet<str> {
        self.lines
            .iter()
            .map(|l| {
                l.items.iter().filter_map(|i| match &i.property {
                    Str(p) => Some(p.id.clone()),
                    _ => None,
                })
            })
            .flatten()
            .collect()
    }
    pub fn simple_custom(s: &str) -> Self {
        let mut items = Vec::new();

        for split in s.split("|") {
            let (prop, size) = match smart_name_to_entryid(split) {
                Ok(ItemEntryKind::Date) => (Date(Default::default()), Size::Absolute(10)),
                Ok(ItemEntryKind::Channel) => (Channel, Size::Proportional(100)),
                Ok(ItemEntryKind::Bool(id)) => (Bool(id.into()), Size::Absolute(1)),
                Ok(ItemEntryKind::I32(id)) => (I32(id.into()), Size::Absolute(6)),
                Ok(ItemEntryKind::Str(id)) => (Str(id.into()), Size::Proportional(100)),
                Err(_) => (Text(String::from(split)), Size::Proportional(100)),
            };
            items.push(PropertyItem {
                size,
                property: prop,
                align: Alignment::Left,
                repeat: false,
            });

            items.push(PropertyItem {
                size: Size::Absolute(1),
                property: Pipe,
                align: Alignment::Left,
                repeat: true,
            });
        }
        items.pop();

        Self::new(
            vec![PropertyLine { height: 1, items }],
            String::from("Custom"),
        )
    }
}
