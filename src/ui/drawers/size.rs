use serde::{Deserialize, Serialize};

#[derive(Copy, Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum Size {
    Absolute(usize),
    Proportional(usize),
}

pub fn resolved_sizes<'a, T1: 'a, T2: 'a, T3: 'a>(
    ws: impl Iterator<Item = (Size, &'a T1, &'a T2, &'a T3)> + Clone,
    mut full_size: usize,
) -> impl Iterator<Item = (usize, &'a T1, &'a T2, &'a T3)> {
    let (absolute, mut proportional) = ws.clone().fold((0, 0), |(a, p), w| match w.0 {
        Size::Absolute(v) => (a + v, p),
        Size::Proportional(v) => (a, p + v),
    });
    full_size -= absolute;
    ws.map(move |w| {
        (
            match w.0 {
                Size::Absolute(v) => v,
                Size::Proportional(p) => {
                    let s = full_size * p / proportional;
                    full_size -= s;
                    proportional -= p;
                    s
                }
            },
            w.1,
            w.2,
            w.3,
        )
    })
}
