pub enum Move {
    Begin,
    End,
    Up(usize),
    Down(usize),
    PageUp(usize),
    PageDown(usize),
    To(usize),
}

pub struct Movelist {
    size: usize,
    offset: usize,
    visible: usize,
    selected: usize,
}

impl Movelist {
    pub fn size(&self) -> usize {
        self.size
    }
    pub fn offset(&self) -> usize {
        self.offset
    }
    pub fn visible(&self) -> usize {
        self.visible
    }
    pub fn selected(&self) -> usize {
        self.selected
    }

    pub fn new_up(size: usize, visible: usize) -> Self {
        Self::new(size, visible, 0)
    }

    pub fn new_down(size: usize, visible: usize) -> Self {
        Self::new(size, visible, size)
    }

    pub fn new(size: usize, visible: usize, selected: usize) -> Self {
        let mut list = Self {
            size,
            offset: 0,
            visible,
            selected,
        };
        list.check();
        list
    }

    pub fn set_visibility(&mut self, visible: usize) {
        self.visible = visible;
        self.check();
    }

    pub fn set_selected(&mut self, selected: usize) {
        self.selected = selected;
        self.check();
    }

    fn check(&mut self) {
        if self.selected >= self.size {
            self.selected = self.size.saturating_sub(1);
        }

        if self.offset + self.visible > self.size {
            self.offset = self.size.saturating_sub(self.visible);
        }
        if self.selected < self.offset {
            self.offset = self.selected;
        }
        if self.selected >= self.offset + self.visible {
            self.offset = (self.selected + 1).saturating_sub(self.visible);
        }
    }

    pub fn mv(&mut self, mv: Move) {
        self.selected = match mv {
            Move::Begin => 0,
            Move::End => self.size.saturating_sub(1),
            Move::Up(x) => self.selected.saturating_sub(x),
            Move::Down(x) => self.selected + x,
            Move::PageUp(x) => self.selected.saturating_sub(self.visible * x),
            Move::PageDown(x) => self.selected + self.visible * x,
            Move::To(pos) => pos,
        };

        self.check();
    }

    pub fn change_size(&mut self, size: usize) {
        self.size = size;
        self.check();
    }
}
