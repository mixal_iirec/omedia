mod handler;
use handler::Handler;

mod cycler;
mod drawers;
mod key_mapper;
mod view;

mod general;
mod list;
mod move_list;
mod variable_editor;

use std::sync::mpsc::{Receiver, Sender, TryRecvError};
use std::sync::Arc;

use crate::config::Config;
use crate::msg::{
    RequestMsg,
    UiMsg::{self, *},
};

use list::ListId;

#[derive(Debug)]
enum UpdateMsg {
    Drawer(drawers::Drawer),
    Backend(crate::msg::UpdateMsg),
}

enum LocalUiMsg {
    Msg(&'static str),
    SetChannels(ListId),
    OpenDrawerChoice(ListId, drawers::DrawerType),
    Update(ListId, UpdateMsg),
    CloseTab(ListId),
}

pub fn run_tui(
    config: Arc<Config>,
    tx_ui: Sender<UiMsg>,
    rx_ui: Receiver<UiMsg>,
    tx_requests: Sender<RequestMsg>,
) {
    tx_requests.send(RequestMsg::RequestChannels).unwrap();

    let mut handler = Handler::new(Arc::clone(&config), tx_requests, tx_ui);

    loop {
        match handle_tui(&mut handler, &rx_ui) {
            Ok(true) => break,
            Ok(false) => (),
            Err(crate::Error::SendError) => {
                eprintln!("Error sending message to backend!");
                break;
            }
            Err(e) => {
                handler.report_error(e);
                handler.to_redraw();
            }
        }
    }
}

fn handle_tui(handler: &mut Handler, rx_ui: &Receiver<UiMsg>) -> crate::Result<bool> {
    loop {
        while let Some(msg) = handler.take_ui_msg() {
            use LocalUiMsg::*;
            match msg {
                SetChannels(list) => handler.set_channel_tab(Some(list)),
                OpenDrawerChoice(list, typ) => handler.add_drawer_choice_tab(list, typ)?,
                CloseTab(list) => handler.tab_close(list),
                Update(list, update) => handler.tab_update(list, update)?,
                Msg(msg) => handler.set_infomsg(String::from(msg)),
            }
        }

        match rx_ui.try_recv() {
            Ok(msg) => {
                match msg {
                    ReceiveChannels(channels) => handler.set_channels(channels),
                    ReceivePlaylists(list, channel_info, playlists) => {
                        handler.set_playlist_tab(Some(list), channel_info, playlists)
                    }
                    ReceivePlaylist(list, info, vec) => handler.set_item_tab(Some(list), info, vec),
                    ReceivePlayData(list) => handler.player().play(list)?,
                    ReceiveDownloadList(list, vec, done) => {
                        handler.set_download_tab(Some(list), vec, done)
                    }
                    End => return Ok(true),
                    Progress(progress, msg) => handler.progress_msg(progress, msg)?,
                    Update(list, update) => handler.tab_update(list, UpdateMsg::Backend(update))?,
                    Msg(msg) => handler.set_infomsg(String::from(msg)),
                    Error(e) => handler.report_error(e),
                }
                handler.to_redraw();
            }
            Err(TryRecvError::Empty) => break,
            Err(TryRecvError::Disconnected) => return Ok(true),
        }
    }
    while let Some(input) = handler.input() {
        handler.process_input(input)?;
    }
    handler.redraw_if_needed();
    Ok(false)
}
