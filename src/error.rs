use std::path::{Path, PathBuf};
use std::sync::mpsc::SendError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("regex: `{0}`")]
    Regex(#[from] regex::Error),
    #[error("{0}")]
    ChronoError(#[from] chrono::format::ParseError),
    #[error("{0}")]
    SerdeYaml(#[from] serde_yaml::Error),
    #[error("{0}")]
    Io(std::io::Error), //dont auto convert, use IoPath if possible

    #[error("{0}")]
    Formatted(String),

    #[error("Unable to send data through channel!")]
    SendError,
    #[error("Tried to load invalid utf8!")]
    Utf8Error,
    #[error("Item with such file already exists!")]
    FileAlreadyExists,
    #[error("Path `{0}`: `{1}`!")]
    IoPath(PathBuf, std::io::Error),
    #[error("Different lenght of data arrays!")]
    DifArraysLen,
    #[error("File not downloaded!")]
    FileNotDownloaded,
    #[error("File download aborted!")]
    FileDownloadAborted,
    #[error("Invalid/outdated id!")]
    InvalidId,
    #[error("Invalid entry name!")]
    InvalidEntryName,
    #[error("Missing Url/Path!")]
    MissingUrlPath,
    #[error("Type guaranteed")]
    TypeGuaranteed,
    #[error("Missing name")]
    MissingName,
    #[error("Unable to scrape item: {0}")]
    UnableToScrapeItem(String),
    #[error("Action aborted by user")]
    ActionAbortedByUser,
    #[error("Unimplemented action!")]
    Unimplemented,
}
impl From<(&Path, std::io::Error)> for Error {
    fn from(f: (&Path, std::io::Error)) -> Self {
        Self::IoPath(PathBuf::from(f.0), f.1)
    }
}
impl From<(&PathBuf, std::io::Error)> for Error {
    fn from(f: (&PathBuf, std::io::Error)) -> Self {
        Self::IoPath(f.0.clone(), f.1)
    }
}

impl<T> From<SendError<T>> for Error {
    fn from(_: SendError<T>) -> Self {
        Self::SendError
    }
}

impl From<std::string::FromUtf8Error> for Error {
    fn from(_: std::string::FromUtf8Error) -> Self {
        Self::Utf8Error
    }
}

impl From<String> for Error {
    fn from(v: String) -> Self {
        Error::Formatted(v.into())
    }
}

pub type Result<T> = std::result::Result<T, Error>;
