use crate::config::BoolEntries;
use crate::item::ItemBools;

pub struct EditBoolEntries {
    default: ItemBools,
    from_to: Vec<(usize, usize)>,
}

impl EditBoolEntries {
    pub fn new(orig: &BoolEntries, new: &BoolEntries) -> Self {
        let default = new.bool_default();
        let mut from_to = Vec::new();

        for (from, orig_entry) in orig.entries.iter().enumerate() {
            for (to, new_entry) in new.entries.iter().enumerate() {
                if orig_entry.name == new_entry.name {
                    from_to.push((from, to));
                }
            }
        }

        Self { default, from_to }
    }

    pub fn transform(&self, orig: &ItemBools) -> ItemBools {
        let mut new = self.default.clone();

        for (from, to) in self.from_to.iter() {
            new.set(to, orig.get(from));
        }

        new
    }
}
