pub mod changer;
pub mod changer_conditional;
pub mod matcher;

use chrono::{Local, NaiveDate};
use serde::{Deserialize, Serialize};

use std::sync::Arc;

use crate::channel::ChannelId;
use crate::entries::{EntryId, EntryMap};
use crate::general::BitMask;

pub type ItemId = crate::general::Idi32<Item>;
pub type ItemMap<T> = crate::general::IdMap<Item, ItemId, T>;
pub type ItemBools = BitMask<u32>;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ItemIdLess {
    date: NaiveDate,

    pub strings: EntryMap<str, Arc<str>>,
    pub i32s: EntryMap<i32, i32>,
    pub bools: ItemBools,
}

impl Default for ItemIdLess {
    fn default() -> Self {
        Self {
            date: Local::today().naive_utc(),
            strings: EntryMap::new(),
            i32s: EntryMap::new(),
            bools: ItemBools::new(),
        }
    }
}

impl ItemIdLess {
    pub fn new(
        date: NaiveDate,
        strings: EntryMap<str, Arc<str>>,
        i32s: EntryMap<i32, i32>,
        bools: ItemBools,
    ) -> Self {
        Self {
            date,
            strings,
            i32s,
            bools,
        }
    }

    pub fn date(&self) -> &NaiveDate {
        &self.date
    }

    pub fn var_string(&self, id: EntryId<str>) -> Option<&str> {
        self.strings.get(id).map(|o| o.as_ref())
    }
    pub fn var_i32(&self, id: EntryId<i32>) -> Option<&i32> {
        self.i32s.get(id)
    }
    pub fn var_bool(&self, id: EntryId<bool>) -> bool {
        self.bools.get(Into::<usize>::into(id))
    }
}

pub type ChItemId = (ChannelId, ItemId);

#[derive(Clone, Debug)]
pub struct Item {
    id_less: ItemIdLess,
    id: ChItemId,
}

impl Item {
    pub fn id(&self) -> ChItemId {
        self.id
    }
    pub fn channel_id(&self) -> ChannelId {
        self.id.0
    }
    pub fn item_id(&self) -> ItemId {
        self.id.1
    }

    pub fn id_less(&self) -> &ItemIdLess {
        &self.id_less
    }

    pub fn from_id_less(id_less: ItemIdLess, id: ChItemId) -> Self {
        Self { id_less, id }
    }
    pub fn into_id_less(self) -> ItemIdLess {
        self.id_less
    }
}

impl std::ops::Deref for Item {
    type Target = ItemIdLess;
    fn deref(&self) -> &Self::Target {
        &self.id_less
    }
}

impl std::ops::DerefMut for Item {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.id_less
    }
}
