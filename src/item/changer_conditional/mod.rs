use serde::{Deserialize, Serialize};

use super::changer::ItemChanger;
use super::matcher::ItemMatcher;
use super::{Item, ItemIdLess};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct ItemChangerConditional {
    condition: ItemMatcher,
    change: ItemChanger,
}

impl ItemChangerConditional {
    pub fn default_bools() -> Self {
        Self {
            condition: Default::default(),
            change: ItemChanger::default_bools(),
        }
    }

    pub fn change<F, G>(&self, vec: Vec<&mut Item>, load_strings: F, into_file_readers: G)
    where
        F: Fn(&str, Vec<(&mut Option<String>, &Item)>),
        G: Fn(crate::channel::ChannelId, &str) -> Vec<crate::files::compact::LoadOnRead>,
    {
        for item in self.condition.filter(vec, load_strings, into_file_readers) {
            self.change.change(item);
        }
    }

    pub fn change_id_less(&self, vec: Vec<&mut ItemIdLess>) {
        for item in self.condition.filter_mut_id_less(vec) {
            self.change.change(item);
        }
    }
}
