use crate::item::{Item, ItemIdLess};

use serde::{Deserialize, Serialize};

mod match_bools;
mod match_date;
mod match_ints;
mod match_strings;

use match_bools::MatchBools;
use match_date::MatchDate;
use match_ints::MatchInts;
use match_strings::MatchStrings;

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct ItemMatcher {
    date: Option<MatchDate>,
    strings: MatchStrings,
    i32s: MatchInts,
    bools: MatchBools,
}

impl ItemMatcher {
    fn match_item_non_string(&self, i: &ItemIdLess) -> bool {
        let match_date = &self.date.as_ref();
        self.bools.matches(i.bools)
            && match_date.map(|m| m.matches(*i.date())).unwrap_or(true)
            && self.i32s.matches(&i.i32s)
    }

    pub fn filter<F, G, I>(&self, mut vec: Vec<I>, load_strings: F, into_file_readers: G) -> Vec<I>
    where
        I: std::borrow::Borrow<Item>,
        F: Fn(&str, Vec<(&mut Option<String>, &Item)>),
        G: Fn(crate::channel::ChannelId, &str) -> Vec<crate::files::compact::LoadOnRead>,
    {
        vec.retain(|i| self.match_item_non_string(i.borrow().id_less()));
        self.strings.filter(vec, load_strings, into_file_readers)
    }

    pub fn filter_mut_id_less<'a>(
        &self,
        mut vec: Vec<&'a mut ItemIdLess>,
    ) -> Vec<&'a mut ItemIdLess> {
        vec.retain(|i| self.match_item_non_string(i));
        self.strings.filter_mut_id_less(vec)
    }
}
