use regex::Regex;
use serde::{Deserialize, Serialize};

use std::borrow::Borrow;

use crate::entries::{EntryId, EntryMap, ENTRIES};
use crate::files::compact::LoadOnRead;

use crate::item::{Item, ItemIdLess};

#[derive(Debug, Clone, Default)]
pub struct MatchStrings {
    comp: EntryMap<str, Regex>,
}

impl Serialize for MatchStrings {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::ser::Serializer,
    {
        self.comp
            .iter()
            .map(|(id, reg)| (id, reg.as_str()))
            .collect::<EntryMap<_, _>>()
            .serialize(serializer)
    }
}
impl<'de> Deserialize<'de> for MatchStrings {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::de::Deserializer<'de>,
    {
        let de = EntryMap::<str, String>::deserialize(deserializer)?;
        Ok(Self {
            comp: de
                .iter()
                .filter_map(|(id, reg)| (reg != "").then(|| Ok((id, Regex::new(reg)?))))
                .collect::<Result<_, regex::Error>>()
                .map_err(serde::de::Error::custom)?,
        })
    }
}

fn match_item(id: EntryId<str>, regex: &Regex, item: &ItemIdLess) -> bool {
    item.borrow()
        .var_string(id)
        .map(|s| regex.is_match(s))
        .unwrap_or(false)
}

impl MatchStrings {
    pub fn filter<F, G, I: Borrow<Item>>(
        &self,
        mut list: Vec<I>,
        load_strings: F,
        into_file_readers: G,
    ) -> Vec<I>
    where
        F: Fn(&str, Vec<(&mut Option<String>, &Item)>),
        G: Fn(crate::channel::ChannelId, &str) -> Vec<LoadOnRead>,
    {
        let match_subtitles = |s: &str, regex: &Regex, it: &I| -> bool {
            for l in into_file_readers(it.borrow().channel_id(), s) {
                if l.load_owned().map(|s| regex.is_match(&s)).unwrap_or(false) {
                    return true;
                }
            }
            false
        };

        for (id, regex) in self.comp.iter() {
            let str_id = ENTRIES.string_name_from_id(id);
            list = if list.iter().any(|it| it.borrow().var_string(id).is_some()) {
                if &*str_id != "subtitles" {
                    //todo: general solution
                    list.retain(|it| match_item(id, regex, it.borrow().id_less()));
                } else {
                    list.retain(|it| {
                        if let Some(s) = it.borrow().var_string(id) {
                            match_subtitles(s, regex, it)
                        } else {
                            false
                        }
                    });
                }
                list
            } else {
                let mut strings: Vec<Option<String>> = vec![None; list.len()];
                load_strings(
                    &*str_id,
                    strings
                        .iter_mut()
                        .zip(list.iter().map(|i| i.borrow()))
                        .collect(),
                );
                let strings_iter = strings.into_iter().zip(list.into_iter());
                if &*str_id != "subtitles" {
                    //todo: general solution
                    strings_iter
                        .filter(|(s, _)| s.as_ref().map(|s| regex.is_match(s)).unwrap_or(false))
                        .map(|(_, i)| i)
                        .collect()
                } else {
                    strings_iter
                        .filter_map(|(s, it)| match_subtitles(&s?, regex, &it).then_some(it))
                        .collect()
                }
            };
        }
        list
    }

    pub fn filter_mut_id_less<'a>(
        &self,
        mut list: Vec<&'a mut ItemIdLess>,
    ) -> Vec<&'a mut ItemIdLess> {
        for (id, regex) in self.comp.iter() {
            list.retain(|it| match_item(id, regex, it));
        }
        list
    }
}
