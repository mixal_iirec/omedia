use crate::entries::EntryMap;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "snake_case")]
#[serde(untagged)]
enum MatchInt<T> {
    Value(T),
    Range { start: T, end: T },
}

impl<T: Ord> MatchInt<T> {
    fn matches(&self, cmp: T) -> bool {
        match self {
            Self::Value(date) => *date == cmp,
            Self::Range { start, end } => *start <= cmp && cmp <= *end,
        }
    }
}

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
#[serde(transparent)]
pub struct MatchInts {
    comp: EntryMap<i32, MatchInt<i32>>,
}

impl MatchInts {
    pub fn matches(&self, map: &EntryMap<i32, i32>) -> bool {
        for (id, m) in self.comp.iter() {
            if !m.matches(map.get(id).cloned().unwrap_or(0)) {
                return false;
            }
        }
        true
    }
}
