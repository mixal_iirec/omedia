use chrono::{Duration, Local, NaiveDate};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "snake_case")]
#[serde(untagged)]
pub enum MatchDate {
    Value(NaiveDate),
    Range { start: NaiveDate, end: NaiveDate },
    Relative { start: i32, end: i32 },
}

impl MatchDate {
    pub fn matches(&self, cmp: NaiveDate) -> bool {
        match self {
            Self::Value(date) => *date == cmp,
            Self::Range { start, end } => *start <= cmp && cmp <= *end,
            Self::Relative { start, end } => {
                Local::today().naive_utc() + Duration::days(*start as i64) <= cmp
                    && cmp <= Local::today().naive_utc() + Duration::days(*end as i64)
            }
        }
    }
}
