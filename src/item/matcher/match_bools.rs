use serde::{Deserialize, Serialize};

use crate::entries::EntryMap;

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
#[serde(transparent)]
pub struct MatchBools {
    comp: EntryMap<bool, bool>,
}

impl MatchBools {
    pub fn matches(&self, bools: crate::item::ItemBools) -> bool {
        self.comp
            .iter()
            .map(|(pos, v)| (pos.into(), *v))
            .map(|(pos, v): (usize, bool)| bools.get(pos) == v)
            .find(|b| !b)
            .is_none()
    }
}
