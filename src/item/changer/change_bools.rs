use serde::{Deserialize, Serialize};

use crate::item::ItemIdLess;

use crate::entries::{EntryMap, ENTRIES};

#[derive(Debug, Default, Serialize, Deserialize)]
#[serde(transparent)]
pub struct ChangeBools {
    change: EntryMap<bool, bool>,
}

impl ChangeBools {
    pub fn default_bools() -> Self {
        let default_bools = ENTRIES.bools_default();

        let mut change = EntryMap::new();
        for (idx, _) in ENTRIES.bools() {
            change.insert(idx, default_bools.get(Into::<usize>::into(idx)));
        }
        Self { change }
    }

    pub fn change(&self, item: &mut ItemIdLess) {
        for (pos, change) in self.change.iter() {
            item.bools.set(Into::<usize>::into(pos), *change);
        }
    }
}
