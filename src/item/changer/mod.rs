mod change_bools;

use serde::{Deserialize, Serialize};

use crate::item::ItemIdLess;

use change_bools::ChangeBools;

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct ItemChanger {
    bools: ChangeBools,
}

impl ItemChanger {
    pub fn default_bools() -> Self {
        Self {
            bools: ChangeBools::default_bools(),
        }
    }

    pub fn change(&self, item: &mut ItemIdLess) {
        self.bools.change(item);
    }
}
