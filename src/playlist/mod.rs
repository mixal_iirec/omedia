use std::path::PathBuf;
use std::sync::Arc;

use crate::files::{yaml_from_file, yaml_to_file};

mod playlist_info;

pub use playlist_info::{Playlist, PlaylistId, PlaylistIdLess, PlaylistLocalId, PlaylistMap};

#[derive(Debug)]
pub struct PlaylistDir {
    info: Arc<Playlist>,
    dir: PathBuf,
}

impl PlaylistDir {
    pub fn info(&self) -> &Arc<Playlist> {
        &self.info
    }

    pub fn create(dir: PathBuf, id_less: PlaylistIdLess, id: PlaylistId) -> crate::Result<Self> {
        let playlist = Self {
            info: Arc::new(Playlist::new(id_less, id)),
            dir,
        };
        playlist.save()?;
        Ok(playlist)
    }

    pub fn edit_info(&mut self, id_less: PlaylistIdLess) -> crate::Result<()> {
        self.info = Arc::new(Playlist::new(id_less, self.info.id()));
        self.save()
    }

    pub fn load(dir: PathBuf, id: PlaylistId) -> crate::Result<Self> {
        let file = dir.join("info.yaml");
        let id_less = yaml_from_file(&file)?;
        Ok(Self {
            info: Arc::new(Playlist::new(id_less, id)),
            dir,
        })
    }

    pub fn delete(self) -> crate::Result<()> {
        crate::files::remove_dir_all(&self.dir)
    }

    pub fn save(&self) -> crate::Result<()> {
        crate::files::create_dir_all(&self.dir)?;
        yaml_to_file(&self.dir.join("info.yaml"), &self.info.id_less())
    }
}
