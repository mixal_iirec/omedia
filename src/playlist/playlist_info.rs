use crate::channel::ChannelId;
use crate::item::matcher::ItemMatcher;

use serde::{Deserialize, Serialize};

pub type PlaylistLocalId = crate::general::Idi32<Playlist>;
pub type PlaylistMap<T> = crate::general::IdMap<Playlist, PlaylistLocalId, T>;

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct PlaylistIdLess {
    name: String,
    matcher: ItemMatcher,
}

impl PlaylistIdLess {
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn matcher(&self) -> &ItemMatcher {
        &self.matcher
    }

    pub fn all(name: &str) -> Self {
        Self {
            name: String::from(name),
            matcher: ItemMatcher::default(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum PlaylistId {
    Global(PlaylistLocalId),
    Channel(ChannelId, PlaylistLocalId),
}

impl PlaylistId {
    pub fn channel(&self) -> Option<ChannelId> {
        match self {
            Self::Global(_) => None,
            Self::Channel(channel, _) => Some(channel.clone()),
        }
    }
}

#[derive(Debug)]
pub struct Playlist {
    id_less: PlaylistIdLess,
    id: PlaylistId,
}
impl Playlist {
    pub fn id(&self) -> PlaylistId {
        self.id.clone()
    }

    pub fn new(id_less: PlaylistIdLess, id: PlaylistId) -> Self {
        Self { id_less, id }
    }
    pub fn id_less(&self) -> &PlaylistIdLess {
        &self.id_less
    }
    pub fn clone_id_less(&self) -> PlaylistIdLess {
        self.id_less.clone()
    }
}

impl std::ops::Deref for Playlist {
    type Target = PlaylistIdLess;
    fn deref(&self) -> &Self::Target {
        &self.id_less
    }
}
