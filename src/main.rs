#![allow(dead_code)]
#![feature(type_alias_impl_trait)]
#![feature(never_type)]

#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate itertools;

use std::sync::mpsc::channel;
use std::sync::Arc;
use std::thread;

mod backend;
mod downloader;
mod files;
mod general;
mod player;
mod scrapers;
mod ui;

mod config;
mod error;
mod msg;
pub use error::{Error, Result};

mod channel;
mod edit_entries;
mod entries;
mod item;
mod playlist;

use backend::run_backend;
use ui::run_tui;

fn main() {
    let config = Arc::new(config::Config::new(std::env::args()));
    rayon::ThreadPoolBuilder::new()
        .num_threads(config.scrape_threads())
        .build_global()
        .unwrap();

    let (tx_ui, rx_ui) = channel();
    let (tx_request, rx_request) = channel();

    let tx_request_copy = tx_request.clone();
    let tx_ui_copy = tx_ui.clone();
    let config_copy = Arc::clone(&config);
    let tui_handle =
        thread::spawn(move || run_tui(config_copy, tx_ui_copy, rx_ui, tx_request_copy));

    run_backend(Arc::clone(&config), tx_ui, rx_request, tx_request);

    tui_handle.join().unwrap();

    let _ = config.save_if_not_already();
}
