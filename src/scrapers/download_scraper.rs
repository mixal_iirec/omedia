use std::fs::read_to_string;
use std::io::{BufRead, BufReader};
use std::path::Path;
use std::process::{Command, Stdio};
use std::sync::{
    mpsc::{Receiver, Sender, TryRecvError},
    Arc,
};

use crate::config::Config;
use crate::downloader::DownloadMsg;
use crate::general::progress::{ProgressId, ProgressMsg};
use crate::msg::{DownloadType, RequestMsg};

#[derive(Clone, Debug)]
pub struct DownloadScraper {
    code: Arc<str>,
}

impl DownloadScraper {
    pub fn new(code: &Path) -> std::io::Result<Self> {
        let mut code = read_to_string(code)?;
        code.push_str(
            "
import sys
dir = sys.argv[3]
if dir != '' and not dir.endswith('/'):
	dir += '/'
info = download(sys.argv[1], sys.argv[2], dir, int(sys.argv[4]), sys.argv[5])
if info[1] != None:
	print(*info[1], sep='\\n', end='\\n')
if info[0] != None:
	print(info[0], end = '\\n')
else:
	print('', end = '\\n')
		",
        );
        Ok(Self {
            code: Arc::from(code),
        })
    }

    pub fn download(
        &self,
        url: &str,
        dir: &Path,
        typ: DownloadType,
        config: Arc<Config>,
        rx: Receiver<DownloadMsg>,
        tx_request: &Sender<RequestMsg>,
        progress_id: ProgressId,
    ) -> crate::Result<(Option<String>, Vec<String>)> {
        let mut process = Command::new("python3")
            .args(&[
                "-u",
                "-c",
                &self.code,
                match typ {
                    DownloadType::MediaOnly => "media_only",
                    DownloadType::MediaAndSubtitles => "media_and_subtitles",
                    DownloadType::SubtitlesOnly => "subtitles_only",
                },
                url,
                dir.to_str().unwrap(),
                &format!("{}", config.max_resolution_download()),
                config.subtitle_languages(),
            ])
            .stdout(Stdio::piped())
            .spawn()
            .map_err(|e| crate::Error::Io(e))?;
        let mut last_line = String::new();
        let mut subtitles = Vec::new();
        for line in BufReader::new(process.stdout.take().unwrap()).lines() {
            let line = match line {
                Ok(line) => line,
                _ => return abort(&mut process),
            };
            match rx.try_recv() {
                Err(TryRecvError::Empty) => (),
                _ => return abort(&mut process),
            }
            let split: Vec<_> = line.split('/').collect();
            if split.len() == 2 {
                if let (Ok(done), Ok(from)) = (
                    usize::from_str_radix(split[0], 10),
                    usize::from_str_radix(split[1], 10),
                ) {
                    let _ = tx_request.send(RequestMsg::ReportProgress {
                        id: progress_id,
                        msg: ProgressMsg::Set { done, from },
                    });
                }
            } else if line.ends_with(".vtt") {
                subtitles.push(line);
            } else {
                last_line = line;
            }
        }
        let out = process
            .wait_with_output()
            .map_err(|e| crate::Error::Io(e))?;
        if !out.status.success() {
            return Err(crate::Error::FileDownloadAborted);
        }
        Ok((
            if last_line.is_empty() {
                None
            } else {
                Some(last_line)
            },
            subtitles,
        ))
    }
}

fn abort(process: &mut std::process::Child) -> crate::Result<(Option<String>, Vec<String>)> {
    let _ = process.kill();
    Err(crate::Error::FileDownloadAborted)
}
