use chrono::prelude::{Local, NaiveDate};
use std::fs::read_to_string;
use std::path::Path;
use std::process::Command;
use std::sync::Arc;

use crate::entries::{EntryMap, ENTRIES};
use crate::item::ItemIdLess;

#[derive(Debug, Clone)]
pub struct ItemScraper {
    code: String,
}

impl ItemScraper {
    pub fn new(code: &Path) -> std::io::Result<Self> {
        let mut code = read_to_string(code)?;
        code.push_str(
            "
import sys
info = scrape_item(sys.argv[1])
for k,e in info.items():
	if isinstance(e, str):
		print('S', k, '=', e.replace('\\n', '\\t\\t'), sep='')
	elif isinstance(e, int):
		print('I', k, '=', e, sep='')
		",
        );
        Ok(Self { code })
    }

    pub fn scrape_item(
        &self,
        url: &str,
        default_bools: crate::item::ItemBools,
    ) -> crate::Result<ItemIdLess> {
        let mut date = Local::today().naive_utc();

        let out = Command::new("python3")
            .args(&["-c", &self.code, url])
            .output()
            .map_err(|e| crate::Error::Io(e))?;
        let err = String::from_utf8(out.stderr)?;
        if !err.is_empty()
            && (err.contains("ERROR") || !err.contains("Some formats are possibly damaged."))
        {
            let mut restricted = None;
            if err.contains("Sign in to confirm your age.") {
                restricted = Some("<AGE RESTRICTED>");
            } else if err.contains("This video is unavailable on this device.") {
                restricted = Some("<DEVICE RESTRICTED>");
            }

            return match restricted {
                Some(restricted) => {
                    let mut string_map: EntryMap<str, Arc<str>> = EntryMap::new();
                    string_map.insert(ENTRIES.string_name_id("title"), Arc::from(restricted));
                    string_map.insert(ENTRIES.string_name_id("url"), Arc::from(url));
                    Ok(ItemIdLess::new(
                        date,
                        string_map,
                        Default::default(),
                        default_bools,
                    ))
                }
                None => Err(crate::Error::UnableToScrapeItem(err)),
            };
        }
        let out = String::from_utf8(out.stdout)?;

        let mut i32map: EntryMap<i32, i32> = EntryMap::new();
        let mut string_map: EntryMap<str, Arc<str>> = EntryMap::new();
        for l in out.lines() {
            let mut split = l.splitn(2, "=");
            if let Some(name) = split.next().map(|v| v.trim()) {
                if let Some(var) = split.next().map(|v| v.trim()) {
                    let (m, name) = name.split_at(1);
                    match m {
                        "I" => {
                            i32map.insert(
                                ENTRIES.i32_name_id(name),
                                var.parse().map_err(|_| crate::Error::TypeGuaranteed)?,
                            );
                        }
                        "S" => match name {
                            "date" => date = NaiveDate::parse_from_str(var, "%Y%m%d")?,
                            name => {
                                string_map.insert(ENTRIES.string_name_id(name), Arc::from(var));
                            }
                        },
                        _ => return Err(crate::Error::TypeGuaranteed),
                    }
                }
            }
        }
        string_map.insert(ENTRIES.string_name_id("url"), Arc::from(url));

        Ok(ItemIdLess::new(date, string_map, i32map, default_bools))
    }
}
