use chrono::prelude::*;
use std::collections::{HashMap, HashSet};
use std::fs::read_to_string;
use std::path::Path;
use std::process::Command;
use std::str::FromStr;
use std::sync::Arc;

use crate::entries::ENTRIES;
use crate::general::progress::{ProgressId, ProgressMsg};
use crate::msg::{RequestMsg, UiMsg};
use std::sync::mpsc::Sender;

use crate::channel::ChannelId;
use crate::config::Config;
use crate::item::ItemIdLess;

use super::download_scraper::DownloadScraper;
use super::item_scraper::ItemScraper;

#[derive(Debug, Clone)]
pub struct ChannelScraper {
    code: String,
    item_scraper: Option<ItemScraper>,
    download_scraper: Option<DownloadScraper>,
}

impl ChannelScraper {
    pub fn downloader(&self) -> &Option<DownloadScraper> {
        &self.download_scraper
    }
    pub fn from_dir(dir: &Path) -> std::io::Result<Self> {
        let item_scraper_path = dir.join("scrape_item.py");
        let download_scraper_path = dir.join("download.py");
        let item_scraper = if item_scraper_path.exists() {
            Some(ItemScraper::new(&item_scraper_path)?)
        } else {
            None
        };
        let download_scraper = if download_scraper_path.exists() {
            Some(DownloadScraper::new(&download_scraper_path)?)
        } else {
            None
        };
        Self::new(
            &dir.join("scrape_channel.py"),
            item_scraper,
            download_scraper,
        )
    }
    pub fn new(
        code: &Path,
        item_scraper: Option<ItemScraper>,
        download_scraper: Option<DownloadScraper>,
    ) -> std::io::Result<Self> {
        let mut code = read_to_string(code)?;
        code.push_str(
            "
import sys
info = scrape_channel(sys.argv[1], sys.argv[2])
for i in info:
	for k,e in i.items():
		if isinstance(e, str):
			print('S', k, '=', e.replace('\\n', '\\t\\t'), sep='')
		elif isinstance(e, int):
			print('I', k, '=', e, sep='')
		",
        );
        Ok(Self {
            code,
            item_scraper,
            download_scraper,
        })
    }

    pub fn get_new_items_exclude(
        &self,
        channel: ChannelId,
        url: &str,
        last_url: &str,
        config: &Config,
        tx_ui: &Sender<UiMsg>,
        progress: ProgressId,
        tx_requests: &Sender<RequestMsg>,
        exclude: &Vec<String>,
    ) {
        let exclude: HashSet<&str> = exclude.iter().map(|s| s.as_str()).collect();
        if let Err(e) = self.__get_new_items_exclude(
            channel,
            url,
            last_url,
            config,
            tx_ui,
            progress,
            tx_requests,
            exclude,
        ) {
            let _ = tx_ui.send(UiMsg::Error(e));
        }
    }

    fn __get_new_items_exclude(
        &self,
        channel: ChannelId,
        url: &str,
        last_url: &str,
        config: &Config,
        tx_ui: &Sender<UiMsg>,
        progress: ProgressId,
        tx_requests: &Sender<RequestMsg>,
        exclude: HashSet<&str>,
    ) -> crate::Result<()> {
        // backwards compatibility for pure yt-dl
        let exclude: HashSet<String> = exclude
            .into_iter()
            .map(|s| s.replace("https://youtu.be/", "https://www.youtube.com/watch?v="))
            .collect();
        let exclude: HashSet<&str> = exclude.iter().map(|s| s.as_str()).collect();

        let mut items = Vec::new();

        let out = Command::new("python3")
            .args(&["-c", &self.code, url, last_url])
            .output()
            .map_err(|e| crate::Error::Io(e))?;
        let out = String::from_utf8(out.stdout)?;

        let mut string_map: HashMap<String, Vec<String>> = HashMap::new();

        for l in out.lines() {
            let mut split = l.splitn(2, "=");
            if let Some(var_name) = split.next().map(|v| v.trim()) {
                if let Some(var) = split.next().map(|v| v.trim()) {
                    match string_map.get_mut(var_name) {
                        Some(vc) => vc.push(String::from(var)),
                        None => {
                            string_map.insert(String::from(var_name), vec![String::from(var)]);
                        }
                    }
                }
            }
        }
        let urls: Vec<String> = string_map.get("Surl").unwrap_or(&Vec::new()).clone();

        match &self.item_scraper {
            Some(item_scraper) => {
                let urls: Vec<String> = urls
                    .into_iter()
                    .filter(|u| !exclude.contains(u.as_str()))
                    .collect();

                tx_ui.send(UiMsg::Progress(progress, ProgressMsg::AddFrom(urls.len())))?;
                for url in urls.iter().rev() {
                    match item_scraper.scrape_item(url, ENTRIES.bools_default()) {
                        Ok(item) => items.push(item),
                        Err(e) => eprintln!("{:#?}", e),
                    }
                    tx_ui.send(UiMsg::Progress(progress, ProgressMsg::AddDone(1)))?;
                    if items.len() >= config.scrape_item_buffer() {
                        tx_requests.send(RequestMsg::FetchedItems { channel, items })?;
                        items = Vec::new();
                    }
                }
            }
            None => {
                let len = urls.len();
                if let Some(_) = string_map.iter().find(|(_, v)| v.len() != len) {
                    return Err(crate::Error::DifArraysLen);
                }

                let dates = string_map
                    .remove("Sdate")
                    .map(|v| {
                        v.into_iter()
                            .map(|s| NaiveDate::parse_from_str(&s, "%Y%m%d"))
                            .collect()
                    })
                    .unwrap_or(Ok(vec![Local::today().naive_utc(); len]))?;

                let (i32s, strings): (Vec<_>, Vec<_>) = string_map
                    .into_iter()
                    .partition(|(k, _)| k.starts_with("I"));
                let map_strings = strings
                    .into_iter()
                    .map(|(k, v)| {
                        (
                            ENTRIES.string_name_id(k.split_at(1).1),
                            v.into_iter().map(|s| Arc::from(s)).collect(),
                        )
                    })
                    .collect();
                let map_i32s = i32s
                    .into_iter()
                    .map(|(k, v)| {
                        Ok((
                            ENTRIES.i32_name_id(k.split_at(1).1),
                            v.into_iter()
                                .map(|v| {
                                    i32::from_str(&v).map_err(|_| crate::Error::TypeGuaranteed)
                                })
                                .collect::<crate::Result<_>>()?,
                        ))
                    })
                    .collect::<crate::Result<_>>()?;

                let i32s_vec = crate::general::entry_map_vec_to_vec_map(map_i32s, urls.len());
                let strings_vec = crate::general::entry_map_vec_to_vec_map(map_strings, urls.len());

                for (url, date, strings, i32s) in izip!(urls, dates, strings_vec, i32s_vec).rev() {
                    if !exclude.contains(url.as_str()) {
                        items.push(ItemIdLess::new(
                            date,
                            strings,
                            i32s,
                            ENTRIES.bools_default(),
                        ));
                    }
                }
            }
        }
        if !items.is_empty() {
            tx_requests.send(RequestMsg::FetchedItems { channel, items })?;
        }
        tx_ui.send(UiMsg::Progress(progress, ProgressMsg::AddDone(1)))?;
        Ok(())
    }
}
