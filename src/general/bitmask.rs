use std::fmt::Debug;
use std::ops::{BitAnd, BitAndAssign, BitOrAssign, Not, Shl};

use serde::{Deserialize, Serialize};

use crate::entries::{EntryMap, ENTRIES};

#[derive(Copy, Clone, Debug)]
pub struct BitMask<T: Copy + Debug> {
    inner: T,
}

impl<
        T: Sized
            + Copy
            + BitAnd<Output = T>
            + BitAndAssign
            + BitOrAssign
            + Not<Output = T>
            + Shl
            + ZeroOne
            + Eq
            + Debug,
    > BitMask<T>
{
    pub fn new() -> Self {
        Self { inner: T::zero() }
    }
    pub fn inner(&self) -> T {
        self.inner
    }
    pub fn len(&self) -> usize {
        std::mem::size_of::<BitMask<T>>() * 8
    }

    pub fn get<I>(&self, i: I) -> bool
    where
        T: Shl<I, Output = T>,
    {
        (self.inner & (T::one() << i)) != T::zero()
    }

    pub fn set<I>(&mut self, i: I, v: bool)
    where
        T: Shl<I, Output = T>,
    {
        let offset = T::one() << i;
        if v {
            self.inner |= offset;
        } else {
            self.inner &= !offset;
        }
    }

    pub fn apply_iter<I>(mut self, it: impl Iterator<Item = (I, bool)>) -> Self
    where
        T: Shl<I, Output = T>,
    {
        for (idx, b) in it {
            self.set(idx, b);
        }
        self
    }
}

impl<T: Copy + Debug> From<T> for BitMask<T> {
    fn from(inner: T) -> Self {
        Self { inner }
    }
}

pub trait ZeroOne {
    fn zero() -> Self;
    fn one() -> Self;
}

impl ZeroOne for u8 {
    fn zero() -> Self {
        0
    }
    fn one() -> Self {
        1
    }
}
impl ZeroOne for u16 {
    fn zero() -> Self {
        0
    }
    fn one() -> Self {
        1
    }
}
impl ZeroOne for u32 {
    fn zero() -> Self {
        0
    }
    fn one() -> Self {
        1
    }
}
impl ZeroOne for u64 {
    fn zero() -> Self {
        0
    }
    fn one() -> Self {
        1
    }
}

impl<
        T: Sized
            + Copy
            + BitAnd<Output = T>
            + BitAndAssign
            + BitOrAssign
            + Not<Output = T>
            + Shl
            + Shl<usize, Output = T>
            + ZeroOne
            + Eq
            + Debug,
    > Serialize for BitMask<T>
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::ser::Serializer,
    {
        ENTRIES
            .bools()
            .into_iter()
            .map(|(idx, _)| (idx, self.get(Into::<usize>::into(idx))))
            .collect::<EntryMap<bool, bool>>()
            .serialize(serializer)
    }
}
impl<
        'de,
        T: Sized
            + Copy
            + BitAnd<Output = T>
            + BitAndAssign
            + BitOrAssign
            + Not<Output = T>
            + Shl
            + Shl<usize, Output = T>
            + ZeroOne
            + Eq
            + Debug,
    > Deserialize<'de> for BitMask<T>
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::de::Deserializer<'de>,
    {
        let de = EntryMap::<bool, bool>::deserialize(deserializer)?;

        Ok(Self::new().apply_iter(de.into_iter().map(|(id, b)| (Into::<usize>::into(id), b))))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn get() {
        let vec = BitMask::<u32>::from(0b01011);
        assert_eq!(true, vec.get(0));
        assert_eq!(true, vec.get(1));
        assert_eq!(false, vec.get(2));
        assert_eq!(true, vec.get(3));
        assert_eq!(false, vec.get(4));
    }

    #[test]
    fn set() {
        let mut vec = BitMask::<u32>::new();
        vec.set(4, true);
        vec.set(2, true);
        vec.set(7, true);
        vec.set(4, false);
        assert_eq!(vec.inner, 0b1000_0100);
    }
}
