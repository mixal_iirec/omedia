mod serde;

use super::Id;
use std::iter::{Extend, FromIterator};
use std::marker::PhantomData;

pub struct IdMap<IDTYPE: ?Sized, ID: Id<IDTYPE>, V> {
    phantom_id_type: PhantomData<IDTYPE>,
    phantom_id: PhantomData<ID>,
    values: Vec<Option<V>>,
}

impl<IDTYPE: ?Sized, ID: Id<IDTYPE>, V: Clone> Clone for IdMap<IDTYPE, ID, V> {
    fn clone(&self) -> Self {
        Self {
            phantom_id_type: PhantomData,
            phantom_id: PhantomData,
            values: self.values.clone(),
        }
    }
}
impl<IDTYPE: ?Sized, ID: Id<IDTYPE>, V: std::fmt::Debug> std::fmt::Debug for IdMap<IDTYPE, ID, V> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.values)
    }
}

impl<IDTYPE: ?Sized, ID: Id<IDTYPE>, V> Default for IdMap<IDTYPE, ID, V> {
    fn default() -> Self {
        Self::new()
    }
}

impl<IDTYPE: ?Sized, ID: Id<IDTYPE>, V> IdMap<IDTYPE, ID, V> {
    pub fn new() -> Self {
        Self {
            phantom_id_type: PhantomData,
            phantom_id: PhantomData,
            values: Vec::new(),
        }
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            phantom_id_type: PhantomData,
            phantom_id: PhantomData,
            values: Vec::with_capacity(capacity),
        }
    }

    pub fn clear(&mut self) {
        self.values.clear();
    }

    pub fn get(&self, idx: ID) -> Option<&V> {
        self.values.get(idx.into()).map(|i| i.as_ref()).flatten()
    }

    pub fn get_mut(&mut self, idx: ID) -> Option<&mut V> {
        self.values
            .get_mut(idx.into())
            .map(|i| i.as_mut())
            .flatten()
    }
    pub fn get_mut_or_insert<F: FnOnce() -> V>(&mut self, idx: ID, default: F) -> &mut V {
        if !self.contains_key(idx) {
            self.insert(idx, default());
        }
        self.get_mut(idx).unwrap()
    }

    pub fn insert(&mut self, idx: ID, v: V) -> Option<V> {
        let idx = idx.into();
        self.expand_size(idx + 1);
        std::mem::replace(self.values.get_mut(idx).unwrap(), Some(v))
    }

    pub fn remove(&mut self, idx: ID) -> Option<V> {
        match self.values.get_mut(idx.into()) {
            Some(r) => std::mem::replace(r, None),
            None => None,
        }
    }

    pub fn contains_key(&self, idx: ID) -> bool {
        self.get(idx).is_some()
    }

    pub fn values(&self) -> impl DoubleEndedIterator<Item = &V> {
        self.values.iter().filter_map(|v| v.as_ref())
    }

    pub fn values_mut(&mut self) -> impl DoubleEndedIterator<Item = &mut V> {
        self.values.iter_mut().filter_map(|v| v.as_mut())
    }

    pub fn into_values(self) -> impl DoubleEndedIterator<Item = V> {
        self.values.into_iter().filter_map(|v| v)
    }

    pub fn iter(&self) -> impl DoubleEndedIterator<Item = (ID, &V)> {
        self.values
            .iter()
            .enumerate()
            .filter_map(|(i, v)| v.as_ref().map(|v| (i.into(), v)))
    }

    pub fn iter_mut(&mut self) -> impl DoubleEndedIterator<Item = (ID, &mut V)> {
        self.values
            .iter_mut()
            .enumerate()
            .filter_map(|(i, v)| v.as_mut().map(|v| (i.into(), v)))
    }

    pub fn was_always_empty(&self) -> bool {
        self.values.is_empty()
    }

    pub fn first_free(&self) -> ID {
        let mut idx = ID::first();
        while self.contains_key(idx) {
            idx.advance();
        }
        idx
    }

    pub fn next_cycle(&self, prev_id: ID) -> ID {
        let mut id = prev_id;
        id.advance();
        while id.into() < self.values.len() {
            if self.contains_key(id) {
                return id;
            }
            id.advance();
        }
        id = ID::first();
        while id.into() < prev_id.into() {
            if self.contains_key(id) {
                return id;
            }
            id.advance();
        }

        id
    }

    //f returns true to accept
    pub fn next_cycle_filter<F: Fn(&V) -> bool>(&self, prev_id: ID, f: F) -> ID {
        let mut id = prev_id;
        id.advance();
        while id.into() < self.values.len() {
            if let Some(v) = self.get(id) {
                if f(v) {
                    return id;
                }
            }
            id.advance();
        }
        id = ID::first();
        while id.into() < prev_id.into() {
            if let Some(v) = self.get(id) {
                if f(v) {
                    return id;
                }
            }
            id.advance();
        }

        id
    }

    fn expand_size(&mut self, size: usize) {
        if size > self.values.len() {
            self.values.resize_with(size, || None);
        }
    }
}

impl<IDTYPE: ?Sized, ID: Id<IDTYPE>, V: Default> IdMap<IDTYPE, ID, V> {
    pub fn get_mut_or_default(&mut self, idx: ID) -> &mut V {
        self.get_mut_or_insert(idx, || Default::default())
    }
}

impl<IDTYPE: ?Sized, V, ID: Id<IDTYPE>> IntoIterator for IdMap<IDTYPE, ID, V> {
    type Item = (ID, V);
    type IntoIter = impl DoubleEndedIterator<Item = (ID, V)>;
    fn into_iter(self) -> Self::IntoIter {
        self.values
            .into_iter()
            .enumerate()
            .filter_map(|(i, v)| v.map(|v| (i.into(), v)))
    }
}

impl<IDTYPE: ?Sized, V, ID: Id<IDTYPE>> FromIterator<(ID, V)> for IdMap<IDTYPE, ID, V> {
    fn from_iter<I: IntoIterator<Item = (ID, V)>>(iter: I) -> Self {
        let mut map = Self::new();
        map.extend(iter);
        map
    }
}

impl<IDTYPE: ?Sized, V, ID: Id<IDTYPE>> Extend<(ID, V)> for IdMap<IDTYPE, ID, V> {
    fn extend<I: IntoIterator<Item = (ID, V)>>(&mut self, iter: I) {
        for (i, v) in iter {
            self.insert(i, v);
        }
    }
}
