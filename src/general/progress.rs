pub type ProgressId = crate::general::Idi32<Progress>;
pub type ProgressMap<T> = crate::general::IdMap<Progress, ProgressId, T>;

#[derive(Debug)]
pub enum ProgressMsg {
    AddFrom(usize),
    AddDone(usize),
    Set { done: usize, from: usize },
    Done,
}

#[derive(Debug)]
pub struct Progress {
    title: String,
    done: usize,
    from: usize,
    finished: Option<bool>,
}

impl Progress {
    pub fn title(&self) -> &str {
        &self.title
    }
    pub fn done(&self) -> usize {
        self.done
    }
    pub fn from(&self) -> usize {
        self.from
    }
    pub fn new(title: String, finish_automatically: bool) -> Self {
        Self {
            title,
            done: 0,
            from: 0,
            finished: (!finish_automatically).then_some(false),
        }
    }

    pub fn msg(&mut self, msg: ProgressMsg) {
        match msg {
            ProgressMsg::AddFrom(from) => self.from += from,
            ProgressMsg::AddDone(done) => self.done += done,
            ProgressMsg::Set { done, from } => {
                self.done = done;
                self.from = from;
            }
            ProgressMsg::Done => self.finished = Some(true),
        }
    }

    pub fn is_finished(&self) -> bool {
        match self.finished {
            Some(finished) => finished,
            None => self.from > 0 && self.done == self.from,
        }
    }

    pub fn is_zero(&self) -> bool {
        self.from == 0 && self.done == 0
    }
}
