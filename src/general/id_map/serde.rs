use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

use super::{Id, IdMap};

impl<IDTYPE: ?Sized, ID: Id<IDTYPE> + Ord + Serialize, V: Clone + Serialize> Serialize
    for IdMap<IDTYPE, ID, V>
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::ser::Serializer,
    {
        let map: BTreeMap<ID, V> = self.iter().map(|(id, v)| (id, v.clone())).collect();
        map.serialize(serializer)
    }
}
impl<'de, IDTYPE: ?Sized, ID: Id<IDTYPE> + Ord + Deserialize<'de>, V: Clone + Deserialize<'de>>
    Deserialize<'de> for IdMap<IDTYPE, ID, V>
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::de::Deserializer<'de>,
    {
        let de = BTreeMap::deserialize(deserializer)?;
        Ok(de.into_iter().collect())
    }
}
