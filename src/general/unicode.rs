use unicode_segmentation::UnicodeSegmentation;
//currently assumed that all graphemes are one 1 character wide; TODO
//see also ui/view.rs: fn draw_progress;

pub fn split_at_grapheme_max(s: &str, at: usize) -> (&str, &str, usize) {
    let mut idx = 0;
    let mut _at = 0;
    for g in s.graphemes(true) {
        idx += g.len();
        _at += 1;
        if _at == at {
            break;
        }
    }
    let (s1, s2) = s.split_at(idx);
    (s1, s2, _at)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn splits() {
        assert_eq!(split_at_grapheme_max("ab", 1), ("a", "b", 1));
        assert_eq!(split_at_grapheme_max("acb", 2), ("ac", "b", 2));
        assert_eq!(split_at_grapheme_max("abc", 50), ("abc", "", 3));
        assert_eq!(split_at_grapheme_max("фывапр", 3), ("фыв", "апр", 3));
        //assert_eq!(split_at_grapheme_max("汉字/漢字", 6), ("汉字/", "漢字", 5));
        //assert_eq!(split_at_grapheme_max("훈민정음", 4), ("훈민", "정음", 4));
    }
}
