use bit_vec::BitVec;

use super::Id;
use std::iter::{Extend, FromIterator};
use std::marker::PhantomData;

pub struct IdSet<IDTYPE: ?Sized, ID: Id<IDTYPE>> {
    phantom_id_type: PhantomData<IDTYPE>,
    phantom_id: PhantomData<ID>,
    values: BitVec,
}

impl<IDTYPE: ?Sized, ID: Id<IDTYPE>> Clone for IdSet<IDTYPE, ID> {
    fn clone(&self) -> Self {
        Self {
            phantom_id_type: PhantomData,
            phantom_id: PhantomData,
            values: self.values.clone(),
        }
    }
}
impl<IDTYPE: ?Sized, ID: Id<IDTYPE>> std::fmt::Debug for IdSet<IDTYPE, ID> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.values)
    }
}

impl<IDTYPE: ?Sized, ID: Id<IDTYPE>> IdSet<IDTYPE, ID> {
    pub fn new() -> Self {
        Self {
            phantom_id_type: PhantomData,
            phantom_id: PhantomData,
            values: BitVec::new(),
        }
    }

    pub fn clear(&mut self) {
        self.values.clear();
    }

    pub fn insert(&mut self, idx: ID) {
        let idx = idx.into();
        self.expand_size(idx + 1);
        self.values.set(idx, true);
    }

    pub fn remove(&mut self, idx: ID) {
        let idx = idx.into();
        if idx < self.values.len() {
            self.values.set(idx, false);
        }
    }

    pub fn contains(&self, idx: ID) -> bool {
        self.values.get(idx.into()).unwrap_or(false)
    }

    pub fn iter(&self) -> impl Iterator<Item = ID> + '_ {
        (0..self.values.len())
            .map(|i| i.into())
            .filter(move |i| self.contains(*i))
    }

    fn expand_size(&mut self, size: usize) {
        if size > self.values.len() {
            self.values.grow(size - self.values.len(), false);
        }
    }
}

impl<IDTYPE: ?Sized, ID: Id<IDTYPE>> FromIterator<ID> for IdSet<IDTYPE, ID> {
    fn from_iter<I: IntoIterator<Item = ID>>(iter: I) -> Self {
        let mut map = Self::new();
        map.extend(iter);
        map
    }
}

impl<IDTYPE: ?Sized, ID: Id<IDTYPE>> Extend<ID> for IdSet<IDTYPE, ID> {
    fn extend<I: IntoIterator<Item = ID>>(&mut self, iter: I) {
        for i in iter {
            self.insert(i);
        }
    }
}
