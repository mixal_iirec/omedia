#![allow(dead_code)]

mod bitmask;
mod id;
mod id_map;
mod id_set;
pub mod progress;
pub mod unicode;

pub use bitmask::BitMask;
pub use id::{Id, Idi32};
pub use id_map::IdMap;
pub use id_set::IdSet;

use crate::entries::EntryMap;

pub fn entry_map_vec_to_vec_map<Id: ?Sized, I: Clone>(
    map: EntryMap<Id, Vec<I>>,
    len: usize,
) -> Vec<EntryMap<Id, I>> {
    let mut out = vec![EntryMap::new(); len];
    for (k, vec) in map {
        for (v, m) in vec.into_iter().zip(out.iter_mut()) {
            m.insert(k, v);
        }
    }
    out
}

pub struct ListMarker;
pub type ListId = Idi32<ListMarker>;
pub type ListMap<T> = IdMap<ListMarker, ListId, T>;
