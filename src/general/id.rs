use std::cmp::Ordering;
use std::marker::PhantomData;

pub trait Id<IDTYPE: ?Sized>: Into<usize> + From<usize> + Copy {
    fn first() -> Self;
    fn next(self) -> Self;
    fn advance(&mut self) -> Self;
}

pub struct Idi32<IDTYPE: ?Sized> {
    phantom_id_type: PhantomData<IDTYPE>,
    v: i32,
}

impl<IDTYPE: ?Sized> Copy for Idi32<IDTYPE> {}
impl<IDTYPE: ?Sized> Clone for Idi32<IDTYPE> {
    fn clone(&self) -> Self {
        Self {
            phantom_id_type: PhantomData,
            v: self.v,
        }
    }
}
impl<IDTYPE: ?Sized> Eq for Idi32<IDTYPE> {}
impl<IDTYPE: ?Sized> PartialEq for Idi32<IDTYPE> {
    fn eq(&self, o: &Self) -> bool {
        self.v == o.v
    }
}
impl<IDTYPE: ?Sized> std::hash::Hash for Idi32<IDTYPE> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.v.hash(state)
    }
}
impl<IDTYPE: ?Sized> std::fmt::Debug for Idi32<IDTYPE> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.v)
    }
}
impl<IDTYPE: ?Sized> std::fmt::Display for Idi32<IDTYPE> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.v)
    }
}

impl<IDTYPE: ?Sized> Id<IDTYPE> for Idi32<IDTYPE> {
    fn first() -> Self {
        Self {
            phantom_id_type: PhantomData,
            v: 0,
        }
    }

    fn next(self) -> Self {
        Self {
            phantom_id_type: PhantomData,
            v: self.v + 1,
        }
    }

    fn advance(&mut self) -> Self {
        let old = *self;
        *self = self.next();
        old
    }
}

impl<IDTYPE: ?Sized> Into<usize> for Idi32<IDTYPE> {
    fn into(self) -> usize {
        self.v as usize
    }
}

impl<IDTYPE: ?Sized> From<usize> for Idi32<IDTYPE> {
    fn from(u: usize) -> Self {
        Self {
            phantom_id_type: PhantomData,
            v: u as i32,
        }
    }
}

impl<IDTYPE: ?Sized> PartialEq<usize> for Idi32<IDTYPE> {
    fn eq(&self, other: &usize) -> bool {
        self.v as usize == *other
    }
}

impl<IDTYPE: ?Sized> Ord for Idi32<IDTYPE> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.v.cmp(&other.v)
    }
}

impl<IDTYPE: ?Sized> PartialOrd for Idi32<IDTYPE> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
