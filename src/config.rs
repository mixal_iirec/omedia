use std::path::{Path, PathBuf};
use std::sync::Arc;

use serde::{Deserialize, Serialize};

use crate::entries::{EntryMap, ENTRIES};
use crate::files::{yaml_from_file, yaml_to_file};
use crate::item::ItemBools;

#[derive(Serialize, Deserialize)]
pub struct ConfigInner {
    editor: String,

    dir: PathBuf,
    ytdl: PathBuf,

    max_resolution_stream: usize,
    max_resolution_download: usize,
    subtitle_languages: String,

    multiple_visible_tabs: bool,
    enum_lists: bool,

    scrape_threads: usize,
    scrape_item_buffer: usize,
}

impl ConfigInner {
    fn noedit_file(&self) -> PathBuf {
        self.dir.join("noedit.yaml")
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename = "entry")]
pub struct BoolEntry {
    pub name: Arc<str>,
    pub default: bool,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(transparent)]
pub struct BoolEntries {
    pub entries: Vec<BoolEntry>,
}
impl BoolEntries {
    fn new() -> Self {
        Self {
            entries: vec![
                BoolEntry {
                    name: Arc::from("interested"),
                    default: true,
                },
                BoolEntry {
                    name: Arc::from("seen"),
                    default: false,
                },
            ],
        }
    }

    pub fn bool_default(&self) -> ItemBools {
        ItemBools::new().apply_iter(self.entries.iter().map(|i| i.default).enumerate())
    }

    fn set_entries(&self) {
        let mut bool_names = EntryMap::new();

        for (i, entry) in self.entries.iter().enumerate() {
            bool_names.insert(i.into(), Arc::clone(&entry.name));
        }

        ENTRIES.setup_bools(bool_names, self.bool_default());
    }
}

pub struct Config {
    cfg_folder: PathBuf,
    pub bool_entries: BoolEntries,
    c: ConfigInner,
}

impl Config {
    pub fn dir(&self) -> &Path {
        &self.c.dir
    }

    pub fn editor(&self) -> &str {
        &self.c.editor
    }
    pub fn max_resolution_stream(&self) -> usize {
        self.c.max_resolution_stream
    }
    pub fn max_resolution_download(&self) -> usize {
        self.c.max_resolution_download
    }
    pub fn multiple_visible_tabs(&self) -> bool {
        self.c.multiple_visible_tabs
    }
    pub fn enum_lists(&self) -> bool {
        self.c.enum_lists
    }
    pub fn subtitle_languages(&self) -> &str {
        &self.c.subtitle_languages
    }

    pub fn scrape_item_buffer(&self) -> usize {
        self.c.scrape_item_buffer
    }

    pub fn ytdl(&self) -> &Path {
        &self.c.ytdl
    }

    pub fn scrape_threads(&self) -> usize {
        self.c.scrape_threads
    }

    fn cfg_file(cfg_folder: &Path) -> PathBuf {
        cfg_folder.join("config.yaml")
    }
    pub fn save_bool_entries(&self, entries: &BoolEntries) -> crate::Result<()> {
        yaml_to_file(&self.c.noedit_file(), entries)
    }

    pub fn new(_: std::env::Args) -> Self {
        let cfg_folder = PathBuf::from("./");
        let cfg_file = Self::cfg_file(&cfg_folder);

        let c = if !cfg_file.exists() {
            ConfigInner {
                editor: String::from("vim"),

                dir: PathBuf::from("data"),
                ytdl: PathBuf::from("yt-dlp"),

                multiple_visible_tabs: true,
                enum_lists: false,

                max_resolution_stream: 720,
                max_resolution_download: 1080,
                subtitle_languages: String::from("en en-GB en-US"),

                scrape_threads: 8,
                scrape_item_buffer: 128,
            }
        } else {
            let c = yaml_from_file(&cfg_file);
            match c {
                Ok(c) => c,
                Err(e) => {
                    eprintln!("{e}");
                    std::process::exit(1);
                }
            }
        };

        let bool_entries = yaml_from_file(&c.noedit_file()).unwrap_or(BoolEntries::new());

        bool_entries.set_entries();

        Self {
            cfg_folder,
            bool_entries,
            c,
        }
    }

    pub fn save_if_not_already(&self) -> crate::Result<()> {
        let cfg_file = Self::cfg_file(&self.cfg_folder);
        if !cfg_file.exists() {
            yaml_to_file(&cfg_file, &self.c)?;
        }

        let noedit = self.c.noedit_file();
        if !noedit.exists() {
            self.save_bool_entries(&self.bool_entries)?;
        }

        Ok(())
    }
}
