use std::path::PathBuf;

use crate::channel::{ChannelDir, ChannelIdLess};
use crate::files::{create_dir_all, prepare_string_for_filepath, read_dir};
use crate::playlist::{PlaylistDir, PlaylistId, PlaylistIdLess};

use super::Handler;

impl Handler {
    pub(super) fn dir_playlists(&self) -> PathBuf {
        self.config.dir().join("playlists")
    }
    pub(super) fn dir_channels(&self) -> PathBuf {
        self.config.dir().join("channels")
    }

    pub(super) fn ensure_setup(&self) -> crate::Result<()> {
        create_dir_all(self.config.dir())?;
        create_dir_all(&self.dir_channels())?;
        create_dir_all(&self.dir_playlists())?;
        Ok(())
    }

    pub(super) fn load(&mut self) -> crate::Result<()> {
        for dir in read_dir(&self.dir_channels())? {
            self.insert_channel(|id| ChannelDir::load(dir.path(), id))?;
        }
        for dir in read_dir(&self.dir_playlists())? {
            self.insert_playlist(|id| PlaylistDir::load(dir.path(), PlaylistId::Global(id)))?;
        }
        if self.playlists.was_always_empty() {
            self.add_global_playlist(PlaylistIdLess::all("All"))?;
        }
        Ok(())
    }

    pub fn add_channel(&mut self, channel: ChannelIdLess) -> crate::Result<()> {
        let dir = self
            .dir_channels()
            .join(prepare_string_for_filepath(channel.name()));
        if dir.exists() {
            return Err(crate::Error::FileAlreadyExists);
        }

        self.insert_channel(|id| ChannelDir::create(dir, channel, id))
    }

    pub fn add_global_playlist(&mut self, playlist: PlaylistIdLess) -> crate::Result<()> {
        let dir = self
            .dir_playlists()
            .join(prepare_string_for_filepath(playlist.name()));
        if dir.exists() {
            return Err(crate::Error::FileAlreadyExists);
        }

        self.insert_playlist(|id| PlaylistDir::create(dir, playlist, PlaylistId::Global(id)))
    }
}
