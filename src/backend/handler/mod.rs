#![warn(dead_code)]

use std::sync::mpsc::Sender;
use std::sync::Arc;

use crate::entries::EntrySet;
use crate::general::progress::{ProgressId, ProgressMsg};
use crate::msg::{DownloadType, EditMsg, RequestMsg, UiMsg};

use crate::channel::{Channel, ChannelId, ChannelIdLess, ChannelMap};
use crate::config::Config;
use crate::downloader::Downloader;
use crate::item::{ChItemId, Item, ItemIdLess};
use crate::playlist::{Playlist, PlaylistId, PlaylistIdLess, PlaylistLocalId};

mod back;
mod files;
use back::HandlerBack;

pub struct Handler {
    back: HandlerBack,

    config: Arc<Config>,
    downloader: Downloader,
}

impl std::ops::Deref for Handler {
    type Target = HandlerBack;
    fn deref(&self) -> &Self::Target {
        &self.back
    }
}
impl std::ops::DerefMut for Handler {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.back
    }
}

impl Handler {
    pub fn channels_info(&self) -> ChannelMap<Arc<Channel>> {
        self.channels
            .iter()
            .map(|(id, c)| (id, Arc::clone(c.info())))
            .collect()
    }

    pub fn downloader_mut(&mut self) -> &mut Downloader {
        &mut self.downloader
    }
    pub fn downloader(&self) -> &Downloader {
        &self.downloader
    }

    pub fn new(config: Arc<Config>, downloader: Downloader) -> crate::Result<Self> {
        let mut data = Self {
            back: HandlerBack::new(),
            config,
            downloader,
        };
        data.ensure_setup()?;
        data.load()?;
        Ok(data)
    }

    pub fn fetch_items(
        &mut self,
        ui_tx: &Sender<UiMsg>,
        progress: ProgressId,
        tx_requests: &Sender<RequestMsg>,
        all: bool,
    ) -> crate::Result<()> {
        for channel in self.channels.values() {
            if !channel.auto_fetch() && !all {
                continue;
            }
            if let Some(scraper) = channel.channel_scraper() {
                ui_tx.send(UiMsg::Progress(progress, ProgressMsg::AddFrom(1)))?;
                let scraper = scraper.clone();
                let ui_tx = ui_tx.clone();
                let tx_requests = tx_requests.clone();
                let config = Arc::clone(&self.config);
                let urls = channel.fetch_items_info()?;
                let channel_id = channel.id();
                let url = String::from(channel.url());
                rayon::spawn(move || {
                    scraper.get_new_items_exclude(
                        channel_id,
                        &url,
                        if all {
                            ""
                        } else {
                            urls.last().map(|s| s.as_str()).unwrap_or("")
                        },
                        &config,
                        &ui_tx,
                        progress,
                        &tx_requests,
                        &urls,
                    )
                });
            }
        }

        Ok(())
    }

    pub fn insert_fetched_items(
        &mut self,
        channel: ChannelId,
        items: Vec<ItemIdLess>,
    ) -> crate::Result<()> {
        self.channel_mut(channel)?.insert_fetched_items(items)
    }

    fn get_global_item_list(&self) -> Vec<&Item> {
        self.channels
            .values()
            .map(|c| c.items().into_iter())
            .flatten()
            .collect()
    }

    pub fn add_channels(&mut self, channels: Vec<ChannelIdLess>) -> crate::Result<()> {
        Ok(for c in channels {
            self.add_channel(c)?;
        })
    }

    pub fn add_playlists(
        &mut self,
        channel: Option<ChannelId>,
        playlists: Vec<PlaylistIdLess>,
    ) -> crate::Result<()> {
        match channel {
            Some(channel) => {
                for p in playlists {
                    self.channel_mut(channel)?.add_playlist(p)?;
                }
            }
            None => {
                for p in playlists {
                    self.add_global_playlist(p)?;
                }
            }
        }
        Ok(())
    }

    pub fn list_from_ids<It: IntoIterator<Item = ChItemId>>(&self, it: It) -> Vec<Item> {
        it.into_iter()
            .filter_map(|(c, item)| self.channels.get(c)?.item(item))
            .cloned()
            .collect()
    }
    pub fn playlists_id_less_from_ids<It: IntoIterator<Item = PlaylistId>>(
        &self,
        it: It,
    ) -> Vec<PlaylistIdLess> {
        it.into_iter()
            .filter_map(|p| {
                match p {
                    PlaylistId::Channel(c, p) => self.channels.get(c)?.playlist(p),
                    PlaylistId::Global(p) => self.playlists.get(p),
                }
                .map(|p| p.info().clone_id_less())
            })
            .collect()
    }

    pub fn playlist_items(
        &self,
        playlist_id: PlaylistId,
    ) -> crate::Result<(Arc<Playlist>, Vec<Item>)> {
        match playlist_id {
            PlaylistId::Channel(channel, playlist) => {
                self.channel(channel)?.playlist_items(playlist)
            }
            PlaylistId::Global(playlist) => self.global_playlist_items(playlist),
        }
        .ok_or(crate::Error::InvalidId)
    }

    fn load_strings(&self, name: &str, vec: Vec<(&mut Option<String>, &Item)>) {
        for (channel, vec) in
            self.to_channels(vec.into_iter().map(|(a, i)| (i.channel_id(), (a, i))))
        {
            let _ = channel.load_items_string_var(name, vec);
        }
    }

    pub fn global_playlist_items(&self, id: PlaylistLocalId) -> Option<(Arc<Playlist>, Vec<Item>)> {
        self.playlists.get(id).map(|i| {
            (
                Arc::clone(i.info()),
                self.filter_playlist(
                    i.info().id_less(),
                    self.get_global_item_list().into_iter().cloned().collect(),
                ),
            )
        })
    }

    pub fn filter_playlist(&self, playlist: &PlaylistIdLess, items: Vec<Item>) -> Vec<Item> {
        playlist.matcher().filter(
            items,
            |name, vec| self.load_strings(name, vec),
            |c, s| {
                self.channels
                    .get(c)
                    .map(|c| c.down_paths_into_load_on_read(s))
                    .unwrap_or(Vec::new())
            },
        )
    }

    pub fn edit(&mut self, edit: EditMsg) -> crate::Result<bool> {
        match edit {
            EditMsg::Bool(id, vec) => {
                for (channel, vec) in self.to_channels_tuple_mut(vec) {
                    channel.edit_bools(id, vec)?;
                }
            }
            EditMsg::I32(id, vec) => {
                for (channel, vec) in self.to_channels_tuple_mut(vec) {
                    channel.edit_i32s(id, vec)?;
                }
            }
            EditMsg::Str(id, vec) => {
                for (channel, vec) in self.to_channels_tuple_mut(vec) {
                    channel.edit_strings(id, vec)?;
                }
            }
            EditMsg::AddFilePaths(id, vec) => {
                for (channel, vec) in self.to_channels_tuple_mut(vec) {
                    channel.edit_add_file_paths(id, vec)?;
                }
            }
            EditMsg::Channel(vec) => {
                for (channel, info) in vec {
                    self.channel_mut(channel)?.edit_info(info)?;
                }
            }
            EditMsg::Playlist(vec) => {
                for (playlist, info) in vec {
                    match playlist {
                        PlaylistId::Channel(channel, playlist) => {
                            self.channel_mut(channel)?.playlist_mut(playlist)
                        }
                        PlaylistId::Global(playlist) => self.playlists.get_mut(playlist),
                    }
                    .ok_or(crate::Error::InvalidId)?
                    .edit_info(info)?;
                }
            }
            EditMsg::BoolEntries(new) => {
                // errors are ignored - as written there is no way to recover,
                // so continue to maximize correctly transformed entries

                let old = &self.config.bool_entries;

                let edit_entries = crate::edit_entries::EditBoolEntries::new(old, &new);
                for channel in self.channels.values_mut() {
                    match channel.transform_entries(&edit_entries) {
                        Ok(_) => (),
                        Err(error) => eprintln!("{error:?}"),
                    }
                }

                match self.config.save_bool_entries(&new) {
                    Ok(_) => (),
                    Err(error) => eprintln!("{error:?}"),
                }

                // this will mess up all established EntryId<bool>, so reboot
                return Ok(true);
            }
        }
        Ok(false)
    }

    pub fn delete_items(&mut self, ids: Vec<ChItemId>) -> crate::Result<()> {
        for (channel, vec) in self.to_channels_mut(ids) {
            channel.delete_items(vec)?;
        }
        Ok(())
    }

    pub fn delete_item_media(&mut self, ids: Vec<ChItemId>) -> crate::Result<()> {
        for (channel, vec) in self.to_channels_mut(ids) {
            channel.delete_item_media(&vec)?;
        }
        Ok(())
    }

    pub fn download_items(
        &mut self,
        ids: &[(ChItemId, ProgressId)],
        typ: DownloadType,
    ) -> crate::Result<()> {
        for (channel, vec) in self.back.to_channels_tuple_mut(ids.iter().cloned()) {
            if let Some(download_scraper) = channel.download_scraper().cloned() {
                let dir_downloads = channel.dir_downloads();
                let urls = channel.items_string_var("url", vec.iter().map(|(i, _)| *i))?;
                for ((id, progress), url) in vec.into_iter().zip(urls.into_iter()) {
                    if let Some(url) = url {
                        self.downloader.download((
                            (url, dir_downloads.clone(), download_scraper.clone()),
                            (channel.id(), id),
                            progress,
                            typ,
                        ));
                    }
                }
            }
        }
        Ok(())
    }

    pub fn get_media_path(
        &mut self,
        ids: &[ChItemId],
    ) -> crate::Result<Vec<(String, f64, ChItemId)>> {
        let mut path_and_continue_at = vec![(String::new(), 0.); ids.len()];
        for (channel, mut list) in
            self.to_channels_tuple_mut(ids.iter().cloned().zip(path_and_continue_at.iter_mut()))
        {
            channel.get_media_paths(&mut list)?;
        }
        Ok(path_and_continue_at
            .into_iter()
            .zip(ids.iter().cloned())
            .map(|((a, b), c)| (a, b, c))
            .collect())
    }

    pub fn load_strings_needed(
        &self,
        needed: &EntrySet<str>,
        items: &mut [Item],
    ) -> crate::Result<()> {
        for (channel, items) in self.to_channels(items.iter_mut().map(|i| (i.channel_id(), i))) {
            channel.load_strings_needed(needed, items)?;
        }
        Ok(())
    }
    pub fn fully_load_items(&self, items: &mut [Item]) -> crate::Result<()> {
        for (channel, mut items) in self.to_channels(items.iter_mut().map(|i| (i.channel_id(), i)))
        {
            channel.fully_load_items(&mut items)?;
        }
        Ok(())
    }
}
