use std::sync::Arc;

use crate::channel::{Channel, ChannelDir, ChannelId, ChannelMap};
use crate::general::Id;
use crate::item::{ChItemId, ItemId};
use crate::playlist::{Playlist, PlaylistDir, PlaylistId, PlaylistLocalId, PlaylistMap};

pub struct HandlerBack {
    pub(super) channels: ChannelMap<ChannelDir>,
    pub(super) playlists: PlaylistMap<PlaylistDir>,

    next_channel_id: ChannelId,
    next_playlist_id: PlaylistLocalId,
}
impl HandlerBack {
    pub(super) fn channel(&self, id: ChannelId) -> crate::Result<&ChannelDir> {
        self.channels.get(id).ok_or(crate::Error::InvalidId)
    }
    pub(super) fn channel_mut(&mut self, id: ChannelId) -> crate::Result<&mut ChannelDir> {
        self.channels.get_mut(id).ok_or(crate::Error::InvalidId)
    }

    pub(super) fn new() -> Self {
        Self {
            channels: ChannelMap::new(),
            playlists: PlaylistMap::new(),

            next_channel_id: ChannelId::first(),
            next_playlist_id: PlaylistLocalId::first(),
        }
    }

    pub(super) fn insert_playlist<F>(&mut self, f: F) -> crate::Result<()>
    where
        F: FnOnce(PlaylistLocalId) -> crate::Result<PlaylistDir>,
    {
        self.playlists
            .insert(self.next_playlist_id, f(self.next_playlist_id)?);
        self.next_playlist_id.advance();
        Ok(())
    }

    pub(super) fn insert_channel<F>(&mut self, f: F) -> crate::Result<()>
    where
        F: FnOnce(ChannelId) -> crate::Result<ChannelDir>,
    {
        self.channels
            .insert(self.next_channel_id, f(self.next_channel_id)?);
        self.next_channel_id.advance();
        Ok(())
    }

    pub(super) fn to_channels_tuple_mut<T, I: IntoIterator<Item = (ChItemId, T)>>(
        &mut self,
        it: I,
    ) -> impl Iterator<Item = (&mut ChannelDir, Vec<(ItemId, T)>)> {
        self.to_channels_mut(it.into_iter().map(|((c, i), t)| (c, (i, t))))
    }

    pub(super) fn to_channels<T, I: IntoIterator<Item = (ChannelId, T)>>(
        &self,
        it: I,
    ) -> impl Iterator<Item = (&ChannelDir, Vec<T>)> {
        let mut map: ChannelMap<_> = self
            .channels
            .iter()
            .map(|(i, c)| (i, (c, Vec::new())))
            .collect();
        for (channel, v) in it {
            if let Some((_, vec)) = map.get_mut(channel) {
                vec.push(v);
            }
        }
        map.into_values().filter(|(_, v)| !v.is_empty())
    }

    pub(super) fn to_channels_mut<T, I: IntoIterator<Item = (ChannelId, T)>>(
        &mut self,
        it: I,
    ) -> impl Iterator<Item = (&mut ChannelDir, Vec<T>)> {
        let mut map: ChannelMap<_> = self
            .channels
            .iter_mut()
            .map(|(i, c)| (i, (c, Vec::new())))
            .collect();
        for (channel, v) in it {
            if let Some((_, vec)) = map.get_mut(channel) {
                vec.push(v);
            }
        }
        map.into_values().filter(|(_, v)| !v.is_empty())
    }

    pub fn delete_channels(&mut self, ids: Vec<ChannelId>) -> crate::Result<()> {
        Ok(for id in ids {
            self.delete_channel(id)?
        })
    }
    pub fn delete_channel(&mut self, id: ChannelId) -> crate::Result<()> {
        self.channels
            .remove(id)
            .ok_or(crate::Error::InvalidId)?
            .delete()
    }

    pub fn delete_playlists(&mut self, ids: Vec<PlaylistId>) -> crate::Result<()> {
        Ok(for id in ids {
            self.delete_playlist(id)?
        })
    }
    pub fn delete_playlist(&mut self, playlist: PlaylistId) -> crate::Result<()> {
        match playlist {
            PlaylistId::Channel(channel, playlist) => {
                self.channel_mut(channel)?.delete_playlist(playlist)
            }
            PlaylistId::Global(playlist) => self
                .playlists
                .remove(playlist)
                .ok_or(crate::Error::InvalidId)?
                .delete(),
        }
    }

    pub fn playlists_info(
        &self,
        channel: Option<ChannelId>,
    ) -> crate::Result<(Option<Arc<Channel>>, Vec<Arc<Playlist>>)> {
        match channel {
            Some(channel) => {
                let channel = self.channel(channel)?;
                Ok((Some(Arc::clone(channel.info())), channel.playlists_info()))
            }
            None => Ok((
                None,
                self.playlists
                    .values()
                    .map(|c| Arc::clone(c.info()))
                    .collect(),
            )),
        }
    }
}
