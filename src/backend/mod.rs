use std::sync::mpsc::{Receiver, Sender};
use std::sync::Arc;

use crate::config::Config;
use crate::downloader::Downloader;
use crate::msg::{ReqUpdateMsg, RequestMsg, UiMsg, UpdateMsg};

mod handler;
use handler::Handler;

pub fn run_backend(
    config: Arc<Config>,
    ui_tx: Sender<UiMsg>,
    request_rx: Receiver<RequestMsg>,
    tx_requests: Sender<RequestMsg>,
) {
    let mut handler = match Handler::new(
        Arc::clone(&config),
        Downloader::new(config, tx_requests.clone()),
    ) {
        Ok(h) => h,
        Err(e) => {
            eprintln!("Error while loading handler: {}", e);
            std::process::exit(1);
        }
    };

    for msg in request_rx.iter() {
        match handle_msg(&mut handler, &ui_tx, &tx_requests, msg) {
            Ok(true) => {
                let _ = ui_tx.send(UiMsg::End);
                break;
            }
            Ok(false) => (),
            Err(crate::Error::SendError) => {
                eprintln!("Error while sending message from backend!");
                break;
            }
            Err(e) => ui_tx.send(UiMsg::Error(e)).unwrap(),
        }
    }
}

pub fn handle_msg(
    handler: &mut Handler,
    ui_tx: &Sender<UiMsg>,
    tx_requests: &Sender<RequestMsg>,
    msg: RequestMsg,
) -> crate::Result<bool> {
    match msg {
        RequestMsg::RequestChannels => {
            ui_tx.send(UiMsg::ReceiveChannels(handler.channels_info()))?
        }
        RequestMsg::RequestPlaylists { list, channel } => {
            let (channel, playlists) = handler.playlists_info(channel)?;
            ui_tx.send(UiMsg::ReceivePlaylists(list, channel, playlists))?;
        }
        RequestMsg::AddChannels { channels } => handler.add_channels(channels)?,
        RequestMsg::DeleteChannels { channels } => handler.delete_channels(channels)?,
        RequestMsg::RequestPlaylist {
            list,
            id,
            str_needed,
        } => {
            let (playlist, mut items) = handler.playlist_items(id)?;
            handler.load_strings_needed(&str_needed, &mut items)?;
            ui_tx.send(UiMsg::ReceivePlaylist(list, playlist, items))?;
        }
        RequestMsg::AddPlaylists { channel, playlists } => {
            handler.add_playlists(channel, playlists)?
        }
        RequestMsg::DeletePlaylists { ids } => handler.delete_playlists(ids)?,
        RequestMsg::FetchItems { all, progress } => {
            handler.fetch_items(&ui_tx, progress, &tx_requests, all)?
        }
        RequestMsg::FetchedItems { channel, items } => {
            handler.insert_fetched_items(channel, items)?
        }
        RequestMsg::Edit(edit) => return handler.edit(edit),
        RequestMsg::DeleteItems { ids } => handler.delete_items(ids)?,
        RequestMsg::DeleteItemMedia { ids } => handler.delete_item_media(ids)?,
        RequestMsg::DownloadItems { typ, list } => handler.download_items(&list, typ)?,
        RequestMsg::RequestPlayData { list } => {
            ui_tx.send(UiMsg::ReceivePlayData(handler.get_media_path(&list)?))?
        }
        RequestMsg::RequestDownloadList { list, str_needed } => {
            let mut items = handler.list_from_ids(handler.downloader().list_downloads());
            handler.load_strings_needed(&str_needed, &mut items)?;
            let done = handler.downloader().done();
            ui_tx.send(UiMsg::ReceiveDownloadList(list, items, done))?;
        }
        RequestMsg::AbortDownload { ids } => handler.downloader_mut().abort_download(ids),
        RequestMsg::ReportProgress { id, msg } => ui_tx.send(UiMsg::Progress(id, msg))?,
        RequestMsg::CopyItems { ids, into } => {
            let mut items = handler.list_from_ids(ids);
            handler.fully_load_items(&mut items)?;
            let items = items.into_iter().map(|it| it.into_id_less()).collect();
            handler.insert_fetched_items(into, items)?;
            ui_tx.send(UiMsg::Msg("Items pasted!"))?;
        }
        RequestMsg::CopyPlaylists { ids, into } => {
            let playlists = handler.playlists_id_less_from_ids(ids);
            handler.add_playlists(into, playlists)?;
            ui_tx.send(UiMsg::Msg("Playlists pasted!"))?;
        }
        RequestMsg::RequestUpdate { list, msg } => {
            let update_msg = match msg {
                ReqUpdateMsg::Playlists { channel } => {
                    UpdateMsg::Playlists(handler.playlists_info(channel)?.1)
                }
                ReqUpdateMsg::Playlist { id, str_needed } => {
                    let (_, mut items) = handler.playlist_items(id)?;
                    handler.load_strings_needed(&str_needed, &mut items)?;
                    UpdateMsg::Items(items)
                }
                ReqUpdateMsg::ItemsStrNeeded {
                    mut items,
                    str_needed,
                } => {
                    handler.load_strings_needed(&str_needed, &mut items)?;
                    UpdateMsg::Items(items)
                }
                ReqUpdateMsg::CustomPlaylist { items, playlist } => {
                    UpdateMsg::Items(handler.filter_playlist(&playlist, items))
                }
                ReqUpdateMsg::DownloadList { str_needed } => {
                    let mut items = handler.list_from_ids(handler.downloader().list_downloads());
                    handler.load_strings_needed(&str_needed, &mut items)?;
                    let done = handler.downloader().done();
                    UpdateMsg::DownloadList { items, done }
                }
                ReqUpdateMsg::AddItemsFromIds { items, str_needed } => {
                    let mut items = handler.list_from_ids(items);
                    handler.load_strings_needed(&str_needed, &mut items)?;
                    UpdateMsg::AddItems(items)
                }
            };
            ui_tx.send(UiMsg::Update(list, update_msg))?;
        }
        RequestMsg::FileDowloaded => {
            ui_tx.send(UiMsg::Msg("File download done!"))?;
            handler.downloader_mut().download_finished()?;
        }
        RequestMsg::End => {
            return Ok(true);
        }
    }
    Ok(false)
}
