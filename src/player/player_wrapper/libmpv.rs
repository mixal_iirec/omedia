use libmpv::events::{Event, EventContext, PropertyData};
use libmpv::{mpv_end_file_reason, Format, Mpv};

use std::collections::{HashMap, VecDeque};
use std::path::PathBuf;

use super::{FileFinishReason, PlayerBuilderTrait, PlayerEvent, PlayerWrapperTrait};

fn form(err: impl std::fmt::Display) -> String {
    format!("{err}")
}

pub struct PlayerBuilder {
    mpv: Mpv,
}

impl PlayerBuilderTrait for PlayerBuilder {
    type PlayerWrapper = PlayerWrapper;

    fn new() -> Result<Self, String> {
        let mpv = Mpv::new().map_err(form)?;

        mpv.set_property("options/osc", true).map_err(form)?;
        mpv.set_property("options/input-vo-keyboard", true)
            .map_err(form)?;
        mpv.set_property("options/input-default-bindings", true)
            .map_err(form)?;
        mpv.set_property("options/input-media-keys", true)
            .map_err(form)?;
        mpv.set_property("options/force-window", "immediate")
            .map_err(form)?;
        mpv.set_property("options/idle", "once").map_err(form)?;

        Ok(Self { mpv })
    }

    fn set_config_log_dir(&mut self, dir: &str) -> Result<(), String> {
        let mut path = PathBuf::from(dir);
        path.push("mpv.conf");
        if path.exists() {
            let path = path.canonicalize().map_err(form)?;
            self.mpv.load_config(path.to_str().unwrap()).map_err(form)?
        }
        Ok(())
    }
    fn set_height_resolution(&mut self, height: usize) -> Result<(), String> {
        let ytdl_format = format!("bestvideo[height<={height}]+bestaudio/best[height<={height}]");
        self.mpv
            .set_property("options/ytdl-format", ytdl_format.as_str())
            .map_err(form)?;
        Ok(())
    }
    fn set_youtube_dl(&mut self, ytdl: &str) -> Result<(), String> {
        self.mpv
            .set_property(
                "options/script-opts",
                format!("ytdl_hook-ytdl_path={}", ytdl).as_ref(),
            )
            .map_err(form)?;
        Ok(())
    }
    fn build(self) -> Result<Self::PlayerWrapper, String> {
        let mpv_events = self.mpv.create_event_context();

        mpv_events.enable_all_events().map_err(form)?;

        mpv_events
            .observe_property("percent-pos", Format::Double, 1)
            .map_err(form)?;
        mpv_events
            .observe_property("duration", Format::Double, 2)
            .map_err(form)?;
        mpv_events
            .observe_property("time-pos", Format::Double, 3)
            .map_err(form)?;
        /*mpv_events
        .observe_property("idle-active", Format::Flag, 4)
        .map_err(form)?;*/

        // SAFETY: see PlayerWrapper
        let mpv_events: EventContext<'static> = unsafe { std::mem::transmute(mpv_events) };

        Ok(PlayerWrapper {
            pos_percent: 0.,
            pos_seconds: 0.,
            dur_seconds: None,

            current_file: None,

            continue_at: HashMap::new(),

            queue: VecDeque::new(),

            mpv: self.mpv,
            mpv_events,
        })
    }
}

pub struct PlayerWrapper {
    pos_percent: f64,
    pos_seconds: f64,
    dur_seconds: Option<f64>,

    current_file: Option<String>,

    continue_at: HashMap<String, f64>,

    queue: VecDeque<PlayerEvent>,

    mpv: Mpv,

    // self referential to self.mpv
    // SAFETY: Interfaces of mpv are behind shared references. We only need to ensure that it outlives self.mpv
    // SAFETY: Drop is no-op
    mpv_events: EventContext<'static>,
}

impl PlayerWrapper {
    fn queue_file_finish(&mut self, reason: FileFinishReason) {
        if let Some(file) = self.current_file.take() {
            self.queue
                .push_back(PlayerEvent::FileFinished { file, reason });
        }
    }
}

impl PlayerWrapperTrait for PlayerWrapper {
    fn queue_file_or_url_to_play(&mut self, path: &str, continue_at: f64) -> Result<(), String> {
        self.continue_at.insert(String::from(path), continue_at);
        self.mpv
            .playlist_load_files(&[(path, libmpv::FileState::AppendPlay, None)])
            .map_err(form)?;
        Ok(())
    }

    fn check_on(&mut self, wait: f64) -> Result<Option<PlayerEvent>, String> {
        if let Some(event) = self.queue.pop_front() {
            return Ok(Some(event));
        }

        let event = if let Some(event) = self.mpv_events.wait_event(wait) {
            let event = event.map_err(form)?;

            match event {
                Event::StartFile => {
                    self.queue_file_finish(FileFinishReason::Finish);

                    self.current_file = Some(self.mpv.get_property("path").map_err(form)?);
                    self.pos_seconds = 0.;
                    self.pos_percent = 0.;
                    self.dur_seconds = None;

                    Some(PlayerEvent::FileStarted {
                        file: self.current_file.as_ref().unwrap().clone(),
                    })
                }
                Event::FileLoaded => {
                    if let Some(continue_at) =
                        self.continue_at.get(self.current_file.as_ref().unwrap())
                    {
                        self.mpv.seek_absolute(*continue_at).map_err(form)?;
                    }
                    None
                }
                Event::EndFile(reason) => {
                    let reason = match reason {
                        mpv_end_file_reason::Eof => FileFinishReason::Finish,
                        mpv_end_file_reason::Redirect
                        | mpv_end_file_reason::Quit
                        | mpv_end_file_reason::Stop => FileFinishReason::Exit,
                        _ => FileFinishReason::Error,
                    };
                    self.queue_file_finish(reason);
                    None
                }
                Event::Shutdown => {
                    self.queue_file_finish(FileFinishReason::Finish);
                    self.queue.push_back(PlayerEvent::Shutdown);
                    None
                }
                Event::PropertyChange { name, change, .. } => {
                    match (name, change) {
                        ("percent-pos", PropertyData::Double(v)) => self.pos_percent = v as f64,
                        ("duration", PropertyData::Double(v)) => self.dur_seconds = Some(v as f64),
                        ("time-pos", PropertyData::Double(v)) => self.pos_seconds = v as f64,
                        /*("idle-active", PropertyData::Flag(true)) => {
                            self.queue_file_finish(FileFinishReason::Finish);
                            self.queue.push_back(PlayerEvent::Idle);
                        }*/
                        _ => (),
                    }
                    None
                }
                _ => None,
            }
        } else {
            None
        };

        if event.is_none() {
            if let Some(event) = self.queue.pop_front() {
                return Ok(Some(event));
            }
        }
        Ok(event)
    }

    fn pos_percentage(&mut self) -> f64 {
        self.pos_percent
    }

    fn pos_seconds(&mut self) -> f64 {
        self.pos_seconds
    }
    fn dur_seconds(&mut self) -> Option<f64> {
        self.dur_seconds
    }
}
