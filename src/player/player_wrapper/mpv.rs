use mpv::{EndFileReason::*, Event, MpvHandler, MpvHandlerBuilder};

use super::{FileFinishReason, PlayerBuilderTrait, PlayerEvent, PlayerWrapperTrait};

fn form(err: mpv::Error) -> String {
    format!("{err}")
}

pub struct PlayerBuilder {
    builder: MpvHandlerBuilder,
}

impl PlayerBuilderTrait for PlayerBuilder {
    type PlayerWrapper = PlayerWrapper;

    fn new() -> Result<Self, String> {
        let mut builder = MpvHandlerBuilder::new().map_err(form)?;
        builder.set_option("osc", true).map_err(form)?; //On Screen Controller
        builder
            .set_option("input-vo-keyboard", true)
            .map_err(form)?;
        builder
            .set_option("input-default-bindings", true)
            .map_err(form)?;
        builder.set_option("input-media-keys", true).map_err(form)?;
        builder.set_option("force-window", true).map_err(form)?;
        Ok(Self { builder })
    }

    fn set_config_log_dir(&mut self, dir: &str) -> Result<(), String> {
        self.builder
            .set_option("include", format!("{dir}/mpv.conf").as_str())
            .map_err(form)?;
        self.builder
            .set_option("log-file", format!("{dir}/mpv.log").as_str())
            .map_err(form)?;
        Ok(())
    }
    fn set_height_resolution(&mut self, height: usize) -> Result<(), String> {
        let ytdl_format = format!("bestvideo[height<={height}]+bestaudio/best[height<={height}]");
        self.builder
            .set_option("ytdl-format", ytdl_format.as_str())
            .map_err(form)?;
        Ok(())
    }
    fn set_youtube_dl(&mut self, ytdl: &str) -> Result<(), String> {
        self.builder
            .set_option(
                "script-opts",
                format!("ytdl_hook-ytdl_path={}", ytdl).as_ref(),
            )
            .map_err(form)?;
        Ok(())
    }
    fn build(self) -> Result<Self::PlayerWrapper, String> {
        Ok(PlayerWrapper {
            mpv: self.builder.build().map_err(form)?,
            pos_percentage: 0,
            current_file: None,
            shutdown: false,
        })
    }
}

pub struct PlayerWrapper {
    mpv: MpvHandler,
    pos_percentage: i64,

    current_file: Option<String>,

    shutdown: bool,
}

impl PlayerWrapperTrait for PlayerWrapper {
    fn queue_file_or_url_to_play(&mut self, path: &str, _: f64) -> Result<(), String> {
        self.mpv
            .command(&["loadfile", &path, "append-play"])
            .map_err(form)?;
        Ok(())
    }
    fn check_on(&mut self, wait: f64) -> Result<Option<PlayerEvent>, String> {
        if self.shutdown {
            return Ok(Some(PlayerEvent::Shutdown));
        }

        let event = self.mpv.wait_event(wait);
        let event = match event {
            Some(Event::StartFile) => {
                self.current_file = Some(String::from(
                    self.mpv.get_property::<&str>("path").unwrap_or(""),
                ));
                self.pos_percentage = 0;
                Some(PlayerEvent::FileStarted {
                    file: self.current_file.as_ref().unwrap().clone(),
                })
            }

            Some(Event::EndFile(r)) => {
                let reason = match r.map_err(form)? {
                    MPV_END_FILE_REASON_EOF | MPV_END_FILE_REASON_STOP => FileFinishReason::Finish,
                    MPV_END_FILE_REASON_REDIRECT | MPV_END_FILE_REASON_QUIT => {
                        FileFinishReason::Exit
                    }
                    _ => FileFinishReason::Error,
                };
                Some(PlayerEvent::FileFinished {
                    file: self.current_file.take().unwrap(),
                    reason,
                })
            }

            Some(Event::Shutdown) => {
                return Ok(if let Some(file) = self.current_file.take() {
                    self.shutdown = true;
                    Some(PlayerEvent::FileFinished {
                        file,
                        reason: FileFinishReason::Exit,
                    })
                } else {
                    Some(PlayerEvent::Shutdown)
                });
            }

            Some(Event::Idle) => Some(PlayerEvent::Idle),
            None | Some(_) => {
                self.pos_percentage = self.mpv.get_property::<i64>("percent-pos").unwrap_or(0);
                None
            }
        };
        Ok(event)
    }

    fn pos_percentage(&mut self) -> f64 {
        self.pos_percentage as f64
    }
}
