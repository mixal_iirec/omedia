mod libmpv;
pub use self::libmpv::PlayerBuilder;
pub use self::libmpv::PlayerWrapper;

/*
mod mpv;
pub use self::mpv::PlayerBuilder;
pub use self::mpv::PlayerWrapper;
*/

#[derive(Debug, Clone)]
pub enum FileFinishReason {
    Finish,
    Exit,
    Error,
}

/// every started file must finish or exit
/// before starting a new file, info about old file is accessible
/// so we can request relevant info after finish event is reported
#[derive(Debug, Clone)]
pub enum PlayerEvent {
    FileStarted {
        file: String,
    },
    FileFinished {
        file: String,
        reason: FileFinishReason,
    },

    Idle,
    Shutdown,
}

pub trait PlayerBuilderTrait: Sized {
    type PlayerWrapper: PlayerWrapperTrait;

    fn new() -> Result<Self, String>;
    fn set_config_log_dir(&mut self, dir: &str) -> Result<(), String>;
    fn set_height_resolution(&mut self, height: usize) -> Result<(), String>;
    fn set_youtube_dl(&mut self, ytdl: &str) -> Result<(), String>;
    fn build(self) -> Result<Self::PlayerWrapper, String>;
}

pub trait PlayerWrapperTrait {
    fn queue_file_or_url_to_play(&mut self, path: &str, from_second: f64) -> Result<(), String>;
    fn check_on(&mut self, wait: f64) -> Result<Option<PlayerEvent>, String>;

    fn pos_percentage(&mut self) -> f64 {
        self.pos_seconds() / self.dur_seconds().map(|dur| dur).unwrap_or(f64::INFINITY)
    }
    fn pos_seconds(&mut self) -> f64 {
        0.
    }
    fn dur_seconds(&mut self) -> Option<f64> {
        None
    }
}
