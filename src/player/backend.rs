use std::collections::HashMap;
use std::sync::mpsc::{Receiver, SendError, Sender};
use std::sync::Arc;

use crate::config::Config;
use crate::entries::EntryId;
use crate::msg::{EditMsg, RequestMsg, UiMsg};

use crate::entries::ENTRIES;
use crate::item::ChItemId;

use super::player_wrapper::{
    FileFinishReason, PlayerBuilder, PlayerBuilderTrait, PlayerEvent, PlayerWrapper,
    PlayerWrapperTrait,
};

pub(super) enum PlayerMsg {
    Play(Vec<(String, f64, ChItemId)>),
    FullEnd,
}

pub(super) fn player_backend(
    config: Arc<Config>,
    rx: Receiver<PlayerMsg>,
    tx_ui: Sender<UiMsg>,
    tx_requests: Sender<RequestMsg>,
) -> Result<(), SendError<UiMsg>> {
    let mut wrapper = None;
    'l: loop {
        while let Ok(msg) = rx.recv_timeout(std::time::Duration::from_millis(1)) {
            match msg {
                PlayerMsg::Play(list) => {
                    if wrapper.is_none() {
                        match PlayerActions::new(&config, tx_requests.clone(), tx_ui.clone()) {
                            Ok(mpv_wrapper) => wrapper = Some(mpv_wrapper),
                            Err(e) => {
                                tx_ui.send(UiMsg::Error(e))?;
                                break;
                            }
                        }
                    }
                    let wrapper = wrapper.as_mut().unwrap();
                    for (med, continue_at, id) in list {
                        if let Err(e) = wrapper.play(med, continue_at, id) {
                            tx_ui.send(UiMsg::Error(e))?;
                        }
                    }
                }
                PlayerMsg::FullEnd => break 'l,
            }
        }
        if let Some(wrap) = wrapper.as_mut() {
            match wrap.check_mpv() {
                Err(e) => tx_ui.send(UiMsg::Error(e))?,
                Ok(true) => wrapper = None,
                Ok(false) => (),
            }
        }
    }
    Ok(())
}

struct PlayerActions {
    file_to_id: HashMap<String, ChItemId>,
    max_percentage_pos: f64,
    max_pos_seconds: f64,
    seen_id: EntryId<bool>,

    tx_requests: Sender<RequestMsg>,
    tx_ui: Sender<UiMsg>,

    player: PlayerWrapper,
}
impl PlayerActions {
    fn set_seen(&self, file: &str) -> crate::Result<()> {
        if let Some(id) = self.file_to_id.get(file) {
            self.tx_requests.send(RequestMsg::Edit(EditMsg::Bool(
                self.seen_id,
                vec![(*id, true)],
            )))?;
        }
        Ok(())
    }
    fn set_i32(&self, file: &str, name: &str, value: i32) -> crate::Result<()> {
        if let Some(id) = self.file_to_id.get(file) {
            self.tx_requests.send(RequestMsg::Edit(EditMsg::I32(
                ENTRIES.i32_name_id(name),
                vec![(*id, value)],
            )))?;
        }
        Ok(())
    }

    fn new(
        config: &Arc<Config>,
        tx_requests: Sender<RequestMsg>,
        tx_ui: Sender<UiMsg>,
    ) -> crate::Result<Self> {
        let r = |r: Result<(), String>| match r {
            Err(e) => {
                let _ = tx_ui.send(UiMsg::Error(crate::Error::Formatted(e)));
            }
            Ok(_) => (),
        };
        let mut player = PlayerBuilder::new().map_err(|e| String::from(e))?;
        r(player.set_config_log_dir("player"));
        r(player.set_height_resolution(config.max_resolution_stream() as usize));
        r(player.set_youtube_dl(config.ytdl().to_str().unwrap()));
        let player = player.build()?;

        Ok(Self {
            file_to_id: HashMap::new(),
            max_percentage_pos: 0.,
            max_pos_seconds: 0.,
            seen_id: ENTRIES.bool_name_id_option("seen").unwrap(),

            tx_requests,
            tx_ui,

            player,
        })
    }
    fn play(&mut self, path: String, continue_at: f64, id: ChItemId) -> crate::Result<()> {
        if !path.is_empty() {
            self.player.queue_file_or_url_to_play(&path, continue_at)?;
            self.file_to_id.insert(path, id);
            Ok(())
        } else {
            Err(crate::Error::MissingUrlPath)
        }
    }

    fn check_mpv(&mut self) -> crate::Result<bool> {
        while let Some(event) = self.player.check_on(0.05)? {
            match event {
                PlayerEvent::FileStarted { file: _ } => {
                    self.max_percentage_pos = 0.;
                    self.max_pos_seconds = 0.;
                }
                PlayerEvent::FileFinished { file, reason } => {
                    let duration_sec = self.player.dur_seconds();
                    let pos_seconds = self.player.pos_seconds();
                    if match reason {
                        FileFinishReason::Finish => true,
                        FileFinishReason::Exit => match duration_sec {
                            Some(duration_sec) => {
                                self.max_percentage_pos > 75.
                                    && (duration_sec - self.max_pos_seconds) < 300.
                            }
                            None => self.max_percentage_pos > 75.,
                        },
                        _ => false,
                    } {
                        self.set_seen(&file)?;
                        self.set_i32(&file, "continue_at", 0)?;
                    } else {
                        self.set_i32(&file, "continue_at", pos_seconds as i32)?;
                    }

                    if let Some(duration_sec) = duration_sec {
                        self.set_i32(&file, "duration", duration_sec as i32)?;
                    }
                }
                PlayerEvent::Shutdown | PlayerEvent::Idle => return Ok(true),
            }
        }
        self.max_percentage_pos = self.max_percentage_pos.max(self.player.pos_percentage());
        self.max_pos_seconds = self.max_pos_seconds.max(self.player.pos_seconds());

        Ok(false)
    }
}
