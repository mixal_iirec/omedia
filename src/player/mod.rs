use std::sync::{
    mpsc::{channel, Sender},
    Arc,
};
use std::thread::{self, JoinHandle};

use crate::config::Config;
use crate::item::ChItemId;
use crate::msg::{RequestMsg, UiMsg};

mod backend;
use backend::{player_backend, PlayerMsg};

mod player_wrapper;

pub struct Player {
    handle: Option<JoinHandle<()>>,
    tx: Sender<PlayerMsg>,
}

impl Player {
    pub fn new(config: Arc<Config>, tx_ui: Sender<UiMsg>, tx_requests: Sender<RequestMsg>) -> Self {
        let (tx, rx) = channel();
        let handle = thread::spawn(move || {
            let _ = player_backend(config, rx, tx_ui, tx_requests);
        });
        Self {
            handle: Some(handle),
            tx,
        }
    }

    pub fn play(&self, list: Vec<(String, f64, ChItemId)>) -> crate::Result<()> {
        Ok(self.tx.send(PlayerMsg::Play(list))?)
    }
}

impl Drop for Player {
    fn drop(&mut self) {
        let _ = self.tx.send(PlayerMsg::FullEnd);
        self.handle.take().unwrap().join().unwrap();
    }
}
