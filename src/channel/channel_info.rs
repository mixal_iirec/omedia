use std::path::PathBuf;

use crate::item::changer_conditional::ItemChangerConditional;

use serde::{Deserialize, Serialize};

pub type ChannelId = crate::general::Idi32<Channel>;
pub type ChannelMap<T> = crate::general::IdMap<Channel, ChannelId, T>;

fn default_as_true() -> bool {
    true
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ChannelIdLess {
    name: String,
    url: String,
    scraper_dir: Option<PathBuf>,

    #[serde(default = "default_as_true")]
    pub(super) auto_fetch: bool,

    #[serde(default)]
    pub(super) change_on_fetch: Vec<ItemChangerConditional>,
}

impl Default for ChannelIdLess {
    fn default() -> Self {
        Self {
            name: String::new(),
            // Info for default scrapers:
            // rss takes URL pointing to rss file | youtube-dl generally takes URL to a playlist (including channel video list)
            url: String::new(),
            scraper_dir: Some(PathBuf::from("scrapers/youtube-dl/")),
            auto_fetch: true,
            change_on_fetch: vec![ItemChangerConditional::default_bools()],
        }
    }
}

impl ChannelIdLess {
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn url(&self) -> &str {
        &self.url
    }
    pub fn change_on_fetch(&self) -> &Vec<ItemChangerConditional> {
        &self.change_on_fetch
    }

    pub fn scraper_dir(&self) -> Option<&PathBuf> {
        self.scraper_dir.as_ref()
    }
}

#[derive(Debug)]
pub struct Channel {
    id_less: ChannelIdLess,
    id: ChannelId,
}

impl Channel {
    pub fn id(&self) -> ChannelId {
        self.id
    }

    pub fn id_less(&self) -> &ChannelIdLess {
        &self.id_less
    }

    pub fn new(id_less: ChannelIdLess, id: ChannelId) -> Self {
        Self { id_less, id }
    }
}

impl std::ops::Deref for Channel {
    type Target = ChannelIdLess;
    fn deref(&self) -> &Self::Target {
        &self.id_less
    }
}
