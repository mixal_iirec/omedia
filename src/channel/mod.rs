#![warn(dead_code)]

use std::collections::HashSet;
use std::path::PathBuf;
use std::sync::Arc;

use crate::files::prepare_string_for_filepath;
use crate::general::Id;

use crate::entries::ENTRIES;
use crate::files::compact::string_from_compact_file;
use crate::item::{Item, ItemId, ItemIdLess, ItemMap};
use crate::playlist::{
    Playlist, PlaylistDir, PlaylistId, PlaylistIdLess, PlaylistLocalId, PlaylistMap,
};
use crate::scrapers::{ChannelScraper, DownloadScraper};

mod channel_info;
mod edit;
mod storing;

pub use channel_info::{Channel, ChannelId, ChannelIdLess, ChannelMap};

#[derive(Debug)]
pub struct ChannelDir {
    info: Arc<Channel>,
    dir: PathBuf,
    items: Vec<Item>,
    playlists: PlaylistMap<PlaylistDir>,

    channel_scraper: Option<ChannelScraper>,

    item_idx: ItemMap<usize>,
    next_item_id: ItemId,

    next_playlist_id: PlaylistLocalId,
}

impl ChannelDir {
    fn index_from_id(&self, id: ItemId) -> Option<usize> {
        self.item_idx.get(id).cloned()
    }

    pub fn info(&self) -> &Arc<Channel> {
        &self.info
    }
    pub fn url(&self) -> &str {
        &self.info.url()
    }
    pub fn id(&self) -> ChannelId {
        self.info.id()
    }

    pub fn auto_fetch(&self) -> bool {
        self.info.auto_fetch
    }

    pub fn playlists_info(&self) -> Vec<Arc<Playlist>> {
        self.playlists
            .values()
            .map(|p| Arc::clone(p.info()))
            .collect()
    }
    pub fn items(&self) -> &Vec<Item> {
        &self.items
    }
    pub fn channel_scraper(&self) -> &Option<ChannelScraper> {
        &self.channel_scraper
    }
    pub fn download_scraper(&self) -> Option<&DownloadScraper> {
        Some(self.channel_scraper.as_ref()?.downloader().as_ref()?)
    }
    pub fn item(&self, id: ItemId) -> Option<&Item> {
        Some(self.items.get(self.index_from_id(id)?)?)
    }

    pub fn insert_fetched_items(&mut self, new_items: Vec<ItemIdLess>) -> crate::Result<()> {
        let urls = string_from_compact_file(self.str_paths("url"))?;
        let exclude: HashSet<&str> = urls
            .iter()
            .map(|s| s.as_str())
            .filter(|s| !s.is_empty())
            .collect();
        let url_id = ENTRIES.string_name_id("url");
        let mut new_items: Vec<_> = new_items
            .into_iter()
            .filter(|i| !exclude.contains(&i.var_string(url_id).unwrap_or("")))
            .collect();
        for item_changer in self.info.change_on_fetch().iter() {
            item_changer.change_id_less(new_items.iter_mut().collect());
        }
        self.insert_items(new_items)
    }

    pub fn fetch_items_info(&self) -> crate::Result<Vec<String>> {
        Ok(string_from_compact_file(self.str_paths("url"))?)
    }

    fn insert_playlist<F>(&mut self, f: F) -> crate::Result<()>
    where
        F: FnOnce(PlaylistLocalId) -> crate::Result<PlaylistDir>,
    {
        self.playlists
            .insert(self.next_playlist_id, f(self.next_playlist_id)?);
        self.next_playlist_id.advance();
        Ok(())
    }

    pub fn create(dir: PathBuf, info: ChannelIdLess, channel_id: ChannelId) -> crate::Result<Self> {
        let info = Channel::new(info, channel_id);
        let channel_scraper = info
            .scraper_dir()
            .map(|scr| ChannelScraper::from_dir(scr).ok())
            .flatten();
        let mut channel = ChannelDir {
            info: Arc::new(info),
            dir,
            items: Vec::new(),
            playlists: PlaylistMap::new(),

            channel_scraper,

            item_idx: ItemMap::new(),
            next_item_id: ItemId::first(),
            next_playlist_id: PlaylistLocalId::first(),
        };
        channel.save_info()?;
        channel.add_playlist(PlaylistIdLess::all("All"))?;
        Ok(channel)
    }

    pub fn edit_info(&mut self, info: ChannelIdLess) -> crate::Result<()> {
        self.info = Arc::new(Channel::new(info, self.id()));
        self.channel_scraper = self
            .info
            .scraper_dir()
            .map(|scr| ChannelScraper::from_dir(scr).ok())
            .flatten();
        self.save_info()
    }

    pub fn delete(self) -> crate::Result<()> {
        crate::files::remove_dir_all(&self.dir)
    }

    pub fn add_playlist(&mut self, playlist: PlaylistIdLess) -> crate::Result<()> {
        let dir = self
            .dir_playlists()
            .join(prepare_string_for_filepath(playlist.name()));
        if dir.exists() {
            return Err(crate::Error::FileAlreadyExists);
        }

        let channel_id = self.id();
        self.insert_playlist(|id| {
            PlaylistDir::create(dir, playlist, PlaylistId::Channel(channel_id, id))
        })
    }

    pub fn delete_playlist(&mut self, id: PlaylistLocalId) -> crate::Result<()> {
        self.playlists
            .remove(id)
            .ok_or(crate::Error::InvalidId)?
            .delete()
    }

    pub fn playlist(&self, id: PlaylistLocalId) -> Option<&PlaylistDir> {
        self.playlists.get(id)
    }
    pub fn playlist_mut(&mut self, id: PlaylistLocalId) -> Option<&mut PlaylistDir> {
        self.playlists.get_mut(id)
    }

    pub fn playlist_items(&self, id: PlaylistLocalId) -> Option<(Arc<Playlist>, Vec<Item>)> {
        self.playlists.get(id).map(|i| {
            (
                Arc::clone(i.info()),
                i.info().matcher().filter(
                    self.items.clone(),
                    |name, vec| {
                        let _ = self.load_items_string_var(name, vec);
                    },
                    |_, s| self.down_paths_into_load_on_read(s),
                ),
            )
        })
    }
}
