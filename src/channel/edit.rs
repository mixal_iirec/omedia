use crate::itertools::Itertools;

use crate::entries::{EntryId, ENTRIES};
use crate::files::compact::{
    push_string_to_compact_file, set_compact_file, string_from_compact_file,
};
use crate::item::ItemId;

use super::ChannelDir;

impl ChannelDir {
    //should fit into one or two pages, so seeking would be rarely preferable
    pub fn edit_bools(&mut self, id: EntryId<bool>, vec: Vec<(ItemId, bool)>) -> crate::Result<()> {
        if vec.is_empty() {
            return Ok(());
        }
        for (item_id, b) in vec {
            if let Some(index) = self.index_from_id(item_id) {
                self.items[index].bools.set(Into::<usize>::into(id), b);
            }
        }
        set_compact_file(&self.file_vars_bool(), self.items.iter().map(|i| i.bools))
    }
    //should fit into one or two pages, so seeking would be rarely preferable
    pub fn edit_i32s(&mut self, id: EntryId<i32>, vec: Vec<(ItemId, i32)>) -> crate::Result<()> {
        if vec.is_empty() {
            return Ok(());
        }
        for (item_id, v) in vec {
            if let Some(index) = self.index_from_id(item_id) {
                self.items[index].i32s.insert(id, v);
            }
        }
        set_compact_file(
            &self
                .dir_vars_i32()
                .join(ENTRIES.i32_name_from_id(id).as_ref()),
            self.items
                .iter()
                .map(|i| i.var_i32(id).cloned().unwrap_or(i32::default())),
        )
    }
    pub fn edit_strings(
        &mut self,
        id: EntryId<str>,
        list: Vec<(ItemId, String)>,
    ) -> crate::Result<()> {
        if list.is_empty() {
            return Ok(());
        }
        let name_id = ENTRIES.string_name_from_id(id);
        let mut strings = string_from_compact_file(self.str_paths(&name_id))?;
        strings.resize(self.items.len(), String::new());
        let mut min = strings.len();
        for (id, s) in list {
            if let Some(index) = self.index_from_id(id) {
                strings[index] = s;
                min = min.min(index);
            }
        }
        push_string_to_compact_file(self.str_paths(&name_id), &strings[min..], min)
    }

    pub fn edit_add_file_paths(
        &mut self,
        id: EntryId<str>,
        list: Vec<(ItemId, Vec<String>)>,
    ) -> crate::Result<()> {
        if list.is_empty() {
            return Ok(());
        }
        let name_id = ENTRIES.string_name_from_id(id);
        let mut strings = string_from_compact_file(self.str_paths(&name_id))?;
        strings.resize(self.items.len(), String::new());
        let mut min = strings.len();
        for (id, paths) in list {
            if let Some(index) = self.index_from_id(id) {
                let mut set: std::collections::HashSet<&str> = strings[index].split('\t').collect();
                let mut change = false;
                for p in paths.iter() {
                    if set.insert(&p) {
                        change = true;
                    }
                }

                if change {
                    strings[index] = set.into_iter().join("\t");
                    min = min.min(index);
                }
            }
        }
        push_string_to_compact_file(self.str_paths(&name_id), &strings[min..], min)
    }
}
