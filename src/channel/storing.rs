use std::cmp::Ordering;
use std::path::PathBuf;
use std::sync::Arc;

use crate::files::{create_dir_all, read_dir};
use crate::general::Id;

use crate::edit_entries::EditBoolEntries;
use crate::entries::{EntryMap, EntrySet, ENTRIES};
use crate::files::compact::{
    from_compact_file, push_string_to_compact_file, set_compact_file, string_from_compact_file,
    string_from_compact_file_iter, LoadOnRead,
};
use crate::files::{yaml_from_file, yaml_to_file};

use crate::item::changer_conditional::ItemChangerConditional;
use crate::item::{Item, ItemId, ItemIdLess, ItemMap};
use crate::playlist::{PlaylistDir, PlaylistId, PlaylistIdLess, PlaylistLocalId, PlaylistMap};
use crate::scrapers::ChannelScraper;

use super::{Channel, ChannelDir, ChannelId, ChannelIdLess};

impl ChannelDir {
    pub fn load(dir: PathBuf, id: ChannelId) -> crate::Result<Self> {
        let yaml_path = dir.join("info.yaml");

        let mut id_less: ChannelIdLess = yaml_from_file(&yaml_path)?;

        if id_less.change_on_fetch.is_empty() {
            id_less
                .change_on_fetch
                .push(ItemChangerConditional::default_bools());
        }

        let info = Channel::new(id_less, id);

        let channel_scraper = info
            .scraper_dir()
            .map(|scr| ChannelScraper::from_dir(scr).ok())
            .flatten();

        Self {
            info: Arc::new(info),
            dir,
            items: Vec::new(),
            playlists: PlaylistMap::new(),

            channel_scraper,

            item_idx: ItemMap::new(),
            next_item_id: ItemId::first(),
            next_playlist_id: PlaylistLocalId::first(),
        }
        .load_inside()
    }

    fn register_item(&mut self, item: ItemIdLess) -> Item {
        Item::from_id_less(item, (self.id(), self.next_item_id.advance()))
    }

    fn push_item(&mut self, item: ItemIdLess) {
        let id = self.next_item_id.advance();
        let idx = self.items.len();
        self.items.push(Item::from_id_less(item, (self.id(), id)));
        self.item_idx.insert(id, idx);
    }

    fn update_item_idx_map(&mut self) {
        self.item_idx = self
            .items
            .iter()
            .enumerate()
            .map(|(i, item)| (item.item_id(), i))
            .collect();
    }

    pub fn load_items_string_var(
        &self,
        var_name: &str,
        mut list: Vec<(&mut Option<String>, &Item)>,
    ) -> crate::Result<()> {
        list.sort_by_key(|a| self.index_from_id(a.1.item_id()));
        let buff: Vec<_> = string_from_compact_file_iter(
            self.str_paths(var_name),
            list.iter().map(|i| self.index_from_id(i.1.item_id())),
        )?
        .collect();
        for (list, buff) in list.into_iter().zip(buff.into_iter()) {
            *list.0 = buff;
        }
        Ok(())
    }

    pub fn items_string_var<I: Iterator<Item = ItemId>>(
        &self,
        var_name: &str,
        it: I,
    ) -> crate::Result<Vec<Option<String>>> {
        Ok(string_from_compact_file_iter(
            self.str_paths(var_name),
            it.map(|id| self.index_from_id(id)),
        )?
        .collect())
    }

    fn all_string_vars(&self) -> crate::Result<impl Iterator<Item = String>> {
        Ok(read_dir(&self.dir_vars_string())?
            .filter_map(|p| p.file_name().to_str().map(|s| String::from(s))))
    }
    fn all_i32_vars(&self) -> crate::Result<impl Iterator<Item = String>> {
        Ok(read_dir(&self.dir_vars_i32())?
            .filter_map(|p| p.file_name().to_str().map(|s| String::from(s))))
    }
    pub fn fully_load_items<I: std::borrow::BorrowMut<Item>>(
        &self,
        list: &mut [I],
    ) -> crate::Result<()> {
        for name in self.all_string_vars()? {
            let id = ENTRIES.string_name_id(&name);
            let strings = string_from_compact_file(self.str_paths(&name))?;
            for i in list.iter_mut() {
                if let Some(string) = self
                    .index_from_id(i.borrow().item_id())
                    .map(|idx| strings.get(idx).cloned())
                    .flatten()
                {
                    i.borrow_mut().strings.insert(id, Arc::from(string));
                }
            }
        }
        Ok(())
    }

    pub fn down_paths_into_load_on_read(&self, s: &str) -> Vec<LoadOnRead> {
        s.split('\t')
            .map(|p| LoadOnRead::new(self.dir_downloads().join(p)))
            .collect()
    }

    pub fn dir_downloads(&self) -> PathBuf {
        self.dir.join("downloads")
    }
    pub(super) fn dir_playlists(&self) -> PathBuf {
        self.dir.join("playlists")
    }
    pub(super) fn dir_vars_string(&self) -> PathBuf {
        self.dir.join("vars_string")
    }
    pub(super) fn dir_vars_string_offsets(&self) -> PathBuf {
        self.dir.join("vars_string_offsets")
    }
    pub(super) fn dir_vars_i32(&self) -> PathBuf {
        self.dir.join("vars_i32")
    }
    pub(super) fn file_vars_date(&self) -> PathBuf {
        self.dir.join("vars_date")
    }
    pub(super) fn file_vars_bool(&self) -> PathBuf {
        self.dir.join("vars_bool")
    }

    pub(super) fn create_dir(&self) -> crate::Result<()> {
        create_dir_all(&self.dir)?;
        create_dir_all(&self.dir_playlists())?;
        create_dir_all(&self.dir_downloads())?;
        create_dir_all(&self.dir_vars_string())?;
        create_dir_all(&self.dir_vars_string_offsets())?;
        create_dir_all(&self.dir_vars_i32())?;
        Ok(())
    }

    pub(super) fn save_info(&self) -> crate::Result<()> {
        self.create_dir()?;

        yaml_to_file(&self.dir.join("info.yaml"), &**self.info)
    }

    fn load_inside(mut self) -> crate::Result<Self> {
        self.create_dir()?;
        self.load_items()?;

        let channel_id = self.id();
        for dir in read_dir(&self.dir_playlists())? {
            self.insert_playlist(|id| {
                PlaylistDir::load(dir.path(), PlaylistId::Channel(channel_id, id))
            })?;
        }

        if self.playlists.was_always_empty() {
            self.add_playlist(PlaylistIdLess::all("All"))?;
        }
        Ok(self)
    }

    fn push_items(&mut self, mut items: Vec<Item>) -> crate::Result<()> {
        self.fully_load_items(&mut items)?;

        let old_len = self.items.len();

        self.items.extend(items);

        self.push_items_storage(old_len)?;

        for item in &mut self.items {
            item.strings.clear();
        }

        self.update_item_idx_map();
        Ok(())
    }

    fn push_items_storage(&self, from: usize) -> crate::Result<()> {
        let items = &self.items[from..];
        set_compact_file(&self.file_vars_bool(), self.items.iter().map(|i| i.bools))?;
        set_compact_file(
            &self.file_vars_date(),
            self.items.iter().map(|i| i.date().clone()),
        )?;

        let mut strings: EntryMap<str, _> = self
            .all_string_vars()?
            .into_iter()
            .map(|name| (ENTRIES.string_name_id(&name), Vec::new()))
            .collect();

        for (i, item) in items.iter().enumerate() {
            for (k, v) in item.strings.iter() {
                let vec = strings.get_mut_or_default(k);
                vec.resize(i, "");
                vec.push(&v);
            }
        }

        for (k, v) in strings {
            push_string_to_compact_file(self.str_paths(&ENTRIES.string_name_from_id(k)), v, from)?;
        }

        let mut i32s: EntryMap<i32, _> = self
            .all_i32_vars()?
            .into_iter()
            .map(|name| (ENTRIES.i32_name_id(&name), Vec::new()))
            .collect();

        for (i, item) in self.items.iter().enumerate() {
            for (k, v) in item.i32s.iter() {
                let vec = i32s.get_mut_or_default(k);
                vec.resize(i, 0);
                vec.push(*v);
            }
        }
        for (k, v) in i32s {
            let entry = ENTRIES.i32_name_from_id(k);
            set_compact_file(&self.dir_vars_i32().join(entry.as_ref()), v)?;
        }

        Ok(())
    }

    fn load_items(&mut self) -> crate::Result<()> {
        let dates = from_compact_file(&self.file_vars_date())?;
        let bools = from_compact_file(&self.file_vars_bool())?;
        let mut map: EntryMap<i32, Vec<i32>> = EntryMap::new();

        for path in read_dir(&self.dir_vars_i32())? {
            let name_osstr = path.file_name();
            let name = name_osstr.to_str().unwrap();
            let id = ENTRIES.i32_name_id(&name);
            map.insert(id, from_compact_file(&path.path())?);
        }

        //as side efect, we can later get list of all posible names
        self.all_string_vars()?.into_iter().for_each(|name| {
            ENTRIES.string_name_id(&name);
        });

        let i32s_vec = crate::general::entry_map_vec_to_vec_map(map, dates.len());

        for (date, bools, i32s) in izip!(dates, bools, i32s_vec) {
            self.push_item(ItemIdLess::new(date, EntryMap::new(), i32s, bools));
        }
        Ok(())
    }

    pub fn delete_item_media(&mut self, list: &[ItemId]) -> crate::Result<()> {
        if list.is_empty() {
            return Ok(());
        }

        let downfiles = string_from_compact_file(self.str_paths("downfile"))?;
        let subtitles = string_from_compact_file(self.str_paths("subtitles"))?;

        let empties: Vec<_> = list.iter().map(|id| (*id, String::new())).collect();

        for i in list.iter().filter_map(|id| self.index_from_id(*id)) {
            if let Some(downfile) = downfiles.get(i).filter(|downfile| !downfile.is_empty()) {
                crate::files::remove_file(&self.dir_downloads().join(downfile))?;
            }
            if let Some(subtitle) = subtitles.get(i) {
                for sub in subtitle.split('\t').filter(|sub| !sub.is_empty()) {
                    crate::files::remove_file(&self.dir_downloads().join(sub))?;
                }
            }
        }

        self.edit_strings(ENTRIES.string_name_id("downfile"), empties.clone())?;
        self.edit_strings(ENTRIES.string_name_id("subtitle"), empties)?;

        Ok(())
    }

    pub fn delete_items(&mut self, list: Vec<ItemId>) -> crate::Result<()> {
        if list.is_empty() {
            return Ok(());
        }

        self.delete_item_media(&list)?;

        let mut cutoff = self.items.len();
        let mut keep = vec![true; self.items.len()];
        for i in list.into_iter().filter_map(|id| self.index_from_id(id)) {
            cutoff = cutoff.min(i);
            keep[i] = false;
        }
        let mut idx = 0;
        self.items.retain(|_| (keep[idx], idx += 1).0);

        let items = self.items.split_off(cutoff);
        self.push_items(items)
    }

    pub fn insert_items(&mut self, list: Vec<ItemIdLess>) -> crate::Result<()> {
        if list.is_empty() {
            return Ok(());
        }
        let min_date = list.iter().map(|i| i.date()).min().unwrap();

        let cutoff = self
            .items
            .binary_search_by(|i| {
                if i.date() > min_date {
                    Ordering::Greater
                } else {
                    Ordering::Less
                }
            })
            .unwrap_err();

        let mut vec: Vec<_> = self
            .items
            .split_off(cutoff)
            .into_iter()
            .chain(list.into_iter().map(|i| self.register_item(i)))
            .collect();
        vec.sort_by_key(|a| a.date().clone());
        self.push_items(vec)
    }

    pub fn get_media_paths(
        &mut self,
        list: &mut [(ItemId, &mut (String, f64))],
    ) -> crate::Result<()> {
        let continue_at_id = ENTRIES.i32_name_id("continue_at");

        let downfiles = string_from_compact_file(self.str_paths("downfile"))?;
        let urls = string_from_compact_file(self.str_paths("url"))?;
        let mut repair = Vec::new();
        for (id, s) in list {
            let index = self.index_from_id(*id).ok_or(crate::Error::InvalidId)?;
            let downfile = downfiles.get(index).map(|s| s.as_str());
            s.0 = match downfile {
                None | Some("") => String::from(urls.get(index).map(|s| s.as_str()).unwrap_or("")),
                Some(downfile) => {
                    let path = self.dir_downloads().join(downfile);
                    String::from(if path.exists() {
                        path.to_str().unwrap()
                    } else {
                        repair.push(*id);
                        urls.get(index).map(|s| s.as_str()).unwrap_or("")
                    })
                }
            };

            s.1 = self
                .item(*id)
                .map(|it| *it.var_i32(continue_at_id).unwrap_or(&0) as f64)
                .unwrap_or(0.);
        }
        self.edit_strings(
            ENTRIES.string_name_id("downfile"),
            repair.iter().map(|i| (*i, String::new())).collect(),
        )
    }

    pub fn load_strings_needed(
        &self,
        needed: &EntrySet<str>,
        mut items: Vec<&mut Item>,
    ) -> crate::Result<()> {
        items.sort_by_key(|a| self.index_from_id(a.item_id()));
        for id in needed.iter() {
            let strings = self.items_string_var(
                &ENTRIES.string_name_from_id(id),
                items.iter().map(|i| i.item_id()),
            )?;

            for (string, item) in strings.into_iter().zip(items.iter_mut()) {
                if let Some(string) = string {
                    item.strings.insert(id, Arc::from(string));
                }
            }
        }
        Ok(())
    }
    pub(super) fn str_paths(&self, name: &str) -> (PathBuf, PathBuf) {
        (
            self.dir_vars_string().join(name),
            self.dir_vars_string_offsets().join(name),
        )
    }

    pub fn transform_entries(&mut self, edit: &EditBoolEntries) -> crate::Result<()> {
        set_compact_file(
            &self.file_vars_bool(),
            self.items.iter().map(|i| edit.transform(&i.bools)),
        )
    }
}
